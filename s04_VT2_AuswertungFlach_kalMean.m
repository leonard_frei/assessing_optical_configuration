% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

clear all; close all; clc;

%% Automatisiert auswerten

% Alle Datein bestimmen (Pfade bei Bedarf ändern!)
sstrRefMess='Daten/KMGMessungen/20210120_Versuchstag2_Flach/VT2_flach.xlsx';
pfdExp='Daten/20210114_Versuchstag2';
pfd=dir(pfdExp);
nmexp={pfd(:).name}';

% Versuchstagebuch festlegen
strVT='Versuchstagebuch_Tag2_flach_kalMean.xlsx';

% Mittlere Kameraparameter bestimmen
for j=1:4    
    x = {};
        for i=1:length(nmexp)
            if contains(nmexp{i}, ('AufnKal_flach_' + string(j))) == 1
                x{end+1} = nmexp{i};
            end
        end
    kamPara = fpStereoAufn_kalMean(x, pfdExp);
    name = 'kalMean';
    filename = (x{1}(1:end-5) + string(name));
    filename = fullfile(pfdExp, filename);
    save(filename, 'kamPara');
    nmexp={}; pfd=dir(pfdExp); nmexp={pfd(:).name}';
end

% Typen von Datein bestimmen
indFlach=contains(nmexp,'flach');
indProj=contains(nmexp,'AufnProj');
indKal=contains(nmexp,'kalMean');

%% Auswertung flach

% Indizes der Messungen
indFlachMess=(indFlach) & indProj;
nmFlachMess=nmexp(indFlachMess); nFlachMess=size(nmFlachMess,1);

for i=1:nFlachMess
    
    % Aktuelle Messung
    nm_i=nmFlachMess{i};
    
    % Konfiguration der aktuellen Messung
    indUndscr=strfind(nm_i,'_');
    indMat=strfind(nm_i,'.mat');
    konfig_i=nm_i( (indUndscr(end-1)) : (indUndscr(end)) );
    mess_i=nm_i( (indUndscr(end)+1) );
    
    % Zugehörige Kalibrierungen 
    indMess_i=contains(nmexp,konfig_i);
    indKal_i= indMess_i & (indFlach) & indKal;
    nmKal_i=nmexp(indKal_i); nKal_i=size(nmKal_i,1);
    
    for j=1:nKal_i
        
        close all;
        
        % Akuelle Kalibrierung extrahieren und Strings erstellen
        nmKal_ij=nmKal_i{j}; 
        VersuchID=['VT2',konfig_i,mess_i,'_Kal_',num2str(j)];
        aufnKalStr={['Daten/20210114_Versuchstag2/',nmKal_ij],...
            ['Daten/20210114_Versuchstag2/',nm_i]}; % Aufnahmen
        disp(VersuchID);
        
        % Parameter laden und Auswertung durchführen
        s04a_VT2_ParaFlach; vis=false;
        
        % Kreisthreshold
        if any((i==[11 13 14 15 16 17 18 19 20]))
            thK=.95; radiusGr=[13 70];
        elseif i==[12]
            thK=.94; radiusGr=[13 70];
        else
            thK=.9;
        end
        
        % Auswertung
        s04a_VT2_AuswertungFlach;
        
    end
    
end