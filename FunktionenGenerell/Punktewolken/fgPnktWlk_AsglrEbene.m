% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [modell, indIn]=fgPnktWlk_AsglrEbene(PWEbene,dZEbene,varargin)
% Per Ausgleichsrechnung eine Ebene in eine Punktwolke einpassen

% Parsen
p = inputParser;
defaultNiter=5; addOptional(p,'niter',defaultNiter);
parse(p,varargin{:});
niter=p.Results.niter;

% Extraktion der Punktwolke
XYZ=PWEbene.Location;

%% Grobausgleich

% Startlösung
[n_g, p_g, para_g]=funcUnter_nEbene(XYZ);

% Iterative Verfeinerung
for i=1:niter
    
    % Indizes der Punkte finden, die nicht zu weit von der Grobebene
    % entfernt sind
    abstVek=XYZ-p_g;
    abstVekZ=abstVek*n_g;
    indIn=find(abstVekZ<dZEbene);
    
    % Feinausgleich
    XYZ_f=XYZ(indIn,:);
    [n_g, p_g, para_g]=funcUnter_nEbene(XYZ_f);
    
end

%% Feinausgleich

% Indizes der Punkte finden, die nicht zu weit von der Grobebene
% entfernt sind
abstVek=XYZ-p_g;
abstVekZ=abstVek*n_g;
indIn=find(abstVekZ<dZEbene);

% Feinausgleich
XYZ_f=XYZ(indIn,:);
[n_f, p_f, para_f]=funcUnter_nEbene(XYZ_f);

% Übergabe
modell.Parameters=para_f;
modell.Normal=n_f';

end

%% Unterfunktion

function [n, p, para]=funcUnter_nEbene(XYZ)
% Normalenvektor einer Ebene berechnen, die in eine Punktwolke eingepasst
% wird

% Mittelpunkt der Punktwolke
p = mean(XYZ,1);

% Abstand zum Mittelpunkt
R = XYZ-p;

% Berechnung der Hauptrichtungen der Abstandsvektoren
[V,D] = eig(R'*R);

% Normalenvektor extrahieren
n = V(:,1);

% Ebenenparameter
d=-(p*n);
para=[n',d];

end