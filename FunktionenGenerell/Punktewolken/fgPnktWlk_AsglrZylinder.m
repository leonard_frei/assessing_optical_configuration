% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function modell=fgPnktWlk_AsglrZylinder(xyzZ,varargin)
% Einen nicht-linearen Zylinderfit durchf�hren

%% Parsen

p=inputParser;
defaultVis=false; addOptional(p,'vis',defaultVis);
defaultSchaetz=false; addOptional(p,'schaetz',defaultSchaetz);
defaultMxNTrials=1e4; addOptional(p,'mxNTrials',defaultMxNTrials);
defaultMxD=0.005; addOptional(p,'mxD',defaultMxD);

parse(p,varargin{:}); 

vis=p.Results.vis; schaetz=p.Results.schaetz; 
mxNTrials=p.Results.mxNTrials; mxD=p.Results.mxD;

%% Startwert berechnen

% Standardwerte
x0=ones(1,7);

% Startwerte �ber Matlab-Funktion
if isnumeric(schaetz)
    x0=schaetz;
elseif schaetz
    ptCloudIn=pointCloud(xyzZ);
    model = pcfitcylinder(ptCloudIn,mxD,'MaxNumTrials',mxNTrials);
    x0=model.Parameters;
    disp(['Startwerte: ', num2str(x0)]);
end

%% Ausgleichsrechnung

% Nichtlineares Gleichungssystem l�sen
options.Algorithm = 'levenberg-marquardt';
zylp = lsqnonlin(@(x) fu_ZylinderF(x,xyzZ),x0,[],[],options);

% Parameter entpacken
rf=zylp(7); zf=zylp(1:3); wvecf=zylp(4:6); wvecf=wvecf/norm(wvecf); 

% Mittelpunkt und Ursprung anpassen
d=((xyzZ-zf)*wvecf'); 
zf=(max(d)+min(d))/2*wvecf+zf;
hf=max(d)-min(d);

%% �bergabe

modell.Parameter=[zf, wvecf, rf, hf];
modell.Zentrum=zf; 
modell.Orientierung=wvecf;
modell.Radius=rf;
modell.Hoehe=hf;

%% Visualisieren

if vis
    
    % Plotpunkt erstellen
    xyzZf=fgPnktWlk_ZylVis(hf,rf,zf,wvecf);
    
    % Plotten
    figure; plot3(xyzZf(:,1),xyzZf(:,2),xyzZf(:,3),'-*',...
        xyzZ(:,1),xyzZ(:,2),xyzZ(:,3),'o');
    axis('equal'); legend('Fit','Data');
    xlabel('x'); ylabel('y'); zlabel('z');
    
end

end

%% Unterfunktion

function D = fu_ZylinderF(x,M)
% Mit Zylinderparametern x und Beobachtungen M den Fehler zwischen
% gegebenem Radius und berechnetem besteimmen

% Auspacken
xc=x(1); yc=x(2); zc=x(3); a=x(4); b=x(5); c=x(6); r=x(7);
x=M(:,1); y=M(:,2); z=M(:,3);

% Bestandtile
u=c*(y-yc) - b*(z-zc);
v=a*(z-zc) - c*(x-xc); 
w=b*(x-xc) - a*(y-yc); 

% Radius berechnen
R=(u.^2 + v.^2 + w.^2).^0.5 / (a^2 + b^2 + c^2).^0.5;

% Differenz
D=R-r;

end