% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function xyzZ=fgPnktWlk_ZylVis(hmax,r,z,wvec,varargin)
% Punkte auf einem Zylinder berechnen 

%% Parsen

p=inputParser;

defaultRfR=0; addOptional(p,'rfR',defaultRfR);
defaultRfP=0; addOptional(p,'rfP',defaultRfP);
defaultVis=false; addOptional(p,'vis',defaultVis);
defaultNphi=50; addOptional(p,'nphi',defaultNphi);
defaultNh=100; addOptional(p,'nh',defaultNh);
defaultPhiStartEnde=[0 2*pi]; addOptional(p,'phiStartEnde',defaultPhiStartEnde);

parse(p,varargin{:}); 

rfR=p.Results.rfR; rfP=p.Results.rfP; vis=p.Results.vis; 
nphi=p.Results.nphi; nh=p.Results.nh; phiStartEnde=p.Results.phiStartEnde; 

%%

wvec=wvec/norm(wvec);
w90vec=[1 0 0]-wvec*([1 0 0]*wvec'); w90vec=w90vec/norm(w90vec); 
phi=linspace(phiStartEnde(1),phiStartEnde(2),nphi);
h=linspace(0, hmax, nh);

% Zylinderpunkte konstruieren
xyzZ=zeros(nphi*nh,3);
for i=1:nh
    
    % Berechnen aktuellen Zentralpunkt
    h_i=h(i);
    pvec_i=z+wvec*(h_i-hmax/2);
    
    % Rauschen hinzufügen
    pvec_i=pvec_i*(1+rfP*(rand(1)-0.5));
    
    for j=1:nphi
        
        % Anteil in radiale Richtung hinzufügen
        rotv_j = phi(j)*wvec;
        rotMat_j = rotationVectorToMatrix(rotv_j);
        rvec_j=(rotMat_j*(r*w90vec)')';
        
        % Rauschen hinzufügen
        rvec_j=rvec_j*(1+rfR*(rand(1)-0.5));
        
        % Aktuellen Punkt eintragen
        ind_ij=(i-1)*nphi+j;
        xyzZ(ind_ij,:)=(pvec_i+rvec_j)';
    end
end

% Darstellen#
if vis; figure; pcshow(xyzZ,'MarkerSize',15); end

end
