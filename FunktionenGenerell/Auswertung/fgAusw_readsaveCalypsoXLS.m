% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function TKMG=fgAusw_readsaveCalypsoXLS(ordnKMG,nmAuswGesKMG)
% Lese alle VG Studios CSVs aus einem Ordner ein und f�hre sie in einer
% Excel zusammen

% Filenamen
dirKMG=dir([ordnKMG,'*.xls*']);
nmsKMG=string({dirKMG(:).name});

% Einlesen KMG
TwerteKMG=cellfun(@(x) readtable([dirKMG(1).folder,'/',x],...
    'DatetimeType','text','UseExcel',false,'Format','auto'),...
    nmsKMG,'UniformOutput',false);

% Potentiell fehlerhafte Protokolle ausfindig machen (zu kurz)
T1=table2cell(TwerteKMG{1});
lngP=cellfun(@height,TwerteKMG);
if any(lngP<max(lngP))
    warning(append(nmsKMG(lngP<max(lngP))," beinhaltet zu wenige ",...
        "Merkmale. Vielleicht wurde dieser Ordner (",ordnKMG,...
        ") schon ausgewertet und dies ist die Zusammenfassung. ",...
        "Die Werte dieser Datei wurden nicht ber�cksichtigt."));
    TwerteKMG(lngP<max(lngP))=[];
    dirKMG(lngP<max(lngP))=[];
    nmsKMG(lngP<max(lngP))=[];
end

% Spalte der Messwerte und alle Zeilen der Messwerte finden
T1=table2cell(TwerteKMG{1});
[zlKMGAct,spKMG]=find(contains(T1,'Actual'));
zlKMGEnde=size(T1,1);
zlKMG=zlKMGAct(end)+1:zlKMGEnde; spKMG=spKMG(end);

% Nur Spalte mit Messwerten ausw�hlen und in eine Tabelle bringen
werteKMG=cellfun(@(x) table2array(x(zlKMG,spKMG)),TwerteKMG,...
    'UniformOutput',false);
werteKMG=string(horzcat(werteKMG{:}));

% Matrix erstellen
werteKMGd=double(werteKMG);

% Winkelangaben, gesondert erstellen (Winkel werden von Excel als Zeiten
% eingelesen, welche konvertiert werden m�ssen; dazu muss auch ein Versatz
% zwischen Excel und Matlab von einem Tag beglichen werden)
wnklind=isnan(double(werteKMGd));
werteKMGd(wnklind)=(datenum(werteKMG(wnklind))-...
    datenum("01-Jan-1900 00:00:00")+1)*24;

% Gesamttabelle mit Zeilen und Spaltennamne erstellen
nmsTKMG=strrep(nmsKMG,'.xlsx',''); 
nmsTKMG=strrep(nmsTKMG,'.xls',''); 
nmsTKMG=strrep(nmsTKMG,' ','_'); 
zlnm=T1(zlKMG,1);
TKMG = array2table(werteKMGd,'VariableNames',nmsTKMG,'RowNames',zlnm);

% Endergebnis schreiben
TKMG.Properties.DimensionNames{1}='Merkmal';
if exist([ordnKMG,'\',nmAuswGesKMG,'.xlsx'],'file') 
    delete([ordnKMG,'\',nmAuswGesKMG,'.xlsx']); end
writetable(TKMG,[ordnKMG,'\',nmAuswGesKMG,'.xlsx'],'WriteRowNames',true);

end