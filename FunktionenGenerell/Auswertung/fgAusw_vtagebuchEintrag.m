% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function Tvt=fgAusw_vtagebuchEintrag(versuchID,spName,spWerte,varargin)
% Erg�nze einen Eintrag im Versuchstagebuch

%% Parsen und Parameter

p=inputParser;
defaultTvt=[]; addOptional(p,'Tvt',defaultTvt);
defaultStrVT='Versuchstagebuch.xlsx'; addOptional(p,'strVT',defaultStrVT);
parse(p,varargin{:});
TvtIn=p.Results.Tvt; strVT=p.Results.strVT;


%% Einen Eintrag erg�nzen
% Lese das Versuchstagebuch ein
if isempty(TvtIn); Tvt=readtable(strVT); else; Tvt=TvtIn; end
neintr=size(Tvt,1);

% Falls Eingangswerte numerisch, in Zelle umwandeln; und liegend
if isnumeric(spWerte); spWerte=num2cell(spWerte); end
if size(spWerte,1)>size(spWerte,2); spWerte=spWerte'; end

% Entferne Umlaute usw.
spName=strrep(spName,'�','a');
spName=strrep(spName,'-','_');


% Pr�fe, ob es schon einen Eintrag gibt
indND=strcmp(Tvt.VersuchID,versuchID);

% F�ge ggf. einen neuen Eintrag hinzu
if ~any(indND)
    T_i=Tvt(1,:); nsp=size(T_i,2);
    T_i.VersuchID={versuchID}; 
    for i=2:nsp
        if iscell(T_i{1,i})
            T_i{1,i}={' '};
        elseif isnumeric(T_i{1,i})
            T_i{1,i}=NaN;
        end
    end
    Tvt=[Tvt;T_i]; neintr=neintr+1; indND=[indND; true];
end

% Korrekte Zeile finden
T_i=Tvt(indND,:); nmsAlt=T_i.Properties.VariableNames; 

% Herausfinden, welche Spalten bereits vorhanden sind
indVorNvor=cellfun(@(x) any(strcmp(nmsAlt,x)),spName);

% Vorhandene Spalten beschreiben
indVor=cellfun(@(x) find(strcmp(nmsAlt,x)),spName(indVorNvor));
% if ~isempty(indVor); T_i{1,indVor}=[spWerte{indVorNvor}]; end
if ~isempty(indVor); T_i(1,indVor)=spWerte(indVorNvor); end
Tvt(indND,:)=T_i;

% Nicht vorhandene Spalten hinzuf�gen
if any(~indVorNvor)
    wTneu=num2cell(zeros(neintr,sum(~indVorNvor)));
    wTneu(indND,:)=spWerte(~indVorNvor);
    TspNeu=array2table(wTneu);
    TspNeu.Properties.VariableNames=spName(~indVorNvor);
    Tvt=[Tvt,TspNeu];
end

% Versuchstagebuch erg�nzen
if isempty(TvtIn); writetable(Tvt,strVT); end

end