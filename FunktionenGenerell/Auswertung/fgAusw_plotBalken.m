% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function f=fgAusw_plotBalken(werte,streu,beschr,varargin)
% Plotte Werte in einem Balkendiagramm mit Streuungsbalken 

p = inputParser;

defaultCmat = [];
addOptional(p,'cmat',defaultCmat)

defaultLpos=[];
addOptional(p,'lpos',defaultLpos);

parse(p,varargin{:})

cmat=p.Results.cmat;
lpos=p.Results.lpos;

%% Plotten
% Anzahl der Gruppen und Eintr?ge pro Gruppe
ngrup=size(werte,2); neint=size(werte,1);

% Erzeuge Colormap; Standard ist "lines"
if isempty(cmat)
    C=colormap('lines');
    cbar=permute(C(1:ngrup,:), [3 1 2]);
else
    cbar=permute(cmat, [3 1 2]);
end

% Plotte Bars
f=figure; 
ax_bar=superbar(werte,'BarFaceColor',cbar,'E',streu);

%% Beschriftung
% Beschrifte die Abbildung
title(beschr.tit);
ax=gca;
ax.TickLabelInterpreter='none';
ax.XTick=1:neint;
ax.XTickLabelRotation = 45;
ax.XLabel.String=beschr.xName;
ax.YLabel.String=beschr.yName;
ax.XTickLabel=beschr.xTickNames;

% Skalierung der Y-Achse
if isfield(beschr,'ylim'); ax.YLim=beschr.ylim; end

% Positioniere die Abbildung
if isfield(beschr,'pos')
    f.Position=beschr.pos; f.Color=ones(3,1);
else
    f.Position=[0 200 900 400]; f.Color=ones(3,1);
end

% Erzeuge die Legende
if isfield(beschr,'leg')
    leg=legend(ax_bar(1,:),beschr.leg,'Interpreter','none');
    
    if isempty(lpos)
        leg.Location='northeast';
    else
        leg.Position=lpos;
    end
    
end


end


%% SUPERBAR & SUPERER FUNCTIONS
% https://de.mathworks.com/matlabcentral/fileexchange/57499-superbar

function varargout = superbar(X, Y, varargin)

% Check number of inputs is okay
if exist('narginchk', 'builtin')
    narginchk(1, Inf);
elseif abs(nargin) < 1
    error('MATLAB:narginchk:notEnoughInputs', 'Not enough input arguments.');
end

% Extend the reach of varargin
if nargin>=2
    varargin = [{Y}, varargin];
end
varargin = [{X}, varargin];

% Strip out axes input if it is there
[ax, varargin, nargs] = axescheck(varargin{:});
ax_or_empty = ax;
% Otherwise, default with the current axes
if isempty(ax)
    ax = gca;
end
% Check number of inputs is still okay
if nargs<1
    error('Must provide at least 1 input argument, in addition to axes.');
end

% Input handling for X and Y
if nargs==1 || ischar(varargin{2})
    % Deal with omitted X input
    Y = varargin{1};
    X = [];
    % Drop the argument
    varargin = varargin(2:end);
else
    % Take X and Y out of varargin
    X = varargin{1};
    Y = varargin{2};
    % Drop these arguments
    varargin = varargin(3:end);
end
if ~ismatrix(Y)
    error('Y should have no more than 2-dimensions (%d given).', ndims(Y));
end
if size(Y, 1)==1
    Y = Y';
end
if isempty(X)
    X = (1:size(Y, 1))';
end
if size(X, 1)~=size(Y, 1) && size(X, 2)==size(Y, 1)
    X = X';
end
if size(X, 1)~=size(Y, 1)
    error('X and Y must be the same size in dimension 1 (%d and %d given)', ...
        size(X, 1), size(Y, 1));
end

% Use parser for the rest of the arguments
parser = inputParser;
% Make a function for adding arguments to parser which works for both new
% and old matlab versions
    function safelyAddParameter(p, varargin)
        if ismethod(p, 'addParameter');
            % Added in R2013b
            p.addParameter(varargin{:});
        elseif ismethod(p, 'addParamValue')
            % Added in R2007a, and deprecated from R2013b onwards
            p.addParamValue(varargin{:});
        else
            error(...
                ['Could not find a method to add parameters to the input' ...
                ' parser. Please upgrade your MATLAB installation to R2013a' ...
                'or later.']);
        end
    end
% Plot attributes
safelyAddParameter(parser, 'Orientation', 'v', ...
    @ischar);
safelyAddParameter(parser, 'BaseValue', 0, ...
    @(t) (isscalar(t)) && isnumeric(t));
% Bar attributes
safelyAddParameter(parser, 'BarWidth', [], ...
    @(t) (isempty(t) || isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'BarRelativeGroupWidth', 0.8, ...
    @(t) (isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'BarFaceColor', []);
safelyAddParameter(parser, 'BarEdgeColor', []);
safelyAddParameter(parser, 'BarLineWidth', 2);
% Errorbar attributes
safelyAddParameter(parser, 'E', []);
safelyAddParameter(parser, 'ErrorbarRelativeWidth', 0.5, ...
    @(t) (isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'ErrorbarColor', []);
safelyAddParameter(parser, 'ErrorbarStyle', 'I', ...
    @ischar);
safelyAddParameter(parser, 'ErrorbarLineWidth', 2, ...
    @(t) (isscalar(t)) && isnumeric(t));
% P-value attributes
safelyAddParameter(parser, 'P', []);
safelyAddParameter(parser, 'PStarThreshold', [0.05, 0.01, 0.001, 0.0001], ...
    @isnumeric);
safelyAddParameter(parser, 'PStarLatex', 'off', ...
    @(t) (ischar(t) && ismember(t, {'off', 'auto', 'all', 'on'})));
safelyAddParameter(parser, 'PStarIcon', '');
safelyAddParameter(parser, 'PStarColor', [.2 .2 .2]);
safelyAddParameter(parser, 'PStarBackgroundColor', []);
safelyAddParameter(parser, 'PStarFontSize', 14, ...
    @isscalar);
safelyAddParameter(parser, 'PStarShowNS', true, ...
    @isscalar);
safelyAddParameter(parser, 'PStarShowGT', true, ...
    @isscalar);
safelyAddParameter(parser, 'PStarOffset', [], ...
    @(t) (isempty(t) || isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'PStarFixedOrientation', [], ...
    @isscalar);
safelyAddParameter(parser, 'PLineColor', [.5 .5 .5]);
safelyAddParameter(parser, 'PLineWidth', 2, ...
    @(t) (isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'PLineOffset', [], ...
    @(t) (isempty(t) || isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'PLineOffsetSource', [], ...
    @(t) (isempty(t) || isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'PLineSourceRelativeSpacing', [], ...
    @(t) (isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'PLineSourceRelativeBreadth', [], ...
    @(t) (isempty(t) || isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'PLineBacking', []);
safelyAddParameter(parser, 'PLineBackingWidth', [], ...
    @(t) (isempty(t) || isscalar(t)) && isnumeric(t));
safelyAddParameter(parser, 'PLineBackingColor', []);
% Parse the arguments
parse(parser, varargin{:});
input = parser.Results;

% Default input arguments which inherit values from others
% Bar defaults
if isempty(input.BarWidth)
    if numel(X)>1
        % Default with 0.8 of the smallest distance between bars
        input.BarWidth = 0.8 * min(diff(sort(X(:))));
    else
        % Just use 0.8 if there's only one bar
        input.BarWidth = 0.8;
    end
end
if isempty(input.BarFaceColor)
    if isempty(input.BarEdgeColor) || isequal(input.BarFaceColor, 'none')
        input.BarFaceColor = [.4, .4, .4];
    else
        input.BarFaceColor = 'none';
    end
end
if isempty(input.BarEdgeColor)
    if isequal(input.BarFaceColor, 'none')
        input.BarEdgeColor = [.4 .4 .4];
    else
        input.BarEdgeColor = 'none';
    end
end
% Errorbar defaults
if isempty(input.ErrorbarColor)
    % Try taking the colour from the errorbar style
    COLOR_SPECS = 'rgbwcmyk';
    isColorSpec = ismember(COLOR_SPECS, input.ErrorbarStyle);
    if any(isColorSpec)
        idx = find(isColorSpec, 1, 'last');
        input.ErrorbarColor = input.ErrorbarStyle(idx);
    elseif ~isequal(input.BarEdgeColor, 'none')
        % Try taking the color from the bar edge
        input.ErrorbarColor = fix_colors_cell(input.BarEdgeColor);
    elseif ~isequal(input.BarFaceColor, 'none')
        % Try taking the color from the bar face
        color = input.BarFaceColor;
        % Convert cell into RGB array
        color = fix_colors_char(color);
        % Convert char array into RGB array
        color = fix_colors_cell(color);
        % Ensure array is MxNx3
        siz = size(color);
        if length(siz)==2 && siz(2)==3
            color = permute(color, [1 3 2]);
        end
        % Make the bar colour lighter
        lighter_color = 1 - 0.7 * (1 - color);
        % Make the bar colour darker
        darker_color = 0.7 * color;
        % Check when to take lighter and when to take darker
        is_too_dark = repmat(sum(color, 3) < 0.65, [1 1 3]);
        input.ErrorbarColor = ...
            is_too_dark .* lighter_color + ~is_too_dark .* darker_color;
    else
        % Well if you want everything transparent you can have it, I
        % guess, though maybe this should be an error instead
        input.ErrorbarColor = 'none';
    end
    % Fix rogue NaN values from 'none' within cells (an unusual event)
    input.ErrorbarColor(isnan(input.ErrorbarColor)) = 0.5;
end

% Fix size of X, and Y
% Split up bars which are composed of groups
if size(X, 2)==1
    [X, Y, input.BarWidth] = bar2grouped(X, Y, input.BarWidth, ...
        input.BarRelativeGroupWidth);
end
% Check size of X and Y match
assert(isequal(size(X), size(Y)), ...
    'Sizes of X and Y must match. Sizes were %s and %s.', ...
    mat2str(size(X)), mat2str(size(Y)));

% Fix shape of E
if numel(input.E)==1
    input.E = repmat(input.E, numel(Y), 1);
elseif numel(input.E)==2*numel(Y)
    input.E = reshape(input.E, numel(Y), 2);
elseif numel(input.E)==2 && (numel(Y)~=2 || ~ismatrix(input.E))
    input.E = repmat(input.E(:)', numel(Y), 1);
elseif numel(input.E)==numel(Y)
    input.E = input.E(:);
elseif ~isempty(input.E)
    error(...
        ['E input must contain either the same number of values as Y' ...
        ' (for symmetric errorbars), or twice as many values (for' ...
        ' asymmetric errorbars), or a single value/pair of values (to' ...
        ' use the same error for every bar).']);
end

% P-value defaults
if isempty(input.PLineOffset)
    % Base the offset on the maximum of the bars
    input.PLineOffset = 0.1 * max(abs(Y(:)));
end
if isempty(input.PStarOffset)
    if numel(input.P)==numel(X) || numel(input.P)==numel(Y)
        % If we're just showing the stars and no lines, base the offset on
        % the maximum of the bars
        input.PStarOffset = 0.05 * max(abs(Y(:)));
    else
        % If we're showing comparison lines, make the stars be a little
        % above the lines so its clear to which they belong
        input.PStarOffset = input.PLineOffset / 3;
    end
end
if isempty(input.PLineOffsetSource)
    % Vertical offset between source of comparison line above another line,
    % used when there is no horizontal space between sources.
    input.PLineOffsetSource = input.PLineOffset / 3;
end
if isempty(input.PStarFixedOrientation)
    if numel(input.P)==numel(Y)^2
        % For pairwise comparisons
        input.PStarFixedOrientation = false;
    else
        % For single bar significance
        input.PStarFixedOrientation = true;
    end
end
if isempty(input.PLineSourceRelativeSpacing)
    if ~isempty(input.ErrorbarRelativeWidth) && input.ErrorbarRelativeWidth>0
        % The maximum space between any pair of lines should be a fraction
        % of the errorbar width, if possible.
        input.PLineSourceRelativeSpacing = 1/3 * input.ErrorbarRelativeWidth;
    else
        % Otherwise base it on the bar width
        input.PLineSourceRelativeSpacing = 1/3;
    end
end
if isempty(input.PLineSourceRelativeBreadth)
    if ~isempty(input.ErrorbarRelativeWidth) && input.ErrorbarRelativeWidth>0
        % The breadth of the space the lines come from should be the same
        % as the width of the errorbars, if possible
        input.PLineSourceRelativeBreadth = input.ErrorbarRelativeWidth;
    else
        % Otherwise base it on the bar width
        input.PLineSourceRelativeBreadth = 0.8;
    end
end
if strcmp(input.PStarLatex, 'on')
    % Set this as an alias
    input.PStarLatex = 'all';
end
if isempty(input.PStarIcon)
    if strcmp(input.PStarLatex, 'off')
        % With latex off, we use an asterisk operator unicode symbol.
        input.PStarIcon = char(8727);
    else
        % With latex on, we use an asterisk, which will be converted into
        % an asterisk operator by latex.
        input.PStarIcon = '*';
    end
end
if isempty(input.PStarBackgroundColor)
    if numel(input.P)==numel(X) || numel(input.P)==numel(Y)
        input.PStarBackgroundColor = 'none';
    else
        % Background color behind significance text
        input.PStarBackgroundColor = get(ax, 'Color');
    end
end
if isempty(input.PLineBackingColor)
    % Color to pad lines with
    input.PLineBackingColor = get(ax, 'Color');
end
if isempty(input.PLineBackingWidth)
    input.PLineBackingWidth = 3 * input.PLineWidth;
end
if isempty(input.PLineBacking)
    if input.PLineSourceRelativeSpacing == 0 || ...
            input.PLineSourceRelativeBreadth == 0
        input.PLineBacking = false;
    else
        input.PLineBacking = true;
    end
end

% Fix relative widths
errorbarWidth = input.ErrorbarRelativeWidth * input.BarWidth;
PLineSourceBreadth = input.PLineSourceRelativeBreadth * input.BarWidth;
PLineSourceSpacing = input.PLineSourceRelativeSpacing * input.BarWidth;

% Fix char array input for colours; convert to RGB array
    function C = fix_colors_char(C)
        if ~ischar(C)
            return;
        end
        if numel(C)==1
            C = colorspec2rgb(C);
            return;
        end
        if strcmp(C, 'none')
            return;
        end
        siz = size(C);
        C_out = nan([siz 3]);
        for iCol = 1:siz(2)
            for iRow = 1:siz(1)
                C_out(iRow, iCol, :) = colorspec2rgb(C(iRow, iCol));
            end
        end
        C = C_out;
    end
% Fix cellarray for color input
    function C = fix_colors_cell(C)
        if ~iscell(C)
            return;
        end
        siz = size(C);
        assert(numel(siz)<3, 'Too many dimensions for cellarray C.');
        C_out = nan([siz 3]);
        for iCol = 1:siz(2)
            for iRow = 1:siz(1)
                if ischar(C{iRow, iCol}) && strcmp(C{iRow, iCol}, 'none')
                    % Encode 'none' as NaN. We'll decode it later.
                    C_out(iRow, iCol, :) = NaN;
                elseif ischar(C{iRow, iCol})
                    C_out(iRow, iCol, :) = colorspec2rgb(C{iRow, iCol});
                elseif isnumeric(C{iRow, iCol}) && numel(C{iRow, iCol})==3
                    C_out(iRow, iCol, :) = C{iRow, iCol};
                else
                    error('Cell array must contain only strings and RGB vectors');
                end
            end
        end
        C = C_out;
    end
% Extend colors to be per bar
    function C = extend_colors(C)
        siz = size(Y);
        siz_ = size(C);
        if ~ischar(C)
            assert(length(siz_)<=3, 'Too many dimensions for C.');
            assert(siz_(end)==3, 'Must be RGB color in C with 3 channels.');
        end
        if length(siz_)==2 && ( ~ischar(C) || isequal(C, 'none') )
            C = permute(C, [1, 3, 2]);
        end
        siz_ = size(C);
        C = repmat(C, ceil(siz(1) / siz_(1)), ceil(siz(2) / siz_(2)));
        C = C(1:siz(1), 1:siz(2), :);
    end
input.BarFaceColor = extend_colors(fix_colors_cell(input.BarFaceColor));
input.BarEdgeColor = extend_colors(fix_colors_cell(input.BarEdgeColor));
input.ErrorbarColor = extend_colors(fix_colors_cell(input.ErrorbarColor));

% Map NaN colours back to 'none'
    function C = nan2none(C)
        if all(isnan(C))
            C = 'none';
        end
    end

% Check if hold is already on
wasHeld = ishold(ax);
% If not, clear the axes and turn hold on
if ~wasHeld;
    cla(ax);
    hold(ax, 'on');
end;

nBar = numel(Y);
hb = nan(size(Y));
for iBar=1:nBar
    % Get indices to tell which row and column to take colour from
    [i, j] = ind2sub(size(Y), iBar);
    % Plot bar
    if strncmpi(input.Orientation, 'h', 1)
        hb(iBar) = barh(ax, X(iBar), Y(iBar), input.BarWidth);
    else
        hb(iBar) = bar(ax, X(iBar), Y(iBar), input.BarWidth);
    end
    % Colour it in correctly
    set(hb(iBar), ...
        'FaceColor', nan2none(input.BarFaceColor(i,j,:)), ...
        'EdgeColor', nan2none(input.BarEdgeColor(i,j,:)), ...
        'BaseValue', input.BaseValue, ...
        'LineWidth', input.BarLineWidth);
end
% Add errorbars
if isempty(input.E)
    he = [];
elseif strncmpi(input.Orientation, 'h', 1)
    % Horizontal errorbars
    he = supererr(ax, Y, X, input.E, [], input.ErrorbarStyle, ...
        errorbarWidth, ...
        'Color', input.ErrorbarColor, ...
        'LineWidth', input.ErrorbarLineWidth);
    he = reshape(he(:, 1), size(Y));
else
    % Vertical errorbars
    he = supererr(ax, X, Y, [], input.E, input.ErrorbarStyle, ...
        errorbarWidth, ...
        'Color', input.ErrorbarColor, ...
        'LineWidth', input.ErrorbarLineWidth);
    he = reshape(he(:, 2), size(Y));
end
% Add p-values
if isempty(input.P)
    % Do nothing
    hpt = [];
    hpl = [];
    hpb = [];
elseif numel(input.P)==numel(Y)
    % Add stars above bars
    hpt = plot_p_values_single(ax_or_empty, ...
        X, Y, input.E, input.P, ...
        input.Orientation, input.BaseValue, input.PStarThreshold, ...
        input.PStarOffset, input.PStarShowNS, input.PStarShowGT, ...
        input.PStarFixedOrientation, input.PStarIcon, input.PStarLatex, ...
        {'Color', input.PStarColor, ...
        'FontSize', input.PStarFontSize, ...
        'BackgroundColor', input.PStarBackgroundColor});
    hpl = [];
    hpb = [];
elseif numel(input.P)==numel(Y)^2
    % Add lines and stars between pairs of bars
    [hpt, hpl, hpb] = plot_p_values_pairs(ax_or_empty, ...
        X, Y, input.E, input.P, ...
        input.Orientation, input.PStarThreshold, input.PLineOffset, ...
        input.PLineOffsetSource, input.PStarOffset, input.PStarShowNS, ...
        input.PStarShowGT, PLineSourceSpacing, PLineSourceBreadth, ...
        input.PLineBacking, input.PStarFixedOrientation, input.PStarIcon, ...
        input.PStarLatex, ...
        {'Color', input.PLineColor, ...
        'LineWidth', input.PLineWidth}, ...
        {'Color', input.PLineBackingColor, ...
        'LineWidth' input.PLineBackingWidth}, ...
        {'Color', input.PStarColor, ...
        'FontSize', input.PStarFontSize, ...
        'BackgroundColor', input.PStarBackgroundColor});
else
    error('Bad number of P-values');
end

% If hold was off, turn it off again
if ~wasHeld; hold(ax, 'off'); end;

if nargout==0
    varargout = {};
else
    varargout = {hb, he, hpt, hpl, hpb};
end

end


%colorspec2rgb
%   Convert a color string to an RGB value.
%          b     blue
%          g     green
%          r     red
%          c     cyan
%          m     magenta
%          y     yellow
%          k     black
%          w     white
function color = colorspec2rgb(color)

% Define the lookup table
rgb = [1 0 0; 0 1 0; 0 0 1; 1 1 1; 0 1 1; 1 0 1; 1 1 0; 0 0 0];
colspec = 'rgbwcmyk';

idx = find(colspec==color(1));
if isempty(idx)
    error('colorstr2rgb:InvalidColorString', ...
        'Unknown color string: %s.', color(1));
end

if idx~=3 || length(color)==1,
    color = rgb(idx, :);
elseif length(color)>2,
    if strcmpi(color(1:3), 'bla')
        color = [0 0 0];
    elseif strcmpi(color(1:3), 'blu')
        color = [0 0 1];
    else
        error('colorstr2rgb:UnknownColorString', 'Unknown color string.');
    end
end

end


%bar2grouped
%   Split vector X to position all bars in a group into appropriate places.
function [X, Y, width] = bar2grouped(X, Y, width, group_width)

% Parameters
if nargin<4
    group_width = 0.75;
end

nElePerGroup = size(Y, 2);

if nElePerGroup==1
    % No need to do anything to X as the groups only contain one element
    return;
end

if size(X, 1)==1
    % Transpose X if necessary
    X = X';
end
if ~ismatrix(X) || size(X, 2)>1
    error('X must be a column vector.')
end

% Compute the offset for each bar, such that they are centred correctly
dX = width / nElePerGroup * ((0:nElePerGroup-1) - (nElePerGroup-1)/2);
% Apply the offset to each bar in X
X = bsxfun(@plus, X, dX);
% Reduce width of bars so they only take up group_width as much space, and
% divide what there is evenly between the bars per group
width = width * group_width / nElePerGroup;

end


%plot_p_values_single
%   Plot stars above bars to indicate which are statistically significant.
%   Can be used with bars in either horizontal or vertical direction.
function h = plot_p_values_single(ax, X, Y, E, P, orientation, baseval, ...
    p_threshold, offset, show_ns, show_gt, fixed_text_orientation, ...
    star_icon, use_latex, text_args)

if isempty(E)
    E = zeros(size(Y));
end

% Validate inputs
assert(numel(X)==numel(Y), 'Number of datapoints mismatch {X,Y}.');
assert(numel(X)==numel(E), 'Number of datapoints mismatch {X,E}.');
assert(numel(X)==numel(P), 'Number of datapoints mismatch {X,P}.');
assert(all(E(:) >= 0), 'Error must be a non-negative value.');
assert(offset > 0, 'Offset must be a positive value.');

% Deal with axes
if ~isempty(ax)
    % We can't pass an axes argument to the line function because it only
    % became available in R2016a, so instead we change axes if necessary.
    % Make a cleanup object to revert focus back to previous axes
    prev_ax = gca();
    finishup = onCleanup(@() axes(prev_ax));
    % Change focus to the axes we want to work on
    axes(ax);
end

% Loop over every bar
h = nan(size(X));
for i=1:numel(X)
    % Check how many stars to put in the text
    num_stars = sum(P(i) <= p_threshold);
    str = char(repmat(star_icon, 1, num_stars));
    % Check whether to include a > sign too
    if show_gt && all(P(i) < p_threshold) && numel(str) > 1
        str = ['>' str(1:end-1)];
    end
    % Wrap for LaTeX, if its on and we have something to wrap
    if ~strcmp(use_latex, 'off') && ~isempty(str)
        str = ['$' str '$'];
    end
    % Check whether to write n.s. above non-significant bars
    if show_ns && num_stars == 0 && ~isnan(P(i))
        str = 'n.s.';
    end
    % Work out where to put the text
    x = X(i);
    y = Y(i);
    if strncmpi(orientation, 'h', 1)
        % Swap x and y
        tmp = x;
        x = y;
        y = tmp;
        if fixed_text_orientation
            HorizontalAlignment = 'left';
            rotation = 0;
        else
            HorizontalAlignment = 'center';
            rotation = 270;
        end
        % Add on the offset to the x co-ordinate
        if x >= baseval
            x = x + E(i) + offset;
        else
            x = x - E(i) - offset;
        end
    else
        HorizontalAlignment = 'center';
        rotation = 0;
        if y >= baseval
            y = y + E(i) + offset;
        else
            y = y - E(i) - offset;
        end
    end
    % Add the text for the stars
    h(i) = text(x, y, str, ...
        'HorizontalAlignment', HorizontalAlignment, ...
        'VerticalAlignment', 'middle', ...
        'Rotation', rotation, ...
        text_args{:});
    % Change the interpreter to LaTeX, if desired
    if strcmp(use_latex, 'all') || ...
            (strcmp(use_latex, 'auto') && num_stars > 0)
        set(h(i), 'Interpreter', 'latex');
    end
end

end


%plot_p_values_pairs
%   Plot lines and stars to indicate pairwise comparisons and whether they
%   are significant. Only works for error bars in the Y-direction.
function [ht, hl, hbl] = plot_p_values_pairs(ax, X, Y, E, P, orientation, ...
    p_threshold, offset, source_offset, star_offset, show_ns, show_gt, ...
    max_dx_single, max_dx_full, pad_lines, fixed_text_orientation, ...
    star_icon, use_latex, line_args, pad_args, text_args)

if isempty(E)
    E = zeros(size(Y));
end

% Validate inputs
N = numel(X);
assert(numel(Y)==N, 'Number of datapoints mismatch {X,Y}.');
assert(numel(E)==N, 'Number of datapoints mismatch {X,E}.');
assert(numel(P)==N^2, 'Number of datapoints mismatch {X,P}.');

% Deal with axes
if ~isempty(ax)
    % We can't pass an axes argument to the line function because it only
    % became available in R2016a, so instead we change axes if necessary.
    % Make a cleanup object to revert focus back to previous axes
    prev_ax = gca();
    finishup = onCleanup(@() axes(prev_ax));
    % Change focus to the axes we want to work on
    axes(ax);
end

% Turn into vectors
X = X(:);
Y = Y(:);
E = E(:);
P = reshape(P, N, N);
assert(all(all(P==P' | isnan(P))), 'P must be symmetric between pairs');

% Sort by bar location
[X, IX] = sort(X);
Y = Y(IX);
E = E(IX);
P = P(IX, IX);

% Ensure P is symmetric
P = max(P, P');
% Remove lower triangle
P(logical(tril(ones(size(P))))) = NaN;

% Find the max of each pair of bars
pair_max_y = max(repmat(Y + E, 1, N), repmat(Y' + E', N, 1));
% Find the distance between the bars
pair_distance = abs(repmat(X, 1, N) - repmat(X', N, 1));
% Remove pairs which are not measured
li = isnan(P);
pair_max_y(li) = NaN;
pair_distance(li) = NaN;

% Sort by maximum value, smallest first
[~, I1] = sort(pair_max_y(:));
pair_distance_tmp = pair_distance(I1);
% Sort by pair distance
[~, I2] = sort(pair_distance_tmp);
% Combine the two mappings into a single indexing step
IS = I1(I2);
% Now we have primary sort by pair_distance and secondary sort by max value
[ISi, ISj] = ind2sub(size(pair_distance), IS);

% For each bar, check how many lines there will be
num_comp_per_bar = sum(~isnan(max(P, P')), 2);
dX_list = nan(size(P));
for i=1:numel(X)
    dX_list(1:num_comp_per_bar(i), i) = ...
        (0:(num_comp_per_bar(i)-1)) - (num_comp_per_bar(i)-1) / 2;
end
dX_each = min(max_dx_single, max_dx_full / max(num_comp_per_bar));
dX_list = dX_list * dX_each;

% Minimum value for lines over each bar
YEO = Y + E;
current_height = repmat(YEO(:)', N, 1);

% Loop over every pair with a measurement
num_comparisons = sum(~isnan(P(:)));
hl = nan([size(P), 2]);
ht = nan(size(P));
hbl = nan(size(P));
coords = nan(4, 2, num_comparisons);
for iPair=1:num_comparisons
    % Get index of left and right pairs
    i = min(ISi(iPair), ISj(iPair));
    j = max(ISi(iPair), ISj(iPair));
    % Check we're not failing terribly
    if isnan(P(i,j))
        error('This shouldnt be NaN!');
    end
    % Check which bar origin point we're up to
    il = find(~isnan(dX_list(:, i)), 1, 'last');
    jl = find(~isnan(dX_list(:, j)), 1, 'first');
    % Offset the X value to get the non-intersecting origin point
    xi = X(i) + dX_list(il, i);
    xj = X(j) + dX_list(jl, j);
    % Clear these origin points so they aren't reused
    dX_list(il, i) = NaN;
    dX_list(jl, j) = NaN;
    xx = [xi, xi, xj, xj];
    % Work out how high the line must be
    if dX_each==0
        yi = current_height(il, i) + source_offset;
        yj = current_height(jl, j) + source_offset;
    else
        yi = YEO(i) + source_offset;
        yj = YEO(j) + source_offset;
    end
    % It must be higher than all intermediate lines; check which these are
    if dX_each==0
        intermediate_index = (1 + N*(i-1)) : ( N*j );
    else
        intermediate_index = (il + N*(i-1)) : (jl + N*(j-1));
    end
    % Also offset so we are higher than these lines
    y_ = max(current_height(intermediate_index)) + offset;
    yy = [yi, y_, y_, yj];
    % Update intermediates so we know the new hight above them
    current_height(intermediate_index) = y_;
    % Save the co-ordinates to plot later
    coords(:, 1, iPair) = xx;
    coords(:, 2, iPair) = yy;
end

% Plot backing, text and comparison lines
for iPair=num_comparisons:-1:1
    % Get index of left and right pairs
    i = min(ISi(iPair), ISj(iPair));
    j = max(ISi(iPair), ISj(iPair));
    % Get co-ordinates back again
    if strncmpi(orientation, 'h', 1)
        xx = coords(:, 2, iPair);
        yy = coords(:, 1, iPair);
        x_offset = star_offset;
        y_offset = 0;
        if fixed_text_orientation
            HorizontalAlignment = 'left';
            rotation = 0;
        else
            HorizontalAlignment = 'center';
            rotation = 270;
        end
    else
        xx = coords(:, 1, iPair);
        yy = coords(:, 2, iPair);
        x_offset = 0;
        y_offset = star_offset;
        HorizontalAlignment = 'center';
        rotation = 0;
    end
    % Draw the backing line
    if pad_lines
        hbl(i, j) = line(xx([2 3]), yy([2 3]), pad_args{:});
    end
    % Check how many stars to put in the text
    num_stars = sum(P(i, j) <= p_threshold);
    str = char(repmat(star_icon, 1, num_stars));
    % Check whether to include a > sign too
    if show_gt && all(P(i, j) < p_threshold) && numel(str) > 1
        str = ['>' str(1:end-1)];
    end
    % Wrap for LaTeX, if its on and we have something to wrap
    if ~strcmp(use_latex, 'off') && ~isempty(str)
        str = ['$' str '$'];
    end
    % Check whether to write n.s. above non-significant comparisons
    if show_ns && num_stars == 0
        str = 'n.s.';
    end
    % Add the text for the stars, slightly above the middle of the line
    ht(i, j) = text(...
        mean(xx([2 3])) + x_offset, mean(yy([2 3])) + y_offset, ...
        str, ...
        'HorizontalAlignment', HorizontalAlignment, ...
        'VerticalAlignment', 'middle', ...
        'Rotation', rotation, ...
        text_args{:});
    % Change the interpreter to LaTeX, if desired
    if strcmp(use_latex, 'all') || ...
            (strcmp(use_latex, 'auto') && num_stars > 0)
        set(ht(i, j), 'Interpreter', 'latex');
    end
    % Draw the main lines
    hl(i, j, 1) = line(xx, yy, line_args{:});
end

% Plot the horizontal lines again, on top of everything else (specifically
% so they are on top of the text)
for iPair=num_comparisons:-1:1
    % Get index of left and right pairs
    i = min(ISi(iPair), ISj(iPair));
    j = max(ISi(iPair), ISj(iPair));
    % Get co-ordinates back again
    if strncmpi(orientation, 'h', 1)
        xx = coords(:, 2, iPair);
        yy = coords(:, 1, iPair);
    else
        xx = coords(:, 1, iPair);
        yy = coords(:, 2, iPair);
    end
    % Draw the horizontal lines again on top of everything else
    hl(i, j, 2) = line(xx([2 3]), yy([2 3]), line_args{:});
end

end

function varargout = supererr(X, Y, XE, YE, X_style, Y_style, cap_width, ...
    varargin)

VALID_STYLES = 'I|T''=_';
COLOR_SPECS = 'rgbwcmyk';
DEFAULT_STYLE = 'I';

% Check number of inputs is okay
if exist('narginchk', 'builtin')
    narginchk(3, Inf);
elseif abs(nargin) < 3
    error('MATLAB:narginchk:notEnoughInputs', 'Not enough input arguments.');
end

% Extend the reach of varargin
if nargin>=7
    varargin = [{cap_width}, varargin];
end
if nargin>=6
    varargin = [{Y_style}, varargin];
end
if nargin>=5
    varargin = [{X_style}, varargin];
end
if nargin>=4
    varargin = [{YE}, varargin];
end
% Must be at least 3 input arguments
varargin = [{X, Y, XE}, varargin];

% Strip out axes input if it is there
[ax, varargin, nargs] = axescheck(varargin{:});
if ~isempty(ax)
    % We can't pass an axes argument to the line function because it only
    % became available in R2016a, so instead we change axes if necessary.
    % Make a cleanup object to revert focus back to previous axes
    prev_ax = gca();
    finishup = onCleanup(@() axes(prev_ax));
    % Change focus to the axes we want to work on
    axes(ax);
end
% Check number of inputs is still okay
if nargs<3
    error('Must provide at least 3 input arguments, in addition to axes.');
end
% Read out parameters with ax potentially removed from args
X = varargin{1};
Y = varargin{2};
XE = varargin{3};

% Default inputs for XE and YE
if isempty(XE)
    XE = nan(numel(X), 1);
end
if nargs<4 || isempty(varargin{4})
    YE = nan(numel(X), 1);
else
    YE = varargin{4};
end

% Just keep the rest of the arguments
varargin = varargin(5:end);

% Default args
X_style = '';
Y_style = '';
cap_width = [];

% Function to check that style is valid
    function tf = check_is_style(t)
        if numel(t)==1
            tf = ismember(t, VALID_STYLES) || ismember(t, COLOR_SPECS);
        elseif numel(t)==2
            tf = (...
                ismember(t(1), VALID_STYLES) && ismember(t(2), COLOR_SPECS) ...
                ) || (...
                ismember(t(2), VALID_STYLES) && ismember(t(1), COLOR_SPECS) ...
                );
        else
            tf = false;
        end
    end
% Parse the style and cap_width components out of varargin
numeric_in = 0;
string_in = 0;
for iArg=1:min(3, numel(varargin))
    if isnumeric(varargin{iArg})
        if numeric_in>0
            break;
        end
        cap_width = varargin{iArg};
        numeric_in = numeric_in + 1;
    elseif ischar(varargin{iArg}) && ...
            (isempty(varargin{iArg}) || check_is_style(varargin{iArg}))
        if string_in==0
            X_style = varargin{iArg};
        elseif string_in==1
            Y_style = varargin{iArg};
        else
            break;
        end
        string_in = string_in + 1;
    else
        break;
    end
end
% Cut away the style and cap_width inputs
varargin = varargin((numeric_in + string_in + 1):end);

% Default inputs
if isempty(cap_width)
    cap_width = 0.5;
end
if isempty(X_style)
    X_style = DEFAULT_STYLE;
end
if isempty(Y_style)
    Y_style = X_style;
end

% Check inputs
assert(ischar(X_style), 'X_style must be a char string.');
assert(ischar(Y_style), 'Y_style must be a char string.');
% Set default colors
X_color = [.2 .2 .2];
Y_color = [.2 .2 .2];
% Check if style contains color spec
if ~isempty(X_style)
    idx = find(ismember(X_style, VALID_STYLES), 1, 'last');
    if isempty(idx)
        X_color = X_style;
        X_style = DEFAULT_STYLE;
    elseif numel(idx) < numel(X_style)
        X_color = X_style([1:idx-1, idx+1:end]);
        X_style = X_style(idx);
    end
end
if ~isempty(Y_style)
    idx = find(ismember(Y_style, VALID_STYLES), 1, 'last');
    if isempty(idx)
        Y_color = Y_style;
        Y_style = DEFAULT_STYLE;
    elseif numel(idx) < numel(Y_style)
        Y_color = Y_style([1:idx-1, idx+1:end]);
        Y_style = Y_style(idx);
    end
end
% Check if varargin contains colors
is_color_arg = false(size(varargin));
for iArg = 1:numel(varargin)-1
    if isequal(varargin{iArg}, 'Color')
        is_color_arg(iArg:(iArg+1)) = true;
        X_color = varargin{iArg+1};
        Y_color = varargin{iArg+1};
    elseif isequal(varargin{iArg}, 'XColor')
        is_color_arg(iArg:(iArg+1)) = true;
        X_color = varargin{iArg+1};
    elseif isequal(varargin{iArg}, 'YColor')
        is_color_arg(iArg:(iArg+1)) = true;
        Y_color = varargin{iArg+1};
    end
end
% Remove color arguments from varargin
varargin = varargin(~is_color_arg);

% Replicate single inputs so there is a copy for every error bar
if numel(XE)==1
    XE = XE * ones(numel(X), 1);
end
if numel(YE)==1
    YE = YE * ones(numel(X), 1);
end
if numel(XE)==2 && (numel(X)~=2 || size(XE,1)==1)
    XE = repmat(XE(:)', numel(X), 1);
end
if numel(YE)==2 && (numel(X)~=2 || size(YE,1)==1)
    YE = repmat(YE(:)', numel(X), 1);
end
% Reshape colour arguments
if ischar(X_color)
    X_color = X_color(:);
else
    assert(size(X_color, ndims(X_color))==3, ...
        'Last dimension must be RGB channel.')
    X_color = reshape(X_color, numel(X_color)/3, 3);
end
if ischar(Y_color)
    Y_color = Y_color(:);
else
    assert(size(Y_color, ndims(Y_color))==3, ...
        'Last dimension must be RGB channel.')
    Y_color = reshape(Y_color, numel(Y_color)/3, 3);
end

% Check inputs are okay
assert(numel(X)==numel(Y), 'X and Y need to be the same size.');

check_e = @(t) (isempty(t) || ...
    (size(t,1)==numel(X) && ismatrix(t) && (size(t,2)==1 || size(t,2)==2)));
assert(check_e(XE), 'Input XE is badly formatted.');
assert(check_e(YE), 'Input YE is badly formatted.');
assert(all(XE(:)>=0 | isnan(XE(:))), 'Input XE must be non-negative');
assert(all(YE(:)>=0 | isnan(YE(:))), 'Input YE must be non-negative');

assert(isnumeric(cap_width), 'Cap width must be a double.');
assert(numel(cap_width)==1, 'Input cap_width must be a scalar.');

assert(ischar(X_style), 'X_style must be a string');
assert(ischar(Y_style), 'Y_style must be a string');
assert(all(ismember(X_style, VALID_STYLES)), ...
    'X_style must be one of %s', VALID_STYLES);
assert(all(ismember(Y_style, VALID_STYLES)), ...
    'Y_style must be one of %s', VALID_STYLES);

% Main
H = nan(numel(X), 2);
for i = 1:numel(X)
    % Include co-ordinates for X error bar
    if ~all(isnan(XE(i,:)))
        % Take the x and y co-ordinates the other way around, since this is
        % an x errobar
        [yco, xco] = compute_errorbar_relative_coordinates(...
            XE(i,:), X_style, cap_width);
        % Shift co-ordinates to centre at the correct location
        xco = xco + X(i);
        yco = yco + Y(i);
        % Let the color loop around if it runs out
        iCol = 1 + mod(i-1, size(X_color, 1));
        % Draw the errorbar with line
        H(i, 1) = line(xco, yco, 'Color', X_color(iCol, :), varargin{:});
    end
    % Include co-ordinates for Y error bar
    if ~all(isnan(YE(i,:)))
        [xco, yco] = compute_errorbar_relative_coordinates(...
            YE(i,:), Y_style, cap_width);
        % Shift co-ordinates to centre at the correct location
        xco = xco + X(i);
        yco = yco + Y(i);
        % Let the color loop around if it runs out
        iCol = 1 + mod(i-1, size(Y_color, 1));
        % Draw the error bar with line
        H(i, 2) = line(xco, yco, 'Color', Y_color(iCol, :), varargin{:});
    end
end

if nargout==0
    varargout = {};
else
    varargout = {H};
end

end


%compute_errorbar_relative_coordinates
%   Computes the co-ordinates of an error bar in the y-direction, centred
%   around (0,0) with style given by the input style. E input is the error
%   on the value, which can be either a single value or two values. In the
%   former case, the error bar can be either drawn in both directions the
%   same length or only one. If E contains two values, the error bar is
%   always drawn in both directions, with different lengths.
function [xx, yy] = compute_errorbar_relative_coordinates(E, style, width)

style = upper(style);

xx = [];
yy = [];

% Add co-ordinates for the stave
if numel(E)>1 && ismember(style, 'I|T''')
    % Asymmetric stave
    xx = [xx,     0,    0, NaN];
    yy = [yy, -E(1), E(2), NaN];
elseif numel(E)==1 && ismember(style, 'I|')
    % Symmetric stave
    xx = [xx,  0, 0, NaN];
    yy = [yy, -E, E, NaN];
elseif numel(E)==1 && ismember(style, 'T''')
    % Half stave
    xx = [xx, 0, 0, NaN];
    yy = [yy, 0, E, NaN];
end
% Don't draw caps on if they don't have any width
if all(width==0)
    return;
end
% Duplicate width if there is only one input
if numel(width)==1
    width = [1, 1] * width;
end
% Halve the width, so we have the amount on each side
width = width / 2;
% Add co-ordinates for the caps
if numel(E)>1 && ismember(style, 'IT=_')
    % Asymmetric caps
    xx = [xx, -width(1), width(1), NaN, -width(2), width(2), NaN];
    yy = [yy,     -E(1),    -E(1), NaN,      E(2),     E(2), NaN];
elseif numel(E)==1 && ismember(style, 'I=')
    % Symmetric caps
    xx = [xx, -width(1), width(1), NaN, -width(2), width(2), NaN];
    yy = [yy,        -E,       -E, NaN,         E,        E, NaN];
elseif numel(E)==1 && ismember(style, 'T_')
    % Single cap
    xx = [xx, -width(2), width(2), NaN];
    yy = [yy,         E,        E, NaN];
end

end

