% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function TCT=fgAusw_readsaveVGcsv(ordnAusw,nmAuswGesCT,...
    varargin)
% Lese alle VG Studios CSVs aus einem Ordner ein und f�hre sie in einer
% Excel zusammen

%% Parse die Eingangsargumente
p = inputParser;

defaultSpalteMerkmale = 1;
checkSpalteMerkmale = @isnumeric;
addOptional(p,'SpalteMerkmale',defaultSpalteMerkmale,checkSpalteMerkmale)

defaultNameSpalteMerkmale = 'Istwert';
addOptional(p,'NameSpalteMerkmale',defaultNameSpalteMerkmale)

parse(p,varargin{:})

spM=p.Results.SpalteMerkmale;
nmSpM=p.Results.NameSpalteMerkmale;

%% Lese die Daten ein

% Filenamen
dirCT=dir([ordnAusw,'*.csv']);
nmsCT=string({dirCT(:).name});

% Einlesen CT
TwerteCT=cellfun(@(x) readtable([dirCT(1).folder,'/',x]),nmsCT,...
    'UniformOutput',false);
varNm=TwerteCT{1}.Properties.VariableNames;
spCT=contains(varNm,nmSpM);

% Nur Spalte mit Messwerten ausw�hlen und in eine Tabelle bringen
werteCT=cellfun(@(x) table2array(x(:,spCT)),TwerteCT,'UniformOutput',false);
werteCT=string(horzcat(werteCT{:}));

% Einheit l�schen und "," in "." umwandeln
werteCT=strrep(werteCT,' mm',''); 
werteCT=strrep(werteCT,' �',''); 
werteCT=strrep(werteCT,',','.');

% Matrix erstellen
werteCT=double(werteCT);

% Gesamttabelle mit Zeilen und Spaltennamne erstellen
nmsTCT=strrep(nmsCT,'.csv',''); 
nmsTCT=strrep(nmsTCT,'3d',''); nmsTCT=strrep(nmsTCT,' ','_');
zlnm=readtable([dirCT(1).folder,'/',nmsCT{1}]); zlnm=zlnm{:,'Name'};
TCT = array2table(werteCT,'VariableNames',nmsTCT,'RowNames',zlnm);

% Endergebnis schreiben
TCT.Properties.DimensionNames{1}='Merkmal';
if exist([ordnAusw,'/',nmAuswGesCT,'.xlsx'],'file') 
    delete([ordnAusw,'/',nmAuswGesCT,'.xlsx']); end
writetable(TCT,[ordnAusw,'/',nmAuswGesCT,'.xlsx'],'WriteRowNames',true);

end