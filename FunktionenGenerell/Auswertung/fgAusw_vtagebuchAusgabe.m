% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function RCK=fgAusw_vtagebuchAusgabe(strVT,id,spNm)
%Aus einem Versuchstagebuch die �ber spNm spezifizierten Spaltenwerte aus
%der mit id spezifizierten Zeile zur�ckgeben

% Einlesen des Versuchstagebuches
Tvt=readtable(strVT);
zlID=strcmp(Tvt.VersuchID,id);

if any(zlID)
    % Auslesen
    nsp=length(spNm);
    RCK=cell(nsp,1);
    for i=1:nsp
        
        % Aktuelle Spalte durchsuchen
        spNm_i=spNm{i};
        if any(strcmp(Tvt.Properties.VariableNames,spNm_i))
            % Es gibt die spezifizierte Spalte
            RCK{i}=Tvt.(spNm_i)(zlID);
        else
            % Es gibt sie nicht
            warning(['Die Spalte ',spNm_i,' gibt es nicht. NaN R�ckgabewert'])
            RCK{i}=NaN;
        end
    end
else
    % Die Zeile gibt es nicht
    warning(['Die gesuchte Zeile gibt es nicht. [] R�ckgabewert'])
    RCK=[];
end


end