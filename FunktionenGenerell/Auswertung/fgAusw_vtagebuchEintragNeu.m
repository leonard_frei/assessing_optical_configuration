% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function Tvt=fgAusw_vtagebuchEintragNeu(Tvt,name,merkm,werte)
% Neue Einträge zu Versuchstagebuch hinzufügen

% Ueberpruefen, dass Variablen nicht mit Zahl beginnen
if any(~cellfun(@isvarname, merkm))
    error('Merkmalnamen ungeeignet. Diese muessen mit Buchstaben beginnen und duerfen keine Umlaute enthalten.'); 
end

% Werte als stehende Vektoren einlesen
if min(size(werte))~=1
    error('Nur Vektoren können als Werte uebergeben werden');
elseif size(werte,2)>1
    werte=werte';
end

% Spaltennamen und Anzahl der Einträge aus Versuchstagebuch auslesen
spNm=Tvt.Properties.VariableNames;
neintr=size(Tvt,1); nsp=size(Tvt,2);

% Index des aktuellen Eintrags finden
indEintr=find(strcmp(Tvt.VersuchID,name)); neuEintr=false;
if isempty(indEintr); neuEintr=true; indEintr=neintr+1; end

% Vorhandene Merkmale und Werte bestimmen
indMerkmVor=cellfun(@(x) any(strcmp(spNm',x)), merkm);
merkmVor=merkm(indMerkmVor);
werteVor=werte(indMerkmVor);

% Neue Werte bestimmen
merkmNeu=merkm(~indMerkmVor); nspNeu=length(merkmNeu);
werteNeu=werte(~indMerkmVor);

% Neuen Eintrag (Zeile) erstellen oder bestehenden erneuern
if neuEintr
    % Es muss ein neuer Eintrag erstellt werden
    
    % Tabelle fuer vorhandene Spalten anlegen
    TvtVor=Tvt(1,:);
    for i=1:nsp
        if iscell(TvtVor{1,i}); TvtVor{1,i}={' '};
        elseif isnumeric(TvtVor{1,i}); TvtVor{1,i}=0; end
    end
    
    % Tabelle für vorhandene Werte beschreiben
    if isempty(merkmVor); TvtVor{1,1}={name};
    else; TvtVor{1,1}={name}; TvtVor{1,merkmVor}=werteVor'; end
    
    % Zeile hinzufügen
    Tvt=[Tvt; TvtVor];
    
    % Tabelle für neue Spalten anlegen
    if isnumeric(werteNeu)
        werteNeuM=zeros(neintr+1,nspNeu);
        werteNeuM(indEintr,:)=werteNeu';
        TvtNeu=array2table(werteNeuM,'VariableNames',merkmNeu);
    else
        werteNeuM=cell(neintr+1,nspNeu);
        werteNeuM(indEintr,:)=werteNeu';
        TvtNeu=array2table(werteNeuM,'VariableNames',merkmNeu);
    end
    
    % Spalten hinzufügen
    Tvt=[Tvt, TvtNeu];
    
else
    % Es besteht schon ein Eintrag
    
    % Tabelle für vorhandene Spalten beschreiben
    if isempty(merkmVor); Tvt{indEintr,1}={name};
    else; Tvt{indEintr,1}={name}; Tvt{indEintr,merkmVor}=werteVor'; end
    
    % Tabelle für neue Spalten anlegen
    if isnumeric(werteNeu)
        werteNeuM=zeros(neintr,nspNeu);
        werteNeuM(indEintr,:)=werteNeu';
        TvtNeu=array2table(werteNeuM,'VariableNames',merkmNeu);
    else
        werteNeuM=cell(neintr,nspNeu);
        werteNeuM(indEintr,:)=werteNeu';
        TvtNeu=array2table(werteNeuM,'VariableNames',merkmNeu);
    end
    
    % Spalten hinzufügen
    Tvt=[Tvt, TvtNeu];
    
end

end