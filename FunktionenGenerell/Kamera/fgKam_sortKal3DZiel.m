% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [xyKls,rKls]=fgKam_sortKal3DZiel(Aufn, xyKl,radiiKl,centerGr,...
    grAufn,feldGr)

xyGes=[xyKl; centerGr];

% Berechne die Mittelpunktskoordinaten aller �bergebenen Kreise
xyGesM=mean(xyGes);

% Berechnen den Distanzvektor aller Kreise zum Mittelpunkt
diffM=(xyGes(:,1)-xyGesM(:,1)).^2+(xyGes(:,2)-xyGesM(:,2)).^2;

% Berechne die  4 Kreise mit dem gr��ten Abstand zum Mittelpunkt
% --> Ecken
[~, indE]=maxk(diffM,4);
xy0=xyGes(indE,:);

% Sortiere sie 
xym=mean(xy0); 
w=atan2(xy0(:,2)-xym(2),xy0(:,1)-xym(1));
[ws,inds]=sort(w); xy0=xy0(inds,:);

% Finde die gro�e obere Bohrung und ordne nach ihr
[X1, X2]=meshgrid(xy0(:,1),centerGr(:,1));
[Y1, Y2]=meshgrid(xy0(:,2),centerGr(:,2));
DX=X1-X2; DY=Y1-Y2; A=sqrt(DX.^2+DY.^2);
[~,ind0Gr]=find(A==0);
ind0=[ind0Gr:4,1:ind0Gr-1];
xys0=xy0(ind0,:); 

% Berechne die Homographie 
abstv=feldGr(1); absth=feldGr(2); 
xy0Zeichn=[0 0; 0 abstv; absth abstv; absth 0];
v = homography_solve(xys0, xy0Zeichn);
xyKlZeichn = homography_transform(xyKl, v);

% Sortiere die transformierten Mittelpunkte
[~,inds]=sort(xyKlZeichn(:,1)+xyKlZeichn(:,2)*absth); 

% Verwende die Sortierung in der Transformation f�r die eigentlichen Punkte
xyKls=xyKl(inds,:); rKls=radiiKl(inds,:);
 
end