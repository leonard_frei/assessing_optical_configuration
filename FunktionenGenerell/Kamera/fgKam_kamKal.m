% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [kamPara,AP,WP,Abw,parBV]=fgKam_kamKal(AufnKal,varargin)
% Eine oder zwei Kameras kalibrieren

%% Parsen
p = inputParser;

defaultSpAufn=false;
addOptional(p,'spAufn',defaultSpAufn);

defaultKalConf='schachbrett';
addOptional(p,'zielKonfg',defaultKalConf);

defaulWpAkq='schachbrett';
addOptional(p,'wpAkq',defaulWpAkq);

defaulKalAlgo='Matlab';
addOptional(p,'kalAlgo',defaulKalAlgo);

defaultSqSize=15;
addOptional(p,'sqSize',defaultSqSize);

defaultRPx=[20 40];
addOptional(p,'rpx',defaultRPx);

defaultParKrs=0;
addOptional(p,'parKrs',defaultParKrs);

defaultSpStrWP='20200303_3DKalibrierzielKMG.mat';
addOptional(p,'spStrWP',defaultSpStrWP);

defaultAbwBr=false;
addOptional(p,'abwBr',defaultAbwBr);

defaultVisAbw=true;
addOptional(p,'visAbw',defaultVisAbw);

defaultParBV=struct;
addOptional(p,'parBV',defaultParBV);

parse(p,varargin{:})

zielKonfg=p.Results.zielKonfg;
wpAkq=p.Results.wpAkq;
kalAlgo=p.Results.kalAlgo;
sqSize=p.Results.sqSize;
rPx=p.Results.rpx;
spAufn=p.Results.spAufn;
parKrs=p.Results.parKrs;
spStrWP=p.Results.spStrWP;
abwBr=p.Results.abwBr;
parBV=p.Results.parBV;


%% Bildpunkte extrahieren

% Anzahl der Kamera bestimmen
szA=size(AufnKal); grA=szA(1:2); na=szA(4); nkam=size(AufnKal,5);

% Ausw�hlen, welche Konfiguration des Kalibrierziel hatte
switch zielKonfg
    
    case 'schachbrett'
        % Schachbrett mit Matlab-Algorithmus detektieren
        
        for i=1:nkam
            
            % Aufnahmen extrahieren
            AufnKal_i=AufnKal(:,:,:,:,i);
            
            % Schachpunkte detektieren
            [AP_ij,bgr,vA] = detectCheckerboardPoints(AufnKal_i);
            AP(:,:,:,i)=AP_ij;
            
        end
        
    case '3DStufen'
        % 3D Stufenziel detektieren
        
        if (size(parKrs,2)~=na); parKrs=ones(nkam,na)*parKrs; end
        for i=1:nkam
            
            for j=1:na
                % Aufnahmen und Parameter extrahieren
                AufnKal_ij=AufnKal(:,:,:,j,i);
                parKrs_ij=parKrs(i,j);
                
                % Schachpunkte detektieren
                [AP_ij, rGes_ij, parKrs_ij]=...
                    fgKam_erkenn3DZiel(AufnKal_ij,rPx,'krs',parKrs_ij);
                AP(:,:,j,i)=AP_ij; parKrs(i,j)=parKrs_ij;
            end
            
        end
        
        % �bergabe der Bildverarbeitungsparameter
        parBV.krs=parKrs;
        
        % Sicherstellen, dass
        if ~strcmp(wpAkq,'laden')
            warning(['F�r das Stufenziel m�ssen WP geladen werden.', ...
                'Standardwerte werden geladen: ', defaultSpStrWP]);
            wpAkq='laden';
        end
        
        if ~strcmp(kalAlgo,'Heikillae')
            warning(['F�r das Stufenziel muss die Haikillae Toolbox ', ...
                'verwendet werden. Wird so eingestellt']);
            kalAlgo='Heikillae';
        end
        
end

%% Weltpunkte akquieren

% Ausw�hlen, wie die Weltpunkte akquiriert werden
switch wpAkq
    
    case 'schachbrett'
        % Schachbrett berechnen
        WP = generateCheckerboardPoints(bgr,sqSize);
        
    case 'laden'
        % Weltpunkte aus der Kalibrierung laden
        load(spStrWP);
        WP=Tges{:,1:3};
end

%% Kalibrierung

% Richtigen Kalibrieralgorithmus ausw�hlen
switch kalAlgo
    case 'Matlab'
        
        % Kalibrierung mit Matlab-Algorithmus
        kamPara=estimateCameraParameters(AP,WP);
        
    case 'Heikillae'
        
        % Kalibrierung mit Matlab-Algo
        [kamPara,parl,parr]=fgKam_kamKalHeik(AP,WP,grA);
end

%% Berechnung der Abweichung

if abwBr
    
    % Indizes f�r Achsen
    switch zielKonfg
        case 'schachbrett'
            
            % Kalibrierung mit Matlab-Algorithmus
            indAchse=bgr(1)-1;
            
        case '3DStufen'
            
            % Kalibrierung mit Matlab-Algo
            indAchse=[6 length(WP)/2];
    end
    
    % Berechnung der Abweichung
    switch kalAlgo
        case 'Matlab'
            % Mit Matlab Kalibrierung
            
            Abw=fgKam_wpAbw(kamPara,AP,WP,indAchse,...
                'kalAlgo',kalAlgo,'zielKonfg',zielKonfg,'vis',true,'AufnKal',AufnKal);
            
        case 'Heikillae'
            % Mit Heikillae Kalibrierung
            
            Abw=fgKam_wpAbw(kamPara,AP,WP,indAchse,...
                'kalAlgo',kalAlgo,'zielKonfg',zielKonfg,'grA',grA,...
                'vis',true,'AufnKal',AufnKal);
    end
    
end

%% Speichern

if spAufn
    % Speichern
    if ischar(spAufn)
        save(['Matfiles/',datestr(now,'yyyymmdd'),'_KamKal_',spAufn],...
            'kamPara','AufnKal');
    else
        save(['Matfiles/',datestr(now,'yyyymmdd'),'_KamKal'],...
            'kamPara','AufnKal');
    end
end


end
