% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function Ibincode=fgKam_Ibincode(sxPr,syPr,mxstPr,nSt,lprj)
% Projektorbilder mit Grey-Code-Streifen (Größe Projektor,
% Projektorbereich, Anzahl der Streifen, Leuchtstärke)

% Bittiefe der Streifen, Pixel pro feinstem Streifen
btSt=log(nSt)/log(2); pxSt=mxstPr/nSt;

% Streifenbilder; pro zusaetzlichem Bit ein Bild; mit Skalierung
St=zeros(nSt*pxSt,nSt*pxSt,btSt);
for i=1:btSt
    npx=nSt/2^i; % Pixel pro Steifen aktueller Bittiefe
    St(:,:,i)=...
        repmat([zeros(nSt*pxSt,npx*pxSt),ones(nSt*pxSt,npx*pxSt)],...
        [1 2^(i-1)]);
end

% Projektorbilder aus Hellbild, vert. und horz. Streifenbildern
Ibincode=zeros(syPr,sxPr,2+btSt*2);
Ibincode(((syPr-nSt*pxSt)/2+1):((syPr-nSt*pxSt)/2+nSt*pxSt),...
    ((sxPr-nSt*pxSt)/2+1):((sxPr-nSt*pxSt)/2+nSt*pxSt),...
    2)=ones(nSt*pxSt,nSt*pxSt);
Ibincode(((syPr-nSt*pxSt)/2+1):((syPr-nSt*pxSt)/2+nSt*pxSt),...
    ((sxPr-nSt*pxSt)/2+1):((sxPr-nSt*pxSt)/2+nSt*pxSt),...
    3:btSt+2)=St;
Ibincode(((syPr-nSt*pxSt)/2+1):((syPr-nSt*pxSt)/2+nSt*pxSt),...
    ((sxPr-nSt*pxSt)/2+1):((sxPr-nSt*pxSt)/2+nSt*pxSt),...
    btSt+3:btSt*2+2)=permute(St,[2 1 3]);

% Leuchtstärke des Projektorbildes anpassen
Ibincode=Ibincode*lprj(1)+lprj(2);

end