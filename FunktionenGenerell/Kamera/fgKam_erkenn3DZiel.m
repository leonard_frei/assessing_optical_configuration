% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [xyGes, rGes, krs]=fgKam_erkenn3DZiel(Aufn,rPx,varargin)
% Erkenne Kreise des 3D Kalibrierziels

%% Parsen

p = inputParser;

defaultKrs=0.88; addOptional(p,'krs',defaultKrs);
defaultFeldGr=[25 75]; addOptional(p,'feldGr',defaultFeldGr);
defaultVis=false; addOptional(p,'vis',defaultVis);

parse(p,varargin{:})

krs=p.Results.krs;
feldGr=p.Results.feldGr; 
vis=p.Results.vis; 

%% Ermittlung der Kreise

% Bestimme die Gr��e der Aufnahme
grAufn=size(Aufn);

if krs==0
    % Bestimme die Kreise interaktiv
    
    interRueck=defaultKrs;
    while ~isempty(interRueck)
        
        % Inertialisier die interaktive Kreisbestimmung
        krsInter=interRueck;
        
        % Bestimme kleine und gro�e Kreise getrennt
        [xy,r]=imfindcircles(Aufn,rPx,'ObjectPolarity','dark',...
            'Sensitivity',krsInter);
        [rS,inds]=sort(r,'descend');
        
        % Stelle alles dar
        fim=figure; imshow(Aufn); viscircles(xy, r,'Color','b');
        text(xy(:,1),xy(:,2),num2str([1:size(xy,1)]'),'color','w');
        fim.Units='normalized'; fim.Position=[0 .25 .5 .5]; 
        title(['Gefundene Kreise: ',num2str(size(xy,1)),...
            ', Sensitivitaet:', num2str(krsInter)]) 
        
        % Frage den Benutzer
        interRueck=input(['Wurden alle Kreise gefunden? ',...
            'Wenn nicht gebe neue Sensitivity ein ']);
        
        % �bergebe die Kreissensitivit�t
        if isempty(interRueck); krs=krsInter; end
    end
    
else
    % Bestimmed ei Kreise mit vorgegebener Sensitivitat
           
    % Bestimme kleine und gro�e Kreise getrennt
    [xy,r]=imfindcircles(Aufn,rPx,'ObjectPolarity','dark',...
        'Sensitivity',krs);
    [rS,inds]=sort(r,'descend');
    
end

%% Sortierung der Kreise

% Ordne die gro�en Kreise zur einfacheren berechnung oben im Array an
rGr=rS(1:2); rKl=rS(3:end); xyGr=xy(inds(1:2),:); xyKl=xy(inds(3:end),:);
xy=[xyGr;xyKl];
r=[rGr;rKl];

% Berechne die Abstandsmatrix
A=fgAllg_abstandAlle(xy);

% Berechne den Indize des gro�en Kreises (row), der in der Ecke liegt und 
% den Indize des ihm gegen�ber liegenden kleinen kreis (col)
Asub=A(1:2,:);
[row, col]=find(ismember(Asub, max(Asub(:))));

% Generiere einen Vektor mit dem gro�en Kreis der oberen h�lfte an
% erster Stelle und dem gro�en Kreis der unteren H�lfte an zweiter Stelle
indVec=[row 3-row];

grSort=xyGr(indVec,:);

% Berechne das Skalarprodukt aller von dem gro�en Eckgreis ausgehenden
% Richtungsvektoren auf den vertikalen Richtungsvektor des Bildes
vec=(xy-xy(row,:))';
dotProducts=abs(dot(vec', repmat([diff(grSort)],size(vec,2),1), 2));

% Extrahiere die oberen und unteren Punkte
[~, indO]=mink(dotProducts,24); [~, indU]=maxk(dotProducts,24);

% Teile die Kreise in die obere und die untere H�lfte ein
sortO=sort(indO); sortU=sort(indU);
xyKlsO=xy(sortO(2:end),:); xyKlsU=xy(sortU(2:end),:);
rKlsO=r(sortO(2:end),:); rKlsU=r(sortU(2:end),:);
xyGrO=xy(sortO(1),:); xyGrU=xy(sortU(1),:);
rGrO=r(sortO(1),:); rGrU=r(sortU(1),:);

% Ordne die Kreise 
[xyKlsO,rKlsO]=fgKam_sortKal3DZiel(Aufn,xyKlsO,rKlsO,...
    xyGrO,grAufn,feldGr);
[xyKlsU,rKlsU]=fgKam_sortKal3DZiel(Aufn,xyKlsU,rKlsU,...
    xyGrU,grAufn,feldGr);

% F�hre alle Kreise zusammen
xyGes=[xyGrO; xyKlsO; xyGrU; xyKlsU];
rGes=[rGrO;rKlsU;rGrU;rKlsO];

if vis
figure; imshow(Aufn); 
viscircles(xyGes, rGes,'Color','b');
text(xyGes(:,1),xyGes(:,2),num2str([1:size(xyGes,1)]'),'color','w')
end

end