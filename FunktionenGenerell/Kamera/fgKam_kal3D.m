% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function stereoPara = fgKam_kal3D(Aufn)
%% 3D-Kalibrierziel-Aufnahmen verarbeiten

% Aufbereiten der Aufnahmen
naufn=size(Aufn,4); 
grB=[size(Aufn,1),size(Aufn,2)]; nb=size(Aufn,4)*2;

% Bildpunkte erkennen
BPall1=zeros(48,2,naufn); BPall2=BPall1;
for i=1:naufn

    % Aufnahmen extrahieren
    Aufn1=Aufn(:,:,:,i,1); Aufn2=Aufn(:,:,:,i,2);

    % Bildpunkte erkennen
    rPx=[20 40];
    [xyGes1, rGes1]=fgKal_erkennR3DZiel(Aufn1,rPx);
    [xyGes2, rGes2]=fgKal_erkennR3DZiel(Aufn2,rPx);

    % Darstellen
    figure; imshow(Aufn1); hold on;
    plot(xyGes1(:,1),xyGes1(:,2));
    viscircles(xyGes1,rGes1,'color','r');
    text(xyGes1(:,1),xyGes1(:,2),num2str([1:size(xyGes1,1)]'),'color','w')
    figure; imshow(Aufn2); hold on;
    plot(xyGes2(:,1),xyGes2(:,2));
    viscircles(xyGes2,rGes2,'color','r');
    text(xyGes2(:,1),xyGes2(:,2),num2str([1:size(xyGes2,1)]'),'color','w')

    % Zusammenfügen
    BPall1(:,:,i)=xyGes1; BPall2(:,:,i)=xyGes2;
end

%% Stereokalibrierung mit Matlab


end