% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function AufnProjU=fgKam_undistImage(AufnProj,kamPara)
% Alle Aufnahmen einer Stereokamera entzerren und 

grAufn=size(AufnProj); 
grI=grAufn(1:2); nI=grAufn(4); nkam=grAufn(5);

%% Entzerre die Aufnahmen for jede Kamera
AufnProjU=zeros(grI(1),grI(2),nI,nkam); 
AufnProjU_i=zeros(grI(1),grI(2),nI);
for i=1:nkam
    
    % Aufnahmen extrahieren
    Aufn_i=AufnProj(:,:,1,:,i); Aufn_i=permute(Aufn_i,[1 2 4 3]);
    
    % Korrigiere Verzeichnung
    for j=1:nI
        AufnProjU_i(:,:,j)=undistortImage(Aufn_i(:,:,j),...
            kamPara.(['CameraParameters',num2str(i)]));
    end
    
    % �bergeben
    AufnProjU(:,:,:,i)=AufnProjU_i;
    
end

end