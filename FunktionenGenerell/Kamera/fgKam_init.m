% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [kam, src, figKam] = fgKam_init(varargin)
% Initialisiere Kamera

%% Parsen
p = inputParser;

defaultIProj = []; addOptional(p,'IProj',defaultIProj,@isnumeric);

parse(p,varargin{:})

IProj=p.Results.IProj; 

%% Initialisierung

imaqreset
kamInfoGENTL=imaqhwinfo('gentl');
if isempty(kamInfoGENTL.DeviceIDs)
    kamInfoGIGE=imaqhwinfo('gige');
    if isempty(kamInfoGIGE.DeviceIDs)
        error('No Camera detected');
    else
        csKam='gige';
    end
else
    csKam='gentl';
end

switch csKam
    case 'gige'; kamInfo=kamInfoGIGE; 
    case 'gentl'; kamInfo=kamInfoGENTL;
end

nKam=numel(kamInfo.DeviceIDs);

for i=1:nKam
    switch csKam
        case 'gige'; kam(i) = videoinput('gige', i, 'Mono8');
        case 'gentl'; kam(i) = videoinput('gentl', i, 'Mono8');
    end
    kam(i).FramesPerTrigger = 1;
    src(i) = getselectedsource(kam(i));
end

% Zeige ein Projektorbild an
if ~isempty(IProj)
   fProj=fgKam_zeigProj(IProj); 
end

%% Gain and Exposure

% Bestimme die aktuelle Belichtungszeit und Verst�rkung
for i=1:nKam
        
    
    src(i).ExposureAuto = 'Continuous';
    src(i).GainAuto = 'Continuous';
    switch csKam
        case 'gige'
            src(i).AcquisitionFrameRateAbs = 5;
            src(i).PacketSize = 8228;
        case 'gentl'
            src(i).AcquisitionFrameRate = 5;
    end
end


%% Vorschau

vidRes = kam(1).VideoResolution;
NumberOfBands = kam(1).NumberOfBands;
br=.7; hh=br*vidRes(2)/vidRes(1)'; rndBr=(1-br)/2; rndHh=(0.5-hh)/3;
frmX=vidRes(1)*[.25 .25 .75 .75 .25];
frmY=vidRes(2)*[.15 .85 .85 .15 .15];

figKam=figure;

switch nKam
    case 1
        figKam.Position=[1, 42, 766.4000, 740.6000/2]; figKam.Color=[1 1 1];
        posKam=[rndBr 2*rndHh br hh*2];
    case 2
        figKam.Position=[1, 42, 766.4000, 740.6000]; figKam.Color=[1 1 1];
        posKam=[rndBr 2*rndHh br hh; rndBr .5+rndHh br hh];
        
end

for i=1:nKam
    
    ax(i)=axes(figKam,'Position',posKam(i,:));
    handleToImageInAxes1 = image( zeros([vidRes(2), vidRes(1), NumberOfBands]));
    preview(kam(i),handleToImageInAxes1);
    
    text(10,50,['Kamera ',num2str(i)],'FontSize',20, 'color', [0.5843, 0.8157, 0.9882]);
    tExp(i)=text(10,110,'Exp. ','FontSize',10, 'color', [0.5843, 0.8157, 0.9882]);
    tGain(i)=text(10,150,'Gain ','FontSize',10, 'color', [0.5843, 0.8157, 0.9882]);
    
    hold on
    plot(frmX, frmY, 'color', [0.5843, 0.8157, 0.9882], 'LineWidth', 3);
    hold off
    
end

pause(2);

%% Fixiere die Belichtung

% Fixiere die Belichtung
for i=1:nKam
    
    src(i).ExposureAuto = 'Off';
    src(i).GainAuto = 'Off';
    
    switch csKam
        case 'gige'
            expStr{i}=['Exp. ',num2str(src(i).ExposureTimeAbs/1e6)];
        case 'gentl'
            expStr{i}=['Exp. ',num2str(src(i).ExposureTime/1e6)];
    end
    gainStr{i}=['Gain ',num2str(src(i).Gain)];
    
    
end

% Blende sie ein
for i=1:nKam
    
    ax(i).Children(3).String=expStr{i};
    ax(i).Children(2).String=gainStr{i};
    
end

%% Belichtungseinstellung

for i=1:nKam
    
    expIO=true;
    disp(' ');
    disp(['Belichtungseinstellung Kamera', num2str(i)]);
    disp(' ');
    
    while expIO
        
        disp('Ist die Belichtung ok? Dr�cke Enter!')
        disp('Wenn nicht, geben eine neue im folgenden Format an')
        expGain=input('[ExpTimeSec, Gain] ');
        
        if ~isempty(expGain)
            
            src(i).ExposureAuto = 'Off';
            src(i).GainAuto = 'Off';
            switch csKam
                case 'gige'; src(i).ExposureTimeAbs=expGain(1)*1e6;
                case 'gentl'; src(i).ExposureTime=expGain(1)*1e6;
            end
            src(i).Gain=expGain(2);
            
            switch csKam
                case 'gige'
                    expStr{i}=['Exp. ',num2str(src(i).ExposureTimeAbs/1e6)];
                case 'gentl'
                    expStr{i}=['Exp. ',num2str(src(i).ExposureTime/1e6)];
            end
            gainStr{i}=['Gain ',num2str(src(i).Gain)];
            
            ax(i).Children(3).String=expStr{i};
            ax(i).Children(2).String=gainStr{i};
        else
            
            expIO=false;
            
        end
        
    end
    
    
end