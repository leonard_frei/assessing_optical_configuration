% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [t, R]=fgKam_posExtr(pos)
% Berechne den t-Vektor und die R-Matrix von Heikilae-Parametern
wa=pos(4)*pi/180;
pa=pos(5)*pi/180;
ra=pos(6)*pi/180;
cw=cos(wa); sw=sin(wa);
cp=cos(pa); sp=sin(pa);
cr=cos(ra); sr=sin(ra);

R(1,:)=[cr*cp -sr*cw+cr*sp*sw sr*sw+cr*sp*cw];
R(2,:)=[sr*cp cr*cw+sr*sp*sw -cr*sw+sr*sp*cw];
R(3,:)=[-sp cp*sw cp*cw];

t=pos(1:3);

end