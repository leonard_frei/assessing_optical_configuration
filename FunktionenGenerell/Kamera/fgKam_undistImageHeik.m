% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [APkL,APkR]=fgKam_undistImageHeik(APl,APr,grA,kamPara)
% Alle Aufnahmen einer Stereokamera entzerren und 

na=size(APl,3); 

% Erstelle die Kameraparameter-Vektoren
parl=[1,kamPara.CameraParameters1.FocalLength(1)/grA(2),...
    kamPara.CameraParameters1.PrincipalPoint,...
    kamPara.CameraParameters1.RadialDistortion,...
    kamPara.CameraParameters1.TangentialDistortion];

parr=[1,kamPara.CameraParameters2.FocalLength(1)/grA(2),...
    kamPara.CameraParameters2.PrincipalPoint,...
    kamPara.CameraParameters2.RadialDistortion,...
    kamPara.CameraParameters2.TangentialDistortion];

% Entzerre die Aufnahmen for jede Kamera
APkL=APl; APkR=APr; 
% for i=1:na
%     APkL(:,:,i)=fgKam_imcorrSpz([grA(1:2) parl(2)*grA(1) 0],parl,APl(:,:,i));
%     APkR(:,:,i)=fgKam_imcorrSpz([grA(1:2) parl(2)*grA(1) 0],parr,APr(:,:,i));
% end

end