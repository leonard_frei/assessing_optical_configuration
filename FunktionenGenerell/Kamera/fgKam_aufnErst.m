% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function Aufn=fgKam_aufnErst(kam,src,varargin)
% Eine oder mehrere Aufnahmen mit der Stereokamera erstellen

%% Parsen
p = inputParser;

defaultIProj = []; addOptional(p,'IProj',defaultIProj,@isnumeric);
defaultNI=1; addOptional(p,'nI',defaultNI,@isnumeric);
defaultInterakt=[]; addOptional(p,'interakt',defaultInterakt,@islogical);
defaultSpAufn=false; addOptional(p,'spAufn',defaultSpAufn,@islogical);
defaultTpaus=1; addOptional(p,'tpaus',defaultTpaus); 

parse(p,varargin{:})

IProj=p.Results.IProj; nI=p.Results.nI; spAufn=p.Results.spAufn;
interakt=p.Results.interakt; tpaus=p.Results.tpaus; 

%% Einrichtung

% Feststellen, ob Aufnahmen angezeigt werden sollen
if ~isempty(IProj); zeigIProj=true; nI=size(IProj,3);
else; zeigIProj=false; end

% Gr��e der Aufnahmenmatrix festlegen
vidRes = kam(1).VideoResolution; nkam=size(kam,2);
Aufn=zeros(vidRes(2),vidRes(1),1,nI,nkam);


%% Aufnahme erstellen

if ~zeigIProj
%% Ohne Projektorbilder
    
    % Erstellen von nI-Aufnahmen ohne den Projektor
    for i=1:nI
        
        disp(' ');
        disp(['Bild: ',num2str(i)]);
        if interakt
            input('Dr�cke Enter, wenn die Positionierung stimmt');
        end
        
        % Erstellen der Aufnahmen
        for k=1:nkam
            AufnKam_ki=getsnapshot(kam(k));
            Aufn(:,:,1,i,k)=im2double(AufnKam_ki);
        end
        
    end
    
elseif zeigIProj
%% Mit Projektorbildern Aufnahmen erstellen
    
    % Bereite das Zeichnen des Projektorbildes vor
    fProj=[];
    
    % Erstellen von nI-Aufnahmen mit dem projektor
    for i=1:nI
        
        % Zu projizierendes Bild extrahieren
        IProj_i=IProj(:,:,i);
        
        % Make the image into a texture
        fProj=fgKam_zeigProj(IProj_i,fProj);
        pause(tpaus);

        % Erstellen der Aufnahmen
        for k=1:nkam
            AufnKam_ki=getsnapshot(kam(k));
            Aufn(:,:,1,i,k)=im2double(AufnKam_ki);
        end
        
    end
    
    close(fProj)
end

%% Speichern

if spAufn
    
    % Speichern
    save(['Matfiles/',datestr(now,'yyyymmdd'),'_Aufn'],...
        'Aufn');
    
end

end
