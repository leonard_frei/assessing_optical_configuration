% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [kamPara,parl,parr]=fgKam_kamKalHeik(AP,WP,grA)
% Kameraparameter mit Heikillaes Toolbox berechnen berechnen

% Anzahl der Kameras
nKam=size(AP,4); na=size(AP,3);

% Schaetze die Brennweite name=[VertPx, HorzPx, ...
% fPx=HorzPx*AbstandW/BreiteW, rPunkte]
kamInfoH=[grA(1:2) 1095*600/297 0];

% Kalibriere entweder eine Mono oder eine Stereokamera
switch nKam
    case 1
        % Monokamera
        
        % Heikillae links
        [parl,posl]=fgKal_cacalSpz(kamInfoH,AP(:,:,:,1),WP);
        Ih1=[parl(2)*grB(2) 0 0; 0 parl(2)*grB(2) 0; parl(3) parl(4) 1];
        
        % Erstellerung des Stereokameraobjekts
        kamPara=cameraParameters('IntrinsicMatrix',Ih1);
        
    case 2
        % Stereokamera
        
        % Heikillae links
        [parl,posl]=fgKam_cacalSpz(kamInfoH,AP(:,:,:,1),WP);
        Ih1=[parl(2)*grA(2) 0 0; 0 parl(2)*grA(2) 0; parl(3) parl(4) 1];
        
        % Heikillae rechts
        [parr,posr]=fgKam_cacalSpz(kamInfoH,AP(:,:,:,2),WP);
        Ih2=[parr(2)*grA(2) 0 0; 0 parr(2)*grA(2) 0; parr(3) parr(4) 1];
        
        % Drehungsmatrix und Verschiebungsvektor zwischen linker und rechter Kam
        tl=zeros(3,1,na); Rl=zeros(3,3,na); tr=tl; Rr=Rl; trl=tl; Rrl=Rr;
        for i=1:na
            [tl(:,1,i), Rl(:,:,i)]=fgKam_posExtr(posl(:,i));
            [tr(:,1,i), Rr(:,:,i)]=fgKam_posExtr(posr(:,i));
            Rrl(:,:,i)=Rl(:,:,i)/Rr(:,:,i);
            trl(:,1,i)=tr(:,1,i)-Rrl(:,:,i)'*tl(:,1,i);
        end
        Rrl=mean(Rrl,3); trl=mean(trl,3);
        
        % Erstellerung des Stereokameraobjekts
        kamHl=cameraParameters('IntrinsicMatrix',Ih1,...
            'RadialDistortion',parl(5:6),'Tangentialdistortion',parl(7:8));
        kamHr=cameraParameters('IntrinsicMatrix',Ih2,...
            'RadialDistortion',parr(5:6),'Tangentialdistortion',parr(7:8));
        kamPara=stereoParameters(kamHl,kamHr,Rrl,trl);
        
end

end

% Rückrechnung
function parH=fuHeik2Mat(kamPara,grA);
parH=[1,kamPara.CameraParameters1.FocalLength(1)/grA(2),...
    kamPara.CameraParameters1.PrincipalPoint,...
    kamPara.CameraParameters1.RadialDistortion,...
    kamPara.CameraParameters1.TangentialDistortion];
end