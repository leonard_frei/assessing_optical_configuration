% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function Abw=fgKam_wpAbw(kamPara,AP,WP,indAchse,varargin)

%% Parsen
p = inputParser;

defaulKalAlgo='Matlab';
addOptional(p,'kalAlgo',defaulKalAlgo);

defaultKalConf='schachbrett';
addOptional(p,'zielKonfg',defaultKalConf);

defaultGrA=[];
addOptional(p,'grA',defaultGrA);

defaultVis=true;
addOptional(p,'vis',defaultVis);

defaultAufnKal=[];
addOptional(p,'AufnKal',defaultAufnKal);

parse(p,varargin{:})

kalAlgo=p.Results.kalAlgo;
zielKonfg=p.Results.zielKonfg;
grA=p.Results.grA; 
vis=p.Results.vis;
AufnKal=p.Results.AufnKal;

if (strcmp(kalAlgo,'Haikillae') && isempty(grA))
    error('F�r die Haikillae-Verzeichnungskorrektur wird die Bildgr�sse ben�tigt')
end

%% Azufnahmepunkte entzerren

% Anzahl der Aufnahmen bestimmen
na=size(AP,3); nwp=size(WP,1);

% Aufnahmen entzerren
switch kalAlgo
    
    case 'Matlab'
        % Entzerrung mit Matlab
        
        APk=AP;
        for i=1:na
            APk(:,:,i,1)=undistortPoints(AP(:,:,i,1),kamPara.CameraParameters1);
            APk(:,:,i,2)=undistortPoints(AP(:,:,i,2),kamPara.CameraParameters2);
        end
        
    case 'Heikillae'
        % Entzerrung mit Haikillae
        
        [APkL,APkR]=fgKam_undistImageHeik(AP(:,:,:,1),AP(:,:,:,2),grA,kamPara);
        APk=cat(4,APkL,APkR);
        
end

%% Abweichung Vorw�rtsprojektion
switch zielKonfg
    
    case 'schachbrett'
        
        % Abweichung berechnen
        Abw=zeros(nwp,3,na);
        AbwM=zeros(na,3);
        for i=1:na
            
            % Weltpunkte durch Triangulation berechnen
            PW_k=triangulate(APk(:,:,i,1),APk(:,:,i,2),kamPara);
            PW_k=pointCloud(PW_k);
            
            % z-Achse
            zEbene=fgPnktWlk_AsglrEbene(PW_k,5);
            zAchse=zEbene.Normal;
            
            % y-Achse
            yP=PW_k.Location(1:indAchse,:);
            [V,D] = eig(cov(yP));
            [~,dmax]=max(sum(D));
            yAchse=V(:,dmax)';
            
            % Richtung bestimmen
            yAchseG=yP(end,:)-yP(1,:); yAchseG=yAchseG./norm(yAchseG);
            if norm(yAchseG-yAchse)>1; yAchse=-yAchse; end
            
            % x-Achse
            xAchse=cross(yAchse,zAchse);
            
            % Tranformation berechnen
            Rworld2kam=[xAchse', yAchse', zAchse']';
            urspG=PW_k.Location(1,:);
            world2kamG=affine3d([Rworld2kam,zeros(3,1); urspG 1]);
            kam2worldG=invert(world2kamG);
            
            % Transformation
            PW_w = pctransform(PW_k,kam2worldG);
            WPt=PW_w.Location;
            
            % Abweichung
            WP=[WP(:,1),WP(:,2),zeros(length(WP),1)];
            Abw(:,:,i)=WPt-WP;
            AbwM(i,:)=mean(abs(Abw(:,:,i)));
                        
        end
        
    case '3DStufen'
                
        Abw=zeros(nwp,3,na);
        AbwM=zeros(na,3);
        for i=1:na
            
            % Weltpunkte durch Triangulation berechnen
            PW_k=triangulate(APk(:,:,i,1),APk(:,:,i,2),kamPara);
            PW_k=pointCloud(PW_k);
            
            % z-Achse
            PWZachse_k=select(PW_k,1:indAchse(2));
            zEbene=fgPnktWlk_AsglrEbene(PWZachse_k,5);
            zAchse=zEbene.Normal; if zAchse(3)>0; zAchse=-zAchse; end
            
            % x-Achse
            xP=PW_k.Location(1:indAchse(1),:);
            [V,D] = eig(cov(xP));
            [~,dmax]=max(sum(D));
            xAchse=V(:,dmax)';
            
            % Richtung bestimmen
            xAchseG=xP(end,:)-xP(1,:); xAchseG=xAchseG./norm(xAchseG);
            if norm(xAchseG-xAchse)>1; xAchse=-yAchse; end
            
            % y-Achse
            yAchse=cross(zAchse,xAchse);
            
            % Tranformation berechnen
            Rworld2kam=[xAchse', yAchse', zAchse']';
            urspG=PW_k.Location(1,:);
            world2kamG=affine3d([Rworld2kam,zeros(3,1); urspG 1]);
            kam2worldG=invert(world2kamG);
            
            % Transformation
            PW_w = pctransform(PW_k,kam2worldG);
            WPt=PW_w.Location;
            
            % Abweichung
            Abw(:,:,i)=WPt-WP;
            AbwM(i,:)=mean(abs(Abw(:,:,i)));
            
        end
end
%% Darstellung der Abweichung

if vis && ~isempty(AufnKal)
    
    for i=1:na
        
        bdgr=get(0,'ScreenSize'); brgr=50;
        fgi=figure('Position',[0 brgr bdgr(4)-3*brgr bdgr(4)-3*brgr]);
        
        ax1=subplot(2,2,1);
        quiver(WP(:,1),WP(:,2),Abw(:,1,i),Abw(:,2,i));
        title({'Abweichungsanteile in x- und y-Richtung in Weltkoordinaten',...
            ['Mittlere Abweichungen x: ',num2str(AbwM(i,1)),...
            ', y: ',num2str(AbwM(i,2)),', z: ',num2str(AbwM(i,3))]});
        xlabel('x'); ylabel('y');
        axis equal
        
        ax2=subplot(2,2,2);
        pcshow(PW_w,'MarkerSize',35); hold on; plot3(WP(:,1),WP(:,2),WP(:,3));
        title('Weltpunktwolke mit triangulierten und KMG-Punkten');
        
        ax3=subplot(2,2,3);
        imshow(AufnKal(:,:,:,i,2)); hold on;
        plot(AP(:,1,i,2),AP(:,2,i,2),'-*')
        title(['Aufnahme links ',num2str(i)])
        
        ax4=subplot(2,2,4);
        imshow(AufnKal(:,:,:,i,1)); hold on;
        plot(AP(:,1,i,1),AP(:,2,i,1),'-*')
        title(['Aufnahme rechts ',num2str(i)])
        
        fgi.Color=[1 1 1];
        ax3.Position=[.02 .02 .45 .45];
        ax4.Position=[.52 .02 .45 .45];
    end
    
elseif vis && isempty(AufnKal)
    
    for i=1:na
        
        bdgr=get(0,'ScreenSize'); brgr=50;
        fgi=figure('Position',[0 brgr bdgr(4)-3*brgr bdgr(4)/2-3*brgr]);
        
        ax1=subplot(1,2,1);
        quiver(WP(:,1),WP(:,2),Abw(:,1,i),Abw(:,2,i));
        title({'Abweichungsanteile in x- und y-Richtung in Weltkoordinaten',...
            ['Mittlere Abweichungen x: ',num2str(AbwM(i,1)),...
            ', y: ',num2str(AbwM(i,2)),', z: ',num2str(AbwM(i,3))]});
        xlabel('x'); ylabel('y');
        axis equal
        
        ax2=subplot(1,2,2);
        pcshow(PW_w,'MarkerSize',35); hold on; plot3(WP(:,1),WP(:,2),WP(:,3));
        title('Weltpunktwolke mit triangulierten und KMG-Punkten');
        
        fgi.Color=[1 1 1];
        
    end
    
end


end