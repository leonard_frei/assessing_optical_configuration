% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function fProj=fgKam_zeigProj(IProj,varargin)
% Zeige mit dem Projektor ein Bild an

%% Parsen
p = inputParser;

defaultFProj = []; addOptional(p,'fProj',defaultFProj);

parse(p,varargin{:})

fProj=p.Results.fProj;


%% Darstellen

% Gr��e des zu projizierenden Bildes
grIProj=size(IProj);

if isempty(fProj)
    
    % Zeige das Bild in neuer Figure an
    fProj=figure; imshow(IProj);
    fProj.Position=[1+grIProj(2) 1 grIProj(2) grIProj(1)];
    set(fProj,'WindowState', 'fullscreen','MenuBar', ...
        'none', 'ToolBar', 'none');
    ax=gca; ax.Position=[0 0 1 1];
    
else
    
    % Beschreibe die bestehende Figure neu
    fProj.Children.Children.CData=IProj;
    
end
end