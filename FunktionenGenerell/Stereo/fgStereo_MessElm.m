% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function messElm=fgStereo_MessElm(messElm,PW_w, varargin)
% Messelemente auswerten

%% Parsen

p = inputParser;

defaultVisErg=false; addOptional(p,'visErg',defaultVisErg);
defaultBohr=[]; addOptional(p,'Bohr',defaultBohr);

parse(p,varargin{:});

visErg=p.Results.visErg; Bohr=p.Results.Bohr;

%% Auswertung
% Ermittle die Anzahl der Messlemente
nelm=length(messElm);

% Berechne die Elemente
for i=1:nelm
    
    % Punkte ausw�hlen
    roi_i=messElm(i).roiM; disp(num2str(i)); 
    ind_1=findPointsInROI(PW_w,roi_i);
    PW_Welt_i = select(PW_w,ind_1);
    fil_i=messElm(i).filM;
    
    % Elementemodell gem�� Typ per Ausgleichsrechnung ermitteln
    elmnTyp_i=messElm(i).typM;
    switch elmnTyp_i
        case 'ebene'
            [modell_i,indp_i]=fgPnktWlk_AsglrEbene(PW_Welt_i,fil_i);
        case 'zyl'
            % [modell_i,indp_i]=pcfitcylinder(PW_Welt_i,fil_i);
            xyzZyl=PW_Welt_i.Location; indp_i=1:length(xyzZyl);
            schaetz_i=messElm(i).sollM;
            modell_i=fgPnktWlk_AsglrZylinder(xyzZyl,'schaetz',schaetz_i);
        case 'bohr'
            [~,indB]=fgAllg_abstandK(Bohr(:,1:3),...
                (roi_i(1:2:5)+roi_i(2:2:6))/2);
            PW_Welt_i=pointCloud(Bohr(indB,1:3));
            modell_i.Radius=Bohr(indB,4); indp_i=1;
            modell_i.Pos=Bohr(indB,1:3);
    end
    
    % Position des Messelements berechnen mit gefilterten Punkten
    pnkt_i=PW_Welt_i.Location(indp_i,:);
    pos_i=mean(pnkt_i,1);
    
    % �bergabe
    messElm(i).modellM=modell_i; clear('modell_i');
    messElm(i).posM=pos_i; clear('pos_i');
    messElm(i).pnktM=pnkt_i; clear('pnkt_i');
    
end

%% Visualisieren

if visErg
    % Stelle die Messelemente dar
    cmap=lines(nelm);
    figure('Position',[903 199 888 603]);
    pcshow(PW_w.Location,[0 1 1],'MarkerSize',35); hold on;
    xlabel('X'); ylabel('Y'); zlabel('Z')
    for i=1:nelm
        pnkt_i=messElm(i).pnktM;
        modell_i=messElm(i).modellM;
        plot3(pnkt_i(:,1),pnkt_i(:,2),pnkt_i(:,3),'*',...
            'Color',cmap(i,:),'MarkerSize',6);
        %     plot(modell_i)
    end
    legend({'Punktwolke',messElm(:).nameM},'TextColor','w')
    view(135, 40)
    title('Punktewolke mit f�r Elementberechnung verwendeten Punkten');
    
end

end