% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [messPnkt_w, kalPnkt_w, messPnkt_au, kalPnkt_au]=...
    fgStereo_PnktWelt(kamPara,kalPnkt_a,messPnkt_a,kalPnkt_KMG,grAufn,varargin)
% Welt-Koordinaten von Punkten berechnen

%% Parsen

p=inputParser; 
dZdir='normal'; addOptional(p,'zDir',dZdir); 

parse(p,varargin{:});

zDir=p.Results.zDir; 

%% Azswertung

% Anzahl der Kameras bestimmen
nkam=size(kalPnkt_a,4);

switch nkam
    
    case 1
        % Monokamera
        
        % Punkte entzerren
        kalPnkt_au = undistortPoints(kalPnkt_a,kamPara);
        messPnkt_au = undistortPoints(messPnkt_a,kamPara);
        
        % Extrinsische Parameter berechne
        [R,t] = extrinsics(kalPnkt_au,kalPnkt_KMG,kamPara);
        
        % Weltkoordianten berechnen
        kamIntr_i=cameraIntrinsics(kamPara.FocalLength, ...
            kamPara.PrincipalPoint, grAufn);
        messPnkt_w=pointsToWorld(kamIntr_i,R,t,messPnkt_au);
        kalPnkt_w=pointsToWorld(kamIntr_i,R,t,kalPnkt_au);
        
    case 2
        % Stereokamera
        
        % Punkte entzerren
        kalPnkt1_a = undistortPoints(kalPnkt_a(:,:,1,1),kamPara.CameraParameters1);
        kalPnkt2_a = undistortPoints(kalPnkt_a(:,:,1,2),kamPara.CameraParameters2);
        messPnkt1_a= undistortPoints(messPnkt_a(:,:,1,1),kamPara.CameraParameters2);
        messPnkt2_a= undistortPoints(messPnkt_a(:,:,1,2),kamPara.CameraParameters2);
        nKal=size(kalPnkt1_a,1); nMess=size(messPnkt1_a,1);
        
        % Triangulieren (--> Kamerakoordinaten)
        kalPnkt_k=triangulate(kalPnkt1_a,kalPnkt2_a,kamPara);
        messPnkt_au=triangulate(messPnkt1_a,messPnkt2_a,kamPara);
        
        % Ausgleichseben für z-Achse durch Kalibrierungspunkte, Y-Achse mit Diff
        % zwischen erstem und zweiten Pnkt
        [zEbene, inlierIndices]=fgPnktWlk_AsglrEbene(pointCloud(kalPnkt_k),.5);
        switch zDir
            case 'normal'
                zAchse=zEbene.Normal; if zAchse(3)<0; zAchse=-zAchse; end
            case 'gespiegelt'
                zAchse=zEbene.Normal; if zAchse(3)>0; zAchse=-zAchse; end
        end
        yAchse=kalPnkt_k(2,:)-kalPnkt_k(1,:); yAchse=yAchse/norm(yAchse);
        
        % Tranformation berechnen
        yAchseProj=yAchse-zAchse*(yAchse*zAchse');
        xAchse=cross(yAchseProj,zAchse);
        Rwolrd2kam=[xAchse', yAchseProj', zAchse']';
        ursp=kalPnkt_k(1,:);
        world2kam=affine3d([Rwolrd2kam,zeros(3,1); ursp 1]);
        kam2world=invert(world2kam);
        
        % Transformation (--> Weltkoordinaten)
        PW_k=pointCloud([kalPnkt_k; messPnkt_au]);
        PW_w = pctransform(PW_k,kam2world);
        kalPnkt_w=PW_w.Location(1:nKal,:);
        messPnkt_w=PW_w.Location((nKal+1):end,:);
              
end
end

