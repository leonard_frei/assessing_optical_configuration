% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [xyKrInd,IBinInd,IbinSt]=fgStereo_KorrsBinaer(Aufn,nSt,varargin)
% Finde die Pixel, welche zu den Feldern eines quadratischen Binaer-Codes
% gehoeren mit der Streifenzahl nSt. Verwende dazu alle Bilder in I

%% Parsen
defaultThDiffB=[]; defaultParDiffSt=.5; defaultVisBinErg=false;

p = inputParser;
addOptional(p,'thDiffB',defaultThDiffB);
addOptional(p,'parDiffSt',defaultParDiffSt);
addOptional(p,'visBinErg',defaultVisBinErg);
parse(p,varargin{:});

thDiffB=p.Results.thDiffB;
parDiffSt=p.Results.parDiffSt;
visBinErg=p.Results.visBinErg;

%% Vorebereitung

% Bittiefe der Streifen, Pixel pro feinstem Streifen
btSt=log(nSt)/log(2);

% Gr��e der Bilder
grI=size(Aufn); npx=grI(1)*grI(2); nast=grI(3)-2;

% Trenne das erste Hellbild von den restlichen Streifenbildern
AufnDnkl=Aufn(:,:,1); AufnHll=Aufn(:,:,2); AufnSt=Aufn(:,:,3:end);

%% Binarisieren

% Differenzbilder berechnen
AufnDiffB=AufnHll-AufnDnkl;
AufnDiffSt=AufnSt-repmat(AufnDnkl,[1,1,nast]);
AufnDiffStInv=repmat(AufnHll,[1,1,nast])-AufnSt;

% Thresholds festlegen f�r das Differenzbild und das am feinsten aufgel�ste
% Streifenbild
otsThDiffB=graythresh(AufnDiffB);
if isempty(thDiffB); thDiffB=otsThDiffB; end
thDiffSt=repmat(AufnDiffB*parDiffSt,[1,1,nast]);
thDiffStInv=repmat(AufnDiffB*parDiffSt,[1,1,nast]);

% Aufnahmen binarisieren
IbinB=imbinarize(AufnDiffB,thDiffB);
IbinSt=imbinarize(AufnDiffSt,thDiffSt);
IbinStInv=imbinarize(AufnDiffStInv,thDiffStInv);

% Bei Bedarf visualisieren
if visBinErg;shwBinErgb(AufnDiffB,IbinB,AufnDiffSt,IbinSt,btSt,...
    thDiffB,parDiffSt); end

%% Masken erstellen

% Feinste Streifen f�r vertikale und horizontale Bilder austrennen
IbinStF_v=IbinSt(:,:,nast/2); IbinStInvF_v=IbinStInv(:,:,nast/2); 
IbinStF_h=IbinSt(:,:,end); IbinStInvF_h=IbinStInv(:,:,end); 

% Rauschen au�erhalb des Messbereiches entfernen
IbinStF_v(~IbinB)=false; IbinStInvF_v(~IbinB)=false;
IbinStF_h(~IbinB)=false; IbinStInvF_h(~IbinB)=false;

% Bereiche die �berlappen
Iuber_v=IbinStF_v & IbinStInvF_v;
Iuber_h=IbinStF_h & IbinStInvF_h; 

% �berlapp entfernen
IbinStF_v=IbinStF_v & ~Iuber_v;
IbinStInvF_v=IbinStInvF_v & ~Iuber_v;
IbinStF_h=IbinStF_h & ~Iuber_h;
IbinStInvF_h=IbinStInvF_h & ~Iuber_h;

% Erodieren, um �berlapp zu vermeiden
r=3; se = strel('disk',r); 
IbinStF_ve=imerode(IbinStF_v,se);
IbinStInvF_ve=imerode(IbinStInvF_v,se);
IbinStF_he=imerode(IbinStF_h,se);
IbinStInvF_he=imerode(IbinStInvF_h,se);

% �berlagerung zur Gesamtmaske
IbinMsk_v=IbinStF_ve|IbinStInvF_ve;
IbinMsk_h=IbinStF_he|IbinStInvF_he;
IbinMsk=IbinMsk_v & IbinMsk_h;

%% Kodieren

% Ordne den Pixeln von I korrespondierende Pixel des Streifebildes zu;
% �berlagere dazu alle Streifenbilder. Die Bereiche mit am meisten Bit
% werden am hellsten. �berlagere Spalten und Zeilen. Gewichte dabei die
% Spalten mit der Maximalzahl, so dass sich der Index innerhalb einer
% Spalte jeweils um den Zeilenwert erh�ht
IKrX=IbinB; IKrY=IbinB;
for i=1:btSt
    IKrX=IKrX+IbinSt(:,:,i)*(2^(btSt-i));
    IKrY=IKrY+IbinSt(:,:,nast/2+i)*(2^(btSt-i));
end
IBinInd=(IKrX.*2^btSt)-2^btSt+IKrY; 

% Beschneide das Bild mit der Maske
IBinInd(~IbinMsk)=0;

% Berechne die mittleren x-y-Werte der Pixel von I, die zum einem Index
% des Streifenbildes geh?ren
regp=regionprops(IBinInd,'Centroid','PixelIdxList');
mitPnkt=vertcat(regp(:).Centroid);

% Erstelle eine Liste mit x-y-Koordinaten der Korrespondenzen
xyKrInd=nan(2^btSt^2,2); nmp=length(mitPnkt);
xyKrInd(1:nmp,:)=mitPnkt;


end

%
function shwBinErgb(AufnDiffB,IbinB,AufnDiffSt,IbinSt,btSt,...
    thDiffB,parDiffSt)

% Figure estellen, die den Bildschirmbereich m�glichst vollst�ndig f�llt
grBld=get(0,'ScreenSize');
grBld(2)=grBld(2)+40; grBld(4)=grBld(4)-117;
fgBin=figure('Position',grBld);

% Die Differenzbildereinf�gen
sp1=subplot('Position',[0 .5 .5 .5]); imshow(AufnDiffB); 
text(0, 30,['Gesamtmaske mit Theshold: ',num2str(thDiffB)],...
    'FontSize',14,'Color',[0.3010 0.7450 0.9330]);
sp2=subplot('Position',[0 0 .5 .5]); imshow(IbinB);
sp3=subplot('Position',[.5 .5 .5 .5]); imshow(AufnDiffSt(:,:,btSt));
text(0, 30,['Binarisierte Streifen mit Parameter des adaptiven Tresholdings: ',...
    num2str(parDiffSt)],'FontSize',14,'Color',[0.3010 0.7450 0.9330]);
sp4=subplot('Position',[.5 0 .5 .5]); imshow(IbinSt(:,:,btSt));

end

% Zus�tzliches Verwenden von Grauwerten
% % Stelle sicher, dass es von Matlab als Bild verstanden wird
% mxst=max(IBinInd(:));
% IBinIndG=mat2gray(IBinInd, [0 mxst]);
% 
% % Berechne die mittleren x-y-Werte der Pixel von I, die zum einem Index
% % des Streifenbildes geh?ren
% regp=regionprops(IbinMsk,IBinIndG,'Centroid','PixelIdxList','MeanIntensity');
% mittIntens=[regp(:).MeanIntensity]'; indXYKr=round(mittIntens*2^btSt^2);
% mitPnkt=vertcat(regp(:).Centroid);
% 
% % Erstelle eine Liste mit x-y-Koordinaten der Korrespondenzen
% xyKrInd=nan(2^btSt^2,2);
% xyKrInd(indXYKr,:)=round(mitPnkt);
%
% Adaptiver Threshold nach Steger 
% AuffDiffTH=(AufnHll+AufnDnkl)/2;
% IbinSt=arrayfun(@(x) AufnDiffSt(:,:,x)>AuffDiffTH, 1:nast,...
%     'UniformOutput', false); IbinSt=cat(3,IbinSt{:});
% IbinStInv=arrayfun(@(x) AufnDiffStInv(:,:,x)>AuffDiffTH, 1:nast,...
%     'UniformOutput', false); IbinStInv=cat(3,IbinStInv{:});

