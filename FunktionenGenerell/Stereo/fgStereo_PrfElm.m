% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function prfElm=fgStereo_PrfElm(prfElm,messElm,TRef)


%% Pr�fmerkmale auswerten

nPrfElm=length(prfElm);
for i=1:nPrfElm
    
    % Auslesen der Messelement Informationen
    elmnTyp_i=prfElm(i).typP;
    messElm_i=prfElm(i).messElmP;
    
    switch elmnTyp_i
        case 'LotEbene'
            
            % Ebenen auslesen
            indMessEb1_i=strcmp({messElm(:).nameM},messElm_i{1});
            indMessEb2_i=strcmp({messElm(:).nameM},messElm_i{2});
            
            % Positionen und Richtungsvektor auslesen
            pos1_i=messElm(indMessEb1_i).posM;
            pos2_i=messElm(indMessEb2_i).posM;
            n2_i=messElm(indMessEb2_i).modellM.Normal;
            
            % Pr�fwert berechnen
            prfElmWert_i=abs((pos1_i-pos2_i)*n2_i');
            
        case 'D'
            
            % Zlyinder auslesen
            indMessZyl_i=strcmp({messElm(:).nameM},messElm_i);
            modell_i=messElm(indMessZyl_i).modellM;
            
            % Pr�fwert berechnen
            prfElmWert_i=modell_i.Radius*2;
            
        case 'W'
            
            % Ebenen auslesen
            indMessEb1_i=strcmp({messElm(:).nameM},messElm_i{1});
            indMessEb2_i=strcmp({messElm(:).nameM},messElm_i{2});
            
            % Positionen und Richtungsvektor auslesen
            n1_i=messElm(indMessEb1_i).modellM.Normal;
            n2_i=messElm(indMessEb2_i).modellM.Normal;
            
             % Pr�fwert berechnen
             % (https://www.mathworks.com/matlabcentral/answers/161515-how-do-i-find-the-angle-between-a-vector-and-a-line)
            prfElmWert_i=atan2d(norm(cross(n1_i,n2_i)),dot(n1_i,n2_i));
            
        case 'X'
            
            % X-Wert auslesen
            indMessZyl_i=strcmp({messElm(:).nameM},messElm_i);
            modell_i=messElm(indMessZyl_i).modellM;
            
            % Pr�fwert berechnen
            prfElmWert_i=modell_i.Pos(1);
            
        case 'Y'
            
            % Y-Wert auslesen
            indMessZyl_i=strcmp({messElm(:).nameM},messElm_i);
            modell_i=messElm(indMessZyl_i).modellM;
            
            % Pr�fwert berechnen
            prfElmWert_i=modell_i.Pos(2);
    end
    
    % �bergabe
    prfElm(i).prfElmWertP=prfElmWert_i;
    
end

%% Messabweichung berechnen


for i=1:nPrfElm
    
    % Auslesen der Messelement Informationen
    name_i=prfElm(i).nameP;
    elmnTyp_i=prfElm(i).typP;
    prfElmWert_i=prfElm(i).prfElmWertP;
    
    % Index des entsprechenden Referenzmessungeswertes bestimmen
    indr_i=strcmp(TRef.Merkmal,name_i);
    
    % Ggf.
    if any(indr_i)
        
        % Wert auslesen und Messabweichung berechnen
%         refWert_i=str2double(TRef.CalypsoMeasuringResult{indr_i});
        refWert_i=TRef.Messwert(indr_i);
        switch elmnTyp_i
            case 'W'
                abw_i=refWert_i*24-prfElmWert_i;                
            otherwise
                abw_i=refWert_i-prfElmWert_i;
        end
        
        %Messabweichung eintrage
        prfElm(i).abwRefP=abw_i;
    else
        
        % Messabeichung eintragen
        prfElm(i).abwRefP=NaN;
    end
    
end

end