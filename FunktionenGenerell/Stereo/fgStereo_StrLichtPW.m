% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [xyz_Kam,IBinInd_i,IBinSt_i]=...
    fgStereo_StrLichtPW(AufnProjU,kamPara,nSt,varargin)
%Erstelle eine Punktwolke aus Stereoaufnahmen, die mit strukturiertem Licht
%kodiert wurden

%% Parsen
defaultThDiffB=[]; defaultParDiffSt=.5; defaultVisBinErg=false;

p = inputParser;
addOptional(p,'thDiffB',defaultThDiffB);
addOptional(p,'parDiffSt',defaultParDiffSt);
addOptional(p,'visBinErg',defaultVisBinErg);
parse(p,varargin{:});

thDiffB=p.Results.thDiffB;
parDiffSt=p.Results.parDiffSt;
visBinErg=p.Results.visBinErg;


grAufn=size(AufnProjU); 
nkam=grAufn(4);

%% Dekodiere die Aufnahmen for jede Kamera
xyKrInd_ges=zeros(nSt.^2,2,nkam);
for i=1:nkam
    
    % Aufnahmen extrahieren
    AufnProjU_i=AufnProjU(:,:,:,i);
    
    % Korrespondenzen berechnen
    [xyKorrsInd_i,IBinInd_i,IBinSt_i]=...
        fgStereo_KorrsBinaer(AufnProjU_i,nSt,...
        'thDiffB',thDiffB,'parDiffSt',parDiffSt,'visBinErg',visBinErg);
    
    % �bergeben
    xyKrInd_ges(:,:,i)=xyKorrsInd_i;
    
end

%% Erstelle die Punktwolke

% Get rid of NaN
xyKorrsInd1_nan=xyKrInd_ges(:,:,1); xyKorrsInd2_nan=xyKrInd_ges(:,:,2);
nanInd1=isnan(xyKrInd_ges(:,1,1)); nanInd2=isnan(xyKrInd_ges(:,1,2));
xyKorrsInd1_nan(nanInd1 | nanInd2,:)=[];
xyKorrsInd2_nan(nanInd1 | nanInd2,:)=[];

% Korrigiere Verzeichnungen mit Haikillae
if isnan(kamPara.MeanReprojectionError)
    [xyKorrsInd1_nan,xyKorrsInd2_nan]=...
        fgKam_undistImageHeik(xyKorrsInd1_nan,xyKorrsInd2_nan,...
        grAufn,kamPara);    
end 

% Trianguliere
xyz_Kam=triangulate(xyKorrsInd1_nan,xyKorrsInd2_nan,kamPara);
end
