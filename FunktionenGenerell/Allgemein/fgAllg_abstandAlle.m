% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function A=fgAllg_abstandAlle(pos)
% Abstand aller Punkte zu allen Punkten

% Ermittle die Anzahl der Dimensionen
ndim=size(pos,2);

% Berechne den Abstand aller Feature zu allen Featuren
switch ndim
    case 2
        
        % 2Dminensional
        [X1, X2]=meshgrid(pos(:,1),pos(:,1));
        [Y1, Y2]=meshgrid(pos(:,2),pos(:,2));
        DX=X1-X2; DY=Y1-Y2; A=sqrt(DX.^2+DY.^2);
    case 3
        
        % 3Dimensional
        [X1, X2]=meshgrid(pos(:,1),pos(:,1));
        [Y1, Y2]=meshgrid(pos(:,2),pos(:,2));
        [Z1, Z2]=meshgrid(pos(:,3),pos(:,3));
        DX=X1-X2; DY=Y1-Y2; DZ=Z1-Z2; A=sqrt(DX.^2+DY.^2+DZ.^2);
end

end