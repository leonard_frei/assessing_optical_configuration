% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function inds_wnkl=funcAllg_winkelSort(centers)
% Sortiere Puntkekreiseförmig um den gemeinsamen Mittelpunkt

% Berechne den gemeinsamen Mittelpunkt
centerM=mean(centers);

% Berechne die Winkel zum absoluten Koordinensystem aller Punkte relativ
% zum Mittelpunkt
rCenter=centers-centerM;
wnkl=atan2(rCenter(:,2),rCenter(:,1));

% Berechne die sortierten Indizes
[~,inds_wnkl]=sort(wnkl);
end