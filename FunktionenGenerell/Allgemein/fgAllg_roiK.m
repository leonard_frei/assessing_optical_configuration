% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function Iroi=fgAllg_roiK(grI,k,r)
% Eine kreisförmige ROI-Maske erstellen

% Entpacke die Kreisinformationen 
if size(r)==1
    r_max=r; r_min=0;
else 
    r_max=r(2); r_min=r(1); 
end
kx=k(1); ky=k(2);

% Erstelle ein Netz
x=(1-kx):(grI(2)-kx);
y=(1-ky):(grI(1)-ky);
[X,Y]=meshgrid(x,y);

% Berechne, welche Radien innerhal des Kreisrings liegen
kond_min=r_min^2 < (X.^2+Y.^2);
kond_max=(X.^2+Y.^2) < r_max^2;
KRK=(kond_min & kond_max); % Kreisring-Kondition

% Fülle die neue Aufnahme
Iroi=false(grI);
Iroi(KRK)=true;

end