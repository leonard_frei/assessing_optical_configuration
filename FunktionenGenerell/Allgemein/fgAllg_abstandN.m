% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [A,ind]=fgAllg_abstandN(pos,pnkt,varargin)
% Abstand aller Punkte zu einem Punkte und Indizes der n n�hsten

%% Parsen

p = inputParser;

defaultK=false; addOptional(p,'k',defaultK);

parse(p,varargin{:});

k=p.Results.k;

%% Abst�nde und ausw�hlen

% Ermittle die Anzahl der Dimensionen
ndim=size(pos,2);

% Berechne den Abstand aller Feature zu allen Featuren
switch ndim
    case 2
        
        % 2Dminensional
        DX=pos(:,1)-pnkt(1); DY=pos(:,2)-pnkt(2); A=sqrt(DX.^2+DY.^2);
        [~,ind]=mink(A,k);
    case 3
        
        % 3Dimensional
        DX=pos(:,1)-pnkt(1); DY=pos(:,2)-pnkt(2); DZ=pos(:,3)-pnkt(3);
        A=sqrt(DX.^2+DY.^2+DZ.^2);
        [~,ind]=mink(A,k);
        
end

end