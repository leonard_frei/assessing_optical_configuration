% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function PnktKorr=f_kom_pl(fl_nm)
% Txt-Datei mit den Messpunkten der Maschine einlesen
fid = fopen(fl_nm,'r');
tmp = textscan(fid,'%s','Delimiter','\n'); %Cell-Array in dem jede Zeile des Messdokuments einzeln eingelesen wurde 
fclose(fid);

%Daten komprimieren
tmp=tmp{1}; %Tiefe des Cell-Arrays, in dem die Daten gespeichert sind, verkleinern
emptyCells = cellfun(@isempty,tmp); %Leere Zeilen finden
tmp(emptyCells) = []; %und aussortieren

% Indizes finden
indPnkt=contains(tmp,'x'); Pnkt=tmp(indPnkt); 
indVek=contains(tmp,'kraft'); KraftV=tmp(indVek); 
indTaster=contains(tmp,'status'); radii=tmp(indTaster); 

% Werte auslesen
Pnkt=cellfun(@(x) textscan(x,'%s %f %f %f'),Pnkt,'UniformOutput',false); 
Pnkt=vertcat(Pnkt{:}); Pnkt=cell2mat(Pnkt(:,2:4));
KraftV=cellfun(@(x) textscan(x,'%s %f %f %f'),KraftV,'UniformOutput',false); 
KraftV=vertcat(KraftV{:}); KraftV=cell2mat(KraftV(:,2:4));
radii=cellfun(@(x) textscan(x,'%s %f %f'),radii,'UniformOutput',false); 
radii=vertcat(radii{:}); radii=cell2mat(radii(:,3));

% Punkte mit Tasterantastvektor korrigieren
PnktKorr=Pnkt+KraftV.*repmat(radii,[1 3]);

end