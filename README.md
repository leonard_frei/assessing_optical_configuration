# Assessing the optical configuration of a structured light scanner in metrological use (Code for Paper)

11 January 2022

## Authors and acknowledgment
Leonard Schild - LeonardSchild@mailbox.org

Fabian Sasse - Fabian.Sasse@kit.edu

Jan-Philipp Kaiser - Jan-Philipp.Kaiser@kit.edu

## First Steps

1. Use script s01_Einrichtung_addpath to initialize all functions and data folders

2. Run exampliary evaluation to generate pictures by running "s03_Abbildungen_Paper"

3. Further data is available on https://dx.doi.org/10.35097/543. These need to be downloaded and included in folder "Daten" to run further scripts.

4. Take own images by running script "s02_Bild_AufnahmeErstellen"

## Abstract of Paper (to be handed in)

Structured light scanners for three-dimensional surface acquisition (3D scanners) are increasingly used for dimensional metrology. The optical configuration of 3D scanners (distance to object and baseline width) influences the triangulation process, on which the scanners’ measurement principle relies. So far, only a limited number of studies has investigated the optical configuration’s influence on the accuracy of a 3D scanner. To close this gap, this work presents a design of experiment in which the optical configuration of a 3D scanner is systematically varied and its influence on the accuracy evaluated. Further, tactile reference measurements allow to separate random from systematical errors, while a special test specimen is used in two different configurations to ensure general applicability of the findings. Thus, this work provides support when designing a 3D scanner by highlighting which optical configuration maximizes accuracy. 

## License

MIT License

Copyright (c) [2022] [Leonard Schild]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

