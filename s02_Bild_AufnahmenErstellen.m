% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. %% Aufnahmen mit Stereokamerasystem erstellen

%% Kalibrieren
close all; clear all; 

[AufnKal, kamPara]=fpStereoAufn_kal;

%% Punktewolke erstellen
close all; clear all; 

kalStr='20210113_1613_AufnKal_2_1.mat'; 
load(['DatenTemp\',kalStr]);
[AufnProj, PW_k, thDiffB, parDiffSt]=fpStereoAufn_PnktWlk(kamPara);