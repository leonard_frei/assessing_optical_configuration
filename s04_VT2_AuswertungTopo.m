% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

clear all; close all;

%% Automatisiert auswerten

% Alle Datein bestimmen
sstrRefMess='Daten/KMGMessungen/20210113_Versuchstag2_Topo/VT2_topo.xlsx';
pfd=dir('Daten/20210114_Versuchstag2');
nmexp={pfd(:).name}';

% Versuchstagebuch festlegen
strVT='Versuchstagebuch_Tag2.xlsx';

% Typen von Datein bestimmen
indFlach=contains(nmexp,'flach');
indProj=contains(nmexp,'AufnProj');
indKal=contains(nmexp,'AufnKal');
indMean=contains(nmexp,'kalMean');

%% Auswertung topografisch

% Indizes der topografischen Messungen
indTopoMess=(~indFlach) & indProj;
nmTopoMess=nmexp(indTopoMess); nTopoMess=size(nmTopoMess,1);

for i=1:nTopoMess
    
    % Aktuelle Messung
    nm_i=nmTopoMess{i};
    
    % Konfiguration der aktuellen Messung
    indUndscr=strfind(nm_i,'_');
    indMat=strfind(nm_i,'.mat');
    konfig_i=nm_i( (indUndscr(end-1)) : (indUndscr(end)) );
    mess_i=nm_i( (indUndscr(end)+1) );
    
    % Zugehörige Kalibrierungen 
    indMess_i=contains(nmexp,konfig_i);
    indKal_i= indMess_i & (~indFlach) & indKal & ~indMean;
    nmKal_i=nmexp(indKal_i); nKal_i=size(nmKal_i,1);
    
    for j=1:nKal_i
        
        % Akuelle Kalibrierung extrahieren und Strings erstellen
        nmKal_ij=nmKal_i{j}; 
        VersuchID=['VT2',konfig_i,mess_i,'_Kal_',num2str(j)];
        aufnKalStr={['Daten/20210114_Versuchstag2/',nmKal_ij],...
            ['Daten/20210114_Versuchstag2/',nm_i]}; % Aufnahmen
        disp(VersuchID);
        
        % Parameter laden und Auswertung durchführen
        s04a_VT2_Para; vis=true;
        
        % Kreisthreshold
        if (i==16)&&(j==4)
            thK=.9;
        elseif (i==16)&&(j==3)
            thK=.89;
        elseif (i==17)&&(j==3)
            thK=.89;
        elseif  (i==18 || i==19)
            thK=.89;
        else
            thK=.88;
        end
        
        % Auswertung
        s04a_VT2_Auswertung;
        disp(['Erledigt: ',num2str((i-1)*nKal_i+j),'/',num2str(nTopoMess*nKal_i)])
        
    end
    
end