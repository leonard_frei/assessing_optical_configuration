% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

%% Auswertung Flach Aufnahmen darstellen
clear all; close all;

% Ordner der Versuche
sstrRefMess='Daten/KMGMessungen/20210120_Versuchstag2_Flach/VT2_flach.xlsx'; 
pfd=dir('Daten/20210114_Versuchstag2/'); 
nmexp={pfd(:).name}';

% Typen von Datein bestimmen
indFlach=contains(nmexp,'flach');
indProj=contains(nmexp,'AufnProj');
indKal=contains(nmexp,'AufnKal');

%% Auswertung flach

% Indizes der topografischen Messungen
indTopoMess=(indFlach) & indProj;
nmTopoMess=nmexp(indTopoMess); nTopoMess=size(nmTopoMess,1);

for i=[1 6 11 16]
    
    % Aktuelle Messung
    nm_i=nmTopoMess{i};
    
    % Konfiguration der aktuellen Messung
    indUndscr=strfind(nm_i,'_');
    indMat=strfind(nm_i,'.mat');
    konfig_i=nm_i( (indUndscr(end-1)) : (indUndscr(end)) );
    mess_i=nm_i( (indUndscr(end)+1) );
    
    % Zugehörige Kalibrierungen
    indMess_i=contains(nmexp,konfig_i);
    indKal_i= indMess_i & (indFlach) & indKal;
    nmKal_i=nmexp(indKal_i); nKal_i=size(nmKal_i,1);
    
    for j=1
        
        % Akuelle Kalibrierung extrahieren und Strings erstellen
        nmKal_ij=nmKal_i{j};
        VersuchID=['VT2',konfig_i,mess_i,'_Kal_',num2str(j)];
        aufnKalStr={['Daten/20210114_Versuchstag2/',nmKal_ij],...
            ['Daten/20210114_Versuchstag2/',nm_i]}; % Aufnahmen
        disp(VersuchID);
        
        % Parameter laden und Auswertung durchführen
        s04a_VT2_ParaFlach; vis=false;
        
        % Kreisthreshold
        if any((i==[11 13 14 15 16 17 18 19 20]))
            thK=.95; radiusGr=[13 70];
        elseif i==[12]
            thK=.94; radiusGr=[13 70];
        else
            thK=.9;
        end
        
        % Aufnahmen laden
        load(aufnKalStr{1}); load(aufnKalStr{2});
        
        % Entzerre alle Aufnahmen
        AufnProjU=fgKam_undistImage(AufnProj,kamPara);
        
        % Einstellungen der kleinen Bohrungen laden
        Tbr=readtable('Daten/Tbr');
        if any(strcmp(Tbr.VersuchID,VersuchID))
            indV_i=strcmp(Tbr.VersuchID,VersuchID);
            Tbr_i=Tbr(indV_i,:);
            radiusKl_i=[Tbr_i.radius_1 Tbr_i.radius_2]; 
            sensKl_i=Tbr_i.sens;
        else
            radiusKl_i=[15 35]; sensKl_i=.9;
            Tbr_i.radius=radiusKl_i; Tbr_i.sens=sensKl_i; Tbr_i.VersuchID=VersuchID;
            Tbr_i=struct2table(Tbr_i);
            indV_i=size(Tbr,1)+1;
        end
        
        % Berechne die Punktwolke der kleinen Bohrungen (Summe 4x12=48)
        [xyzBkl_Kam, rBkl_Kam,radiusKl_i,sensKl_i,bohrKl]=fpStereoMu_BohrFlachKlein(...
            AufnProjU,kamPara,radiusKl_i,sensKl_i,'thK',thK,'vis',false,'inter',false);
                
        % Berechne die Punktewolke der drei Bohrungen auf der Grundplatte
        [xyzB_Kam, rB_Kam, bohrGr]=fpStereoMu_BohrFlach(AufnProjU,kamPara,radiusGr,...
            'thK',thK,'vis',false);
        
        %% Darstellen
        grBld=get(0,'ScreenSize'); grEing=300;
        fInter=figure('Position',[1 grEing grBld(3) grBld(4)-grEing]);
        sgtitle(VersuchID,'Interpreter','none')
        
        ax1=subplot(1,2,1); %ax1.Position=[0 0 1 1];
        imshow(AufnProjU(:,:,2,1)); hold on;
        plot(bohrKl(1).zentr(:,1),bohrKl(1).zentr(:,2),'*-','linewidth',1)
        viscircles(bohrKl(1).zentr, bohrKl(1).radii,'Color','b','LineWidth',1);
        plot(bohrGr(1).zentr(:,1),bohrGr(1).zentr(:,2),'*-','linewidth',1)
        viscircles(bohrGr(1).zentr, bohrGr(1).radii,...
            'LineWidth',.5,'Color',[0.8500 0.3250 0.0980]);
        ax1.Position=[0.005 0 .49 1];
        
        ax2=subplot(1,2,2);
        imshow(AufnProjU(:,:,2,2)); hold on;
        plot(bohrKl(2).zentr(:,1),bohrKl(2).zentr(:,2),'*-','linewidth',1)
        viscircles(bohrKl(2).zentr, bohrKl(2).radii,'Color','b','LineWidth',1);
        plot(bohrGr(2).zentr(:,1),bohrGr(2).zentr(:,2),'*-','linewidth',1)
        viscircles(bohrGr(2).zentr, bohrGr(2).radii,...
            'LineWidth',.5,'Color',[0.8500 0.3250 0.0980]);
        ax2.Position=[.505 0 .49 1];
        
        
    end
    
end