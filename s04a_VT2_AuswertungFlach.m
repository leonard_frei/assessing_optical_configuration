% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

%% Auswertung Versuchstag

%% Erstelle die Punktwolke

% Aufnahmen laden
load(aufnKalStr{1}); load(aufnKalStr{2});

% Entzerre alle Aufnahmen
AufnProjU=fgKam_undistImage(AufnProj,kamPara);

% Berechne die Punktewolke des Streifenmusters
xyz_Kam=fgStereo_StrLichtPW(AufnProjU,kamPara,nSt,...
    'thDiffB',thDiffB,'parDiffSt',parDiffSt,'visBinErg',false);
xyzC_Kam=xyz_Kam(xyz_Kam(:,3)<450 & xyz_Kam(:,3)>300,:);

%% Erstellung der Punktwolke

% Berechne die Punktewolke der drei Bohrungen auf der Grundplatte
[xyzB_Kam, rB_Kam]=fpStereoMu_BohrFlach(AufnProjU,kamPara,radiusGr,...
    'thK',thK,'vis',false);

% Einstellungen der kleinen Bohrungen laden und Punktwolke berechnen
Tbr=readtable('Daten/Tbr'); clear Tbr_i;
if any(strcmp(Tbr.VersuchID,VersuchID))
    
    % Lese definierte Werte aus
    indV_i=strcmp(string(Tbr.VersuchID),VersuchID);
    Tbr_i=Tbr(indV_i,:);
    radiusKl_i=[Tbr_i.radius_1, Tbr_i.radius_2]; 
    sensKl_i=Tbr_i.sens;
    
    % Berechne die Punktwolke der kleinen Bohrungen (Summe 4x12=48)
    [xyzBkl_Kam, rBkl_Kam,radiusKl_i,sensKl_i]=fpStereoMu_BohrFlachKlein(...
        AufnProjU,kamPara,radiusKl_i,sensKl_i,'thK',thK);
    
else
    
    % Definiere neue Werte
    radiusKl_i=[15 35]; sensKl_i=.9;
    Tbr_i.radius_1=radiusKl_i(1); Tbr_i.radius_2=radiusKl_i(2); 
    Tbr_i.sens=sensKl_i; Tbr_i.VersuchID=VersuchID;
    Tbr_i=struct2table(Tbr_i);
    indV_i=size(Tbr,1)+1;
    
    % Berechne die Punktwolke der kleinen Bohrungen (Summe 4x12=48)
    [xyzBkl_Kam, rBkl_Kam,radiusKl_i,sensKl_i]=fpStereoMu_BohrFlachKlein(...
        AufnProjU,kamPara,radiusKl_i,sensKl_i,'thK',thK,'inter',true);
    
    % Einstellungen der kleinen Bohrungen speichern
    Tbr_i.radius_1=radiusKl_i(1); Tbr_i.radius_2=radiusKl_i(2);
    Tbr_i.sens=sensKl_i;
    Tbr=[Tbr;Tbr_i];
    writetable(Tbr,'Daten/Tbr.xlsx');
end

% Erstelle Gesamtpunktwolke
PW_k=pointCloud([xyzB_Kam;xyzBkl_Kam;xyz_Kam]);
nBohr=size(xyzBkl_Kam,1); indBohr=3+(1:nBohr);

%% Berechnung der Messwerte

% Registriere die Punktewolke (Nullpunkt: obere linke Bohrung (in der Linie
% der Welle), x-Achse: Verbindung der beiden oberen Bohrungen, z-Achse:
% senkrecht zur oberen Fläche)
[PW_w, kam2world]=fpStereoMu_PunktWolkeReg(PW_k,dZEbene,'vis',true);
ac=gca; ac.Title.String=strrep(VersuchID,'_',' ');

% Berechnung der Messelemente
messElm=fgStereo_MessElm(messElm,PW_w,'visErg',false,...
    'Bohr',[PW_w.Location(indBohr,:), rBkl_Kam]);

% Berchnung der Prüfelemente
prfElm=fgStereo_PrfElm(prfElm,messElm,TRef); struct2table(prfElm);

%% Versuchstagebuch beschreiben

PrfElmW=[prfElm(:).prfElmWertP];
MessAbw=[prfElm(:).abwRefP];

nameP_VB=cellfun(@(x) strrep(x,' ','_'),nameP,'UniformOutput',false);
nameP_VB=cellfun(@(x) strrep(x,'+',''),nameP_VB,'UniformOutput',false);
nameAbw_VTB=cellfun(@(x) append('Abw_',x),nameP_VB,'UniformOutput',false);
nameWerte_VTB=cellfun(@(x) append('Werte_',x),nameP_VB,'UniformOutput',false);

fgAusw_vtagebuchEintrag(VersuchID,{'Aufnahmen','Kalibrierung'},aufnKalStr,'strVT',strVT);
fgAusw_vtagebuchEintrag(VersuchID,nameAbw_VTB,MessAbw,'strVT',strVT);
fgAusw_vtagebuchEintrag(VersuchID,nameWerte_VTB,PrfElmW,'strVT',strVT);
