% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

clear all; close all;

%% Automatisiert auswerten

% Alle Datein bestimmen
pfdAbb='Diagramme/';
pfd=dir('Daten/Beispiel');
sstrRefMess='Daten/Beispiel/VT2_topo.xlsx';
nmexp={pfd(:).name}';

% Typen von Datein bestimmen
indFlach=contains(nmexp,'flach');
indProj=contains(nmexp,'AufnProj');
indKal=contains(nmexp,'AufnKal');

%% Auswertung topografisch

% Indizes der topografischen Messungen
indTopoMess=(~indFlach) & indProj;
nmTopoMess=nmexp(indTopoMess); nTopoMess=size(nmTopoMess,1);

% Aktuelle Messung
nm_i=nmTopoMess{1};

% Konfiguration der aktuellen Messung
indUndscr=strfind(nm_i,'_');
indMat=strfind(nm_i,'.mat');
konfig_i=nm_i( (indUndscr(end-1)) : (indUndscr(end)) );
mess_i=nm_i( (indUndscr(end)+1) );

% Zugehörige Kalibrierungen
indMess_i=contains(nmexp,konfig_i);
indKal_i= indMess_i & (~indFlach) & indKal;
nmKal_i=nmexp(indKal_i); nKal_i=size(nmKal_i,1);

% Akuelle Kalibrierung extrahieren und Strings erstellen
nmKal_ij=nmKal_i{1};
VersuchID=['VT2',konfig_i,mess_i,'_Kal_',num2str(1)];
aufnKalStr={['Daten/Beispiel/',nmKal_ij],...
    ['Daten/Beispiel/',nm_i]}; % Aufnahmen
disp(VersuchID);

% Parameter laden und Auswertung durchführen
s04a_VT2_Para; vis=true;

%% Auswertung

% Aufnahmen laden
load(aufnKalStr{1}); load(aufnKalStr{2});

% Entzerre alle Aufnahmen
AufnProjU=fgKam_undistImage(AufnProj,kamPara);

% Berechne die Punktewolke des Streifenmusters
[xyz_Kam,IBinInd,IBinSt]=fgStereo_StrLichtPW(AufnProjU,kamPara,nSt,...
    'thDiffB',thDiffB,'parDiffSt',parDiffSt,'visBinErg',false);
xyzC_Kam=xyz_Kam(xyz_Kam(:,3)<450 & xyz_Kam(:,3)>300,:);

% Berechne die Punktewolke der drei Bohrungen auf der Grundplatte
[xyzB_Kam, rB_Kam]=fpStereoMu_Bohr(AufnProjU,kamPara,radiusGr,...
    'thK',.88,'vis',true);

% Erstelle Gesamtpunktwolke
PW_k=pointCloud([xyzB_Kam;xyz_Kam]);

% Registriere die Punktewolke (Nullpunkt: obere linke Bohrung (in der Linie
% der Welle), x-Achse: Verbindung der beiden oberen Bohrungen, z-Achse:
% senkrecht zur oberen Fläche)
[PW_w, kam2world]=fpStereoMu_PunktWolkeReg(PW_k,dZEbene,'vis',false);

%% Plots
close all;

% Aufnahmen vor Binarisierung
fauf=figure; montage(AufnProjU(:,:,3:end,1));
fauf.Color=[1 1 1];
sstr_fauf=[pfdAbb,'methods_images'];

% Aufnahmen nach Binarisierung und Verarbeitung
fbin=figure; montage(IBinSt(:,:,:,1));
fbin.Color=[1 1 1];
sstr_fbin=[pfdAbb,'methods_images_binarized'];

% Aufnahmen nach Binarisierung und Verarbeitung
find=figure; imshow(IBinInd,[]);
find.Color=[1 1 1];
sstr_find=[pfdAbb,'methods_images_indices'];

% Punktwolke nach Registrierung
roiP = [-50 250 -20 200 -20 50]; indP=findPointsInROI(PW_w,roiP);
PW_wp=select(PW_w,indP);
fpc=figure;
pcshow(PW_wp,'MarkerSize',10);
title('Point cloud after registration')
xlabel('X'); ylabel('Y'); zlabel('Z');
sstr_pc=[pfdAbb,'methods_pointCloud'];

%% Export
export_fig(fauf,sstr_fauf,'-jpg','-r150')
export_fig(fbin,sstr_fbin,'-jpg','-r150')
export_fig(find,sstr_find,'-jpg','-r150')
export_fig(fpc,sstr_pc,'-jpg','-r150')
