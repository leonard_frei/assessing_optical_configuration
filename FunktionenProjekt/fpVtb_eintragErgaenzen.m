% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function fpVtb_eintragErgaenzen(versuchID,spName,spWerte)
% Erg�nze einen Eintrag im Versuchstagebuch

% Lese das Versuchstagebuch ein
Tvt=readtable('Versuchstagebuch.xlsx'); neintr=size(Tvt,1);

% Falls Eingangswerte numerisch, in Zelle umwandeln
if isnumeric(spWerte); spWerte=num2cell(spWerte); end

% Entferne Umlaute
spName=strrep(spName,'�','a');

% Pr�fe, ob es schon einen Eintrag gibt
indND=strcmp(Tvt.VersuchID,versuchID);

% F�ge ggf. einen neuen Eintrag hinzu
if ~any(indND)
    T_i=Tvt(1,:); nsp=size(T_i,2);
    T_i.VersuchID={versuchID}; 
    for i=2:nsp
        if iscell(T_i{1,i})
            T_i{1,i}={' '};
        elseif isnumeric(T_i{1,i})
            T_i{1,i}=NaN;
        end
    end
    Tvt=[Tvt;T_i]; neintr=neintr+1; indND=[indND; true];
end

% Korrekte Zeile finden
T_i=Tvt(indND,:); nmsAlt=T_i.Properties.VariableNames; 

% Herausfinden, welche Spalten bereits vorhanden sind
indVorNvor=cellfun(@(x) any(contains(nmsAlt,x)),spName);

% Vorhandene Spalten beschreiben
indVor=cellfun(@(x) find(contains(nmsAlt,x)),spName(indVorNvor));
if ~isempty(indVor); T_i(1,indVor)=spWerte(indVorNvor); end
Tvt(indND,:)=T_i;

% Nicht vorhandene Spalten hinzuf�gen
if sum(~indVorNvor)>0
    wTneu=num2cell(zeros(neintr,sum(~indVorNvor)));
    wTneu(indND,:)=spWerte(~indVorNvor);
    TspNeu=array2table(wTneu);
    TspNeu.Properties.VariableNames=spName(~indVorNvor);
    Tvt=[Tvt,TspNeu];
end

% Versuchstagebuch erg�nzen
writetable(Tvt,'Versuchstagebuch.xlsx')

end