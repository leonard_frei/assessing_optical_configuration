% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function fpVtb_eintragAnlegen(spName,spWerte)
% Erstelle eine neue Zeile im Versuchstagebuch

% Lese das Versuchstagebuch ein
Tvt=readtable('Versuchstagebuch.xlsx'); nspVt=size(Tvt,2);
spTvt=Tvt.Properties.VariableNames;

% Finde die Spalten
spInd=cellfun(@(x) contains(spTvt,x),spName,'UniformOutput',false);
spInd=cellfun(@find,spInd);

% Erstelle einen Eintrag im Versuchstagebuch 
Ti=Tvt(1,:); Ti{1,:}=zeros(1,nspVt);
Ti{1,spInd}=spWerte;

% Zusammenfügen und speichern
Tvt=[Tvt;Ti]; writetable(Tvt,'Versuchstagebuch.xlsx');

end