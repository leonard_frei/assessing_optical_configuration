% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [AufnProj, PW_k, thDiffB, parDiffSt]=...
    fpStereoAufn_PnktWlk(kamPara,varargin)
% Erstelle eine Aufnahmen mit dem Streifenlichtprojektor

%% Parameter

% Default-Parameter
p=inputParser;
nSt_def=128; addOptional(p,'nSt',nSt_def); % Anzahl der Streifen
sxPr_def=1920; addOptional(p,'sxPr',sxPr_def); % Projektoraufl?sung 
syPr_def=1080; addOptional(p,'syPr',syPr_def); % Projektoraufl?sung 
mxstPr_def=1024; addOptional(p,'mxstPr',mxstPr_def); % Pixel des Quadrats, das für die Streifen verwendet wird
lprj_def=[1 0]; addOptional(p,'lprj',lprj_def); % Leuchtstärke des Projektors (Grauwertdifferenz zwischen hellen und dunklen Bereichen sowie dunkelster Grauwert)
thDiffB_def=.05; addOptional(p,'thDiffB',thDiffB_def); % Parameter für die Binarisierung
parDiffSt_def=.5; addOptional(p,'parDiffSt',parDiffSt_def); % Parameeter für die Binarisierung
vis_def=true; addOptional(p,'vis',vis_def);

% Parsen
parse(p,varargin{:});
nSt=p.Results.nSt; sxPr=p.Results.sxPr; syPr=p.Results.syPr;  
mxstPr=p.Results.mxstPr; lprj=p.Results.lprj; thDiffB=p.Results.thDiffB;  
parDiffSt=p.Results.parDiffSt; vis=p.Results.vis;

%% Nehme die Aufnahmen auf

% Erstelle Bilder mit Bildercode, die projeziert werden sollen
IProj=fgKam_Ibincode(sxPr,syPr,mxstPr,nSt,lprj);

% Aufnahmen erstellen oder Laden
[kam, src, figKam] = fgKam_init(IProj(:,:,end/2-1));
AufnProj=fgKam_aufnErst(kam,src,'IProj',IProj);

%% Erstelle die Punktwolke

rueck=[thDiffB,parDiffSt];
while ~isempty(rueck)

% Aktuelle Parameter auspacken
thDiffB=rueck(1); parDiffSt=rueck(2);

% Entzerre alle Aufnahmen
AufnProjU=fgKam_undistImage(AufnProj,kamPara);

% Berechne die Punktewolke des Streifenmusters
xyz_Kam=fgStereo_StrLichtPW(AufnProjU,kamPara,nSt,...
    'thDiffB',thDiffB,'parDiffSt',parDiffSt,'visBinErg',vis);

% Erstelle Gesamtpunktwolke
PW_k=pointCloud(xyz_Kam);

% Visualisieren
if vis; figure; pcshow(PW_k,'MarkerSize',25); end

disp(' ')
disp('Sieht die Punktwolke gut aus? Parameter für die Binarisierung: '),
disp(['[thDiffB parDiffSt]: [',...
    num2str(thDiffB),' ',num2str(parDiffSt),']'])
disp('Falls nicht im Format [thDiffB parDiffSt] neu spezifizieren ')
rueck=input(' ');
close all;

end

%% Speichern

% Abfrage nach Speicherstring
disp(' ')
disp('Wie lautet der Name der Speicherdatei? ')
disp('Als String formatieren') 
spStr=input(' ');

if isempty(spStr)
    save(['DatenTemp/',datestr(now,'yyyymmdd_HHMM'),'_AufnProj_Temp'], ...
        'AufnProj', 'PW_k', 'thDiffB', 'parDiffSt');
else
    save(['DatenTemp/',datestr(now,'yyyymmdd_HHMM'),'_AufnProj_',spStr],...
        'AufnProj', 'PW_k', 'thDiffB', 'parDiffSt');
end


end