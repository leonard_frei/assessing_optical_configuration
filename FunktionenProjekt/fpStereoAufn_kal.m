% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [AufnKal, kamPara]=fpStereoAufn_kal(varargin)
% Erstelle Aufnahmen mit einer Stereokamera und führe mit diesen eine
% Kalibrierung durch

% Default-Parameter
p=inputParser;
nI_def=5; addOptional(p,'nI',nI_def); % Anzahl der Bilder
squareSize_def=15; addOptional(p,'squareSize',squareSize_def); % Größe der Schachbrettquadrate
zielKonfg_def='schachbrett'; addOptional(p,'zielKonfg',zielKonfg_def); % Konfiguration des Kalibrierziels
interakt_def=true;  addOptional(p,'interakt',interakt_def); % Abfrage, die Belichtungszeit interaktiv gewählt wird

% Parsen
parse(p,varargin{:});
nI=p.Results.nI;
squareSize=p.Results.squareSize; 
zielKonfg=p.Results.zielKonfg;
interakt=p.Results.interakt; 

%% Erstelle Aufnahmen und führe mit diesen eine Kalibrierung durch

% Aufnahmen erstellen oder Laden
[kam, src, figKam] = fgKam_init;
AufnKal=fgKam_aufnErst(kam,src,'nI',nI,'interakt',interakt);
kamPara=fgKam_kamKal(AufnKal,'zielKonfg',zielKonfg,'sqSize',squareSize,...
    'wpAkq','schachbrett','kalAlgo','Matlab');
kamPara

% Kameraverbindungen schließen 
close all; 

% Darstellen
nkam=size(AufnKal,5);
for i=1:nkam; figure; montage(AufnKal(:,:,:,:,i)); end
figure;showExtrinsics(kamPara); 

%% Speichern

% Abfrage nach Speicherstring
disp('Wie lautet der Name der Speicherdatei? (Als String formatieren)') 
spStr=input(' ');

if isempty(spStr)
    save(['DatenTemp/',datestr(now,'yyyymmdd_HHMM'),'_AufnKal_Temp'], ...
        'AufnKal', 'kamPara');
else
    save(['DatenTemp/',datestr(now,'yyyymmdd_HHMM'),'_AufnKal_',spStr],...
        'AufnKal', 'kamPara');
end

end