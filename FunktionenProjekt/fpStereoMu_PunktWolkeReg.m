% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [PW_w, kam2world]=fpStereoMu_PunktWolkeReg(PW_k,dZEbene, varargin)
% Punktwolke registrieren

%% Parsen

p=inputParser; 
defaultVis=false; addOptional(p,'vis',defaultVis);
parse(p,varargin{:})
vis = p.Results.vis;

%% Grobregistrierung

% Achsen
yAchse=PW_k.Location(3,:)-PW_k.Location(1,:); yAchse=yAchse/norm(yAchse);
xAchseG=PW_k.Location(2,:)-PW_k.Location(1,:); xAchseG=xAchseG/norm(xAchseG);
xAchseGProj=xAchseG-yAchse*(xAchseG*yAchse');
zAchseG=cross(xAchseGProj,yAchse);

% Tranformation berechnen
Rwolrd2kamG=[xAchseGProj', yAchse', zAchseG']';
urspG=PW_k.Location(1,:);
world2kamG=affine3d([Rwolrd2kamG,zeros(3,1); urspG 1]);
kam2worldG=invert(world2kamG);

% Transformation
PW_wg = pctransform(PW_k,kam2worldG);

%% Feinregistrierung

% % z-Achse erstellen
roi = [-10 18 10 150 -2 2];
indZ=findPointsInROI(PW_wg,roi);
PWZEbene=select(PW_k,indZ);
% [zEbene,inlierIndices,outlierIndices]=pcfitplane(PWZEbene,dZEbene,...
%     'MaxNumTrials',10000,'Confidence',99.999);
[zEbene, inlierIndices]=fgPnktWlk_AsglrEbene(PWZEbene,dZEbene);
zAchseG=zEbene.Normal; if zAchseG(3)>0; zAchseG=-zAchseG; end

% Tranformation berechnen
yAchseProj=yAchse-zAchseG*(yAchse*zAchseG');
xAchseG=cross(yAchseProj,zAchseG);
Rwolrd2kam=[xAchseG', yAchseProj', zAchseG']';
ursp=PW_k.Location(1,:);
world2kam=affine3d([Rwolrd2kam,zeros(3,1); ursp 1]);
kam2world=invert(world2kam);

% Transformation
PW_w = pctransform(PW_k,kam2world);


%% Plotten
if vis
roiP = [-50 250 -20 200 -20 50]; indP=findPointsInROI(PW_w,roiP);
PW_wp=select(PW_w,indP); PWZEbeneP=select(PW_w,indZ);
figure; pcshow(PW_wp,'MarkerSize',35); 
hold on; pcshow(PWZEbeneP.Location,[0 1 0],'MarkerSize',35);
title('Punktwolke nach Registrierung')
end


end
