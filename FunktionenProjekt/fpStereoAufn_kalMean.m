% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function meanStereoCamPara = fpStereoAufn_kalMean(kalStr, pathMean)
% kal_str: struct with strings of kcalibrations ({'kal_1', 'kal_2})

    camPara1 = {};
    camPara2 = {};
    RoC2 = {};
    ToC2 = {};
    FM = {};
    EM = {};
    
    numKal = size(kalStr);
    
    for i=1:numKal(2)
        filepath = fullfile(pathMean, kalStr{i});
        file = load(filepath);
        camPara1{i} = file.kamPara.CameraParameters1;
        camPara2{i} = file.kamPara.CameraParameters2;
        RoC2{i} = file.kamPara.RotationOfCamera2;
        ToC2{i} = file.kamPara.TranslationOfCamera2;
    end

    RoC2_mean = mean(cat(3,RoC2{:}),3);
    ToC2_mean = mean(cat(3,ToC2{:}),3);

    meanCamPara1 = funcUnter_meanCam(camPara1);
    meanCamPara2 = funcUnter_meanCam(camPara2);

    meanStereoCamPara = stereoParameters(meanCamPara1, meanCamPara2, RoC2_mean, ToC2_mean);

    function meanCamPara = funcUnter_meanCam(camPara)

        RD = {}; TD = {}; IM = {};

        numKal = size(camPara);

        for i=1:numKal(2)
            RD{i} = camPara{i}.RadialDistortion;
            TD{i} = camPara{i}.TangentialDistortion;
            IM{i} = camPara{i}.IntrinsicMatrix;
        end
        RD_mean = mean(cat(3,RD{:}),3);
        TD_mean = mean(cat(3,TD{:}),3);
        IM_mean = mean(cat(3,IM{:}),3);

        meanCamPara = cameraParameters('RadialDistortion', RD_mean, 'TangentialDistortion', TD_mean, 'IntrinsicMatrix', IM_mean);

    end
end
