% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [xyzB_Kam, rB_Kam]=fpStereoMu_Bohr(AufnProjU,kamPara,...
    radiusGr,varargin)
% Ermittle die Positionen der Bohrungen des MU Stereo-Bauteils

%% Parsen

p=inputParser; 

defaultVis=false; addOptional(p,'vis',defaultVis);
defaultRrngG=-20; addOptional(p,'rRngG',defaultRrngG);
defaultRrngK=25; addOptional(p,'rRngK',defaultRrngK);
defaultThK=.9; addOptional(p,'thK',defaultThK);

parse(p,varargin{:})

vis = p.Results.vis;
rRngG=p.Results.rRngG;
rRngK=p.Results.rRngK;
thK=p.Results.thK;

%% Berechne die Korrespondenzen zwischen Muster und Aufnahme und trianguliere

% Aufnahmen extrahieren
AufnProj1=AufnProjU(:,:,:,1); AufnProj2=AufnProjU(:,:,:,2); 

%% Kreisbohrungen ermitteln und Triangulieren

% Bohrungsbilder extrahieren
AufnBohr1=AufnProj1(:,:,2); AufnBohr2=AufnProj2(:,:,2);

% Bohrungen detektiren
[zentr1,radii1]=imfindcircles(AufnBohr1,[rRngG 0]+radiusGr(2),...
    'ObjectPolarity','dark','Sensitivity',thK);
[zentr2,radii2]=imfindcircles(AufnBohr2,[rRngG 0]+radiusGr(2),...
    'ObjectPolarity','dark','Sensitivity',thK);

%% Kreisbohrungen sortieren und triangulieren

% Berechne die Abst�nde von allen zu allen Bohrunge
A1_gr=fgAllg_abstandAlle(zentr1);
A2=fgAllg_abstandAlle(zentr2);

% Sortiere die Bohrungen gem�� ihrer Abst�nde zueinander
A1s=sum(A1_gr); [~,indB1]=sort(A1s); 
A2s=sum(A2); [~,indB2]=sort(A2s);

% Speichere sie soriert
zentr1_s=zentr1(indB1,:); radii1_s=radii1(indB1,:);
zentr2_s=zentr2(indB2,:); radii2_s=radii2(indB2,:);

% Korrigiere Verzeichnungen mit Haikillae
if isnan(kamPara.MeanReprojectionError)
    grAufn=size(AufnBohr1); 
    [zentr1_s,zentr2_s]=...
        fgKam_undistImageHeik(zentr1_s,zentr2_s,grAufn,kamPara);    
end 

% Bohrungen aus Bild 1 und 2 triangulieren
xyzB_Kam=triangulate(zentr1_s,zentr2_s,kamPara);

% Bohrungsdurchmesser �ber Abbildungsma�stab berechnen
f=mean(kamPara.CameraParameters1.FocalLength);
abMstb=xyzB_Kam(:,3)./f;
rB_Kam=radii1_s.*abMstb;

%% Kreisbohrungen ermitteln 

% Bohrungsbilder extrahieren
AufnBohr1=AufnProj1(:,:,2); AufnBohr2=AufnProj2(:,:,2);

% Bohrungen detektiren
[zentrK1,radiiK1]=imfindcircles(AufnBohr1,[0 rRngK]+radiusGr(1),...
    'ObjectPolarity','dark','Sensitivity',thK);
[zentrK2,radiiK2]=imfindcircles(AufnBohr2,[0 rRngK]+radiusGr(1),...
    'ObjectPolarity','dark','Sensitivity',thK);

%% Kleine Kreisbohrungen sortieren

% Kleine Bohrungen sortieren 
[zentrK1_s,radiiK1_s]=fu_sortK(zentr1_s, zentrK1, radiiK1); 
[zentrK2_s,radiiK2_s]=fu_sortK(zentr2_s, zentrK2, radiiK2); 

% Korrigiere Verzeichnungen mit Haikillae
if isnan(kamPara.MeanReprojectionError)
    grAufn=size(AufnBohr1); 
    [zentrK1_s,zentrK2_s]=...
        fgKam_undistImageHeik(zentrK1_s,zentrK2_s,grAufn,kamPara);    
end 

% Bohrungen 1 und 2 triangulieren
xyzBK_Kam=triangulate(zentrK1_s,zentrK2_s,kamPara);

% Bohrungsdurchmesser �ber Abbildungsma�stab berechnen
abMstbK=xyzBK_Kam(:,3)./f;
rBK_Kam=radiiK1_s.*abMstbK;

%% �bergabe

xyzB_Kam=[xyzB_Kam; xyzBK_Kam];
rB_Kam=[rB_Kam; rBK_Kam];

%% Darstellung 

if vis
    
    figure; imshow(AufnBohr1); hold on; 
    viscircles(zentr1_s, radii1_s,'Color','b'); text(zentr1_s(:,1),zentr1_s(:,2),num2str(radii1_s),'color','r'); 
    viscircles(zentrK1_s, radiiK1_s,'Color','r'); text(zentrK1_s(:,1),zentrK1_s(:,2),num2str(radiiK1_s),'color','r'); 
    plot(zentr1_s(:,1),zentr1_s(:,2),zentrK1_s(:,1),zentrK1_s(:,2)); 
    
    figure; imshow(AufnBohr2); hold on; 
    viscircles(zentr2_s, radii2_s,'Color','b'); text(zentr2_s(:,1),zentr2_s(:,2),num2str(radii2_s),'color','r'); 
    viscircles(zentrK2_s, radiiK2_s,'Color','r'); text(zentrK2_s(:,1),zentrK2_s(:,2),num2str(radiiK2_s),'color','r'); 
    plot(zentr2_s(:,1),zentr2_s(:,2),zentrK2_s(:,1),zentrK2_s(:,2)); 
end

end

%% Unterfunktionen

function [zentrK1_s,radiiK1_s]=fu_sortK(zentr1_s, zentrK1, radiiK1)
% Kleine Bohrungen sortieren 

% Zwischen gro�en und kleinen Bohrungen unterscheiden
[~,indGr1]=sort(radiiK1,'descend');
zentrK1_gr=zentrK1(indGr1(1:3),:); radiiK1_gr=radiiK1(indGr1(1:3));
zentrK1_kl=zentrK1(indGr1(4:end),:); radiiK1_kl=radiiK1(indGr1(4:end));

% Gro�e Bohrungen extrahieren und anhand Abstand zur y-Bohrung sortieren
D1_gr=zentrK1_gr-zentr1_s(3,:); A1_gr=sqrt(D1_gr(:,1).^2+D1_gr(:,2).^2);
[~,indA1_gr]=sort(A1_gr);
zentrK1_grs=zentrK1_gr(indA1_gr,:); radiiK1_grs=radiiK1_gr(indA1_gr);

% Kleine Bohrungen nhach Abstand zur y-Bohrung sortieren
D1_kl=zentrK1_kl-zentr1_s(3,:);  A1_kl=sqrt(D1_kl(:,1).^2+D1_kl(:,2).^2);
[~,indA1_kl]=sort(A1_kl);
zentrK1_klsy=zentrK1_kl(indA1_kl,:); radiiK1_klsy=radiiK1_kl(indA1_kl);

% Kleine Bohrungen im Viereck sortieren
zentrK1_s=zeros(12,2); radiiK1_s=zeros(12,1); 
for i=1:3;
    
    % Aktuelle gro�e Bohrung ausw�hlen
    indkl_i=(1:3)+(i-1)*3;
    zentrK1Gr_i=zentrK1_grs(i,:);
    zentrK1_kli=zentrK1_klsy(indkl_i,:);
    radiiK1_kli=radiiK1_klsy(indkl_i);
    
    % Grundwinkel besteimmen
    d1_i=zentrK1Gr_i-zentr1_s(3,:); w1_i=atan2(d1_i(2),d1_i(1));
    
    % Winkel zwischen kleinen Bohrungen und gro�er Bohrung berechnen
    D1_i=zentrK1Gr_i-zentrK1_kli;
    wkl1_i=atan2(D1_i(:,2),D1_i(:,1)); 
    wkl1_i(wkl1_i<0)=wkl1_i(wkl1_i<0)+2*pi;
    wd1_i=w1_i-wkl1_i;
    [~,indws_i]=sort(wd1_i);
    
    % Zusammenf�gen
    ind_i=(i-1)*4+(1:4);
    zentrK1_s(ind_i,:)= [zentrK1Gr_i; zentrK1_kli(indws_i,:)];
    radiiK1_s(ind_i,:)=[radiiK1_grs(i); radiiK1_kli(indws_i)];
    
end

end
    
