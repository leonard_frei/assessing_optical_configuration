% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [xyzB_Kam, rB_Kam, bohr]=fpStereoMu_BohrFlach(AufnProjU,kamPara,...
    radiusGr,varargin)
% Ermittle die Positionen der Bohrungen des MU Stereo-Bauteils

%% Parsen

p=inputParser; 

defaultVis=false; addOptional(p,'vis',defaultVis);
defaultRrngG=-20; addOptional(p,'rRngG',defaultRrngG);
defaultRrngK=25; addOptional(p,'rRngK',defaultRrngK);
defaultThK=.9; addOptional(p,'thK',defaultThK);

parse(p,varargin{:})

vis = p.Results.vis;
rRngG=p.Results.rRngG;
rRngK=p.Results.rRngK;
thK=p.Results.thK;

%% Berechne die Korrespondenzen zwischen Muster und Aufnahme und trianguliere

% Aufnahmen extrahieren
AufnProj1=AufnProjU(:,:,:,1); AufnProj2=AufnProjU(:,:,:,2); 

%% Kreisbohrungen ermitteln und Triangulieren

% Bohrungsbilder extrahieren
AufnBohr1=AufnProj1(:,:,2); AufnBohr2=AufnProj2(:,:,2);

% Bohrungen detektiren
[zentr1,radii1]=imfindcircles(AufnBohr1,[rRngG 0]+radiusGr(2),...
    'ObjectPolarity','dark','Sensitivity',thK);
[zentr2,radii2]=imfindcircles(AufnBohr2,[rRngG 0]+radiusGr(2),...
    'ObjectPolarity','dark','Sensitivity',thK);

%% Kreisbohrungen sortieren und triangulieren

% Berechne die Abst�nde von allen zu allen Bohrunge
A1_gr=fgAllg_abstandAlle(zentr1);
A2=fgAllg_abstandAlle(zentr2);

% Sortiere die Bohrungen gem�� ihrer Abst�nde zueinander
A1s=sum(A1_gr); [~,indB1]=sort(A1s); 
A2s=sum(A2); [~,indB2]=sort(A2s);

% Speichere sie soriert
zentr1_s=zentr1(indB1,:); radii1_s=radii1(indB1,:);
zentr2_s=zentr2(indB2,:); radii2_s=radii2(indB2,:);

% Korrigiere Verzeichnungen mit Haikillae
if isnan(kamPara.MeanReprojectionError)
    grAufn=size(AufnBohr1); 
    [zentr1_s,zentr2_s]=...
        fgKam_undistImageHeik(zentr1_s,zentr2_s,grAufn,kamPara);    
end 

% Bohrungen aus Bild 1 und 2 triangulieren
xyzB_Kam=triangulate(zentr1_s,zentr2_s,kamPara);

% Bohrungsdurchmesser �ber Abbildungsma�stab berechnen
f=mean(kamPara.CameraParameters1.FocalLength);
abMstb=xyzB_Kam(:,3)./f;
rB_Kam=radii1_s.*abMstb;

% �bergabe 
bohr(1).zentr=zentr1_s; bohr(1).radii=radii1_s; 
bohr(2).zentr=zentr2_s; bohr(2).radii=radii2_s; 

%% Darstellung 
 
if vis
    
    figure; imshow(AufnBohr1); hold on; 
    viscircles(zentr1_s, radii1_s,'Color','b'); text(zentr1_s(:,1),zentr1_s(:,2),num2str(radii1_s),'color','r'); 
    
    figure; imshow(AufnBohr2); hold on; 
    viscircles(zentr2_s, radii2_s,'Color','b'); text(zentr2_s(:,1),zentr2_s(:,2),num2str(radii2_s),'color','r'); 

end

end
