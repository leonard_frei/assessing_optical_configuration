% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

function [xyzB_Kam, rB_Kam, radius, sens, bohr]=fpStereoMu_BohrFlachKlein(...
    AufnProjU,kamPara,radius,sens,varargin)
% Ermittle die Positionen der Bohrungen des MU Stereo-Bauteils

%% Parsen

p=inputParser;

defaultVis=false; addOptional(p,'vis',defaultVis);
defaultRrngG=-20; addOptional(p,'rRngG',defaultRrngG);
defaultRrngK=25; addOptional(p,'rRngK',defaultRrngK);
defaultThK=.9; addOptional(p,'thK',defaultThK);
defaultInter=false; addOptional(p,'inter',defaultInter);

parse(p,varargin{:})

vis = p.Results.vis;
rRngG=p.Results.rRngG;
rRngK=p.Results.rRngK;
thK=p.Results.thK;
inter=p.Results.inter;

%% Berechne die Korrespondenzen zwischen Muster und Aufnahme und trianguliere

% Aufnahmen extrahieren
AufnProj1=AufnProjU(:,:,:,1); AufnProj2=AufnProjU(:,:,:,2);

%% Kreisbohrungen ermitteln und Triangulieren

% Bohrungsbilder extrahieren
AufnBohr1=AufnProj1(:,:,2); AufnBohr2=AufnProj2(:,:,2); 
grAufn1=size(AufnBohr1); grAufn2=size(AufnBohr2); 

% Ohne Nutzerinteraktion
if ~inter
    % Bohrungen detektieren
    [zentr1,radii1]=imfindcircles(AufnBohr1,radius,...
        'ObjectPolarity','dark','Sensitivity',sens);
    [zentr2,radii2]=imfindcircles(AufnBohr2,radius,...
        'ObjectPolarity','dark','Sensitivity',sens);
    
    % Bohrungen sortieren
    [zentr1_s,radii1_s]=fu_sortK(zentr1,radii1,grAufn1);
    [zentr2_s,radii2_s]=fu_sortK(zentr2,radii2,grAufn2);
end

% Mit Nutzerinteraktion
while inter
    
    % Bohrungen detektieren
    [zentr1,radii1]=imfindcircles(AufnBohr1,radius,...
        'ObjectPolarity','dark','Sensitivity',sens);
    [zentr2,radii2]=imfindcircles(AufnBohr2,radius,...
        'ObjectPolarity','dark','Sensitivity',sens);
    
    % Bohrungen sortieren
    [zentr1_s,radii1_s]=fu_sortK(zentr1,radii1,grAufn1);
    [zentr2_s,radii2_s]=fu_sortK(zentr2,radii2,grAufn2);
        
    % Darstellen
    grBld=get(0,'ScreenSize'); grEing=300;
    fInter=figure('Position',[1 grEing grBld(3) grBld(4)-grEing]);
    
    ax1=subplot(1,2,1); %ax1.Position=[0 0 1 1];
    imshow(AufnBohr1); hold on;
    plot(zentr1_s(:,1),zentr1_s(:,2),'*-','linewidth',2)
    viscircles(zentr1_s, radii1_s,'Color','b');
    text(zentr1_s(:,1),zentr1_s(:,2),num2str(radii1_s),'color','r');
    
    ax2=subplot(1,2,2);
    imshow(AufnBohr2); hold on;
    plot(zentr2_s(:,1),zentr2_s(:,2),'*-','linewidth',2)
    viscircles(zentr2_s, radii2_s,'Color','b');
    text(zentr2_s(:,1),zentr2_s(:,2),num2str(radii2_s),'color','r');
    ax1.Position=[0.005 0 .49 1]; ax2.Position=[.505 0 .49 1];
    
    % Nutzer nach Erkennungszustand fragen
    disp(' '); 
    disp('Wurden allen Kreise korrekt gefunden?')
    disp(num2str([radius,sens]))
    rueck=input('[radiusKlein radius Gross sens] ');
    
    close(fInter);  
    
    if isempty(rueck)
        inter=false;
    else
        radius=rueck(1:2); sens=rueck(3);
    end
    
end

%% Kreisbohrungen sortieren und triangulieren

% Bohrungen aus Bild 1 und 2 triangulieren
xyzB_Kam=triangulate(zentr1_s,zentr2_s,kamPara);

% Bohrungsdurchmesser über Abbildungsmaßstab berechnen
f=mean(kamPara.CameraParameters1.FocalLength);
abMstb=xyzB_Kam(:,3)./f;
rB_Kam=radii1_s.*abMstb;

% Überagbe
bohr(1).zentr=zentr1_s; bohr(1).radii=radii1_s; 
bohr(2).zentr=zentr2_s; bohr(2).radii=radii2_s;

%% Darsstellung
if vis   
    grBld=get(0,'ScreenSize'); grEing=300;
    fInter=figure('Position',[1 grEing grBld(3) grBld(4)-grEing]);
        
    ax1=subplot(1,2,1); %ax1.Position=[0 0 1 1];
    imshow(AufnBohr1); hold on;
    plot(zentr1_s(:,1),zentr1_s(:,2),'*-','linewidth',2)
    viscircles(zentr1_s, radii1_s,'Color','b');
    text(zentr1_s(:,1),zentr1_s(:,2),num2str(radii1_s),'color','r');
    
    ax2=subplot(1,2,2);
    imshow(AufnBohr2); hold on;
    plot(zentr2_s(:,1),zentr2_s(:,2),'*-','linewidth',2)
    viscircles(zentr2_s, radii2_s,'Color','b');
    text(zentr2_s(:,1),zentr2_s(:,2),num2str(radii2_s),'color','r');
    ax1.Position=[0.005 0 .49 1]; ax2.Position=[.505 0 .49 1];
end

end

%% Unterfunktionen

function [zentr_s,radii_s]=fu_sortK(zentr, radii, grAufn)
% Kleine Bohrungen sortieren

% Abstände der Punkte zu den Ecken berechnen
xy1=zentr; d1=sqrt(sum(xy1.^2,2)); 
xy2=[xy1(:,1) grAufn(1)-xy1(:,2)]; d2=sqrt(sum(xy2.^2,2)); 
xy3=[grAufn(2)-xy1(:,1) grAufn(1)-xy1(:,2)]; d3=sqrt(sum(xy3.^2,2)); 
xy4=[grAufn(2)-xy1(:,1) xy1(:,2)]; d4=sqrt(sum(xy4.^2,2)); 

% 
[~,indMn1]=min(d1); eck(1,:)=xy1(indMn1,:); 
[~,indMn2]=min(d2); eck(2,:)=xy1(indMn2,:); 
[~,indMn3]=min(d3); eck(3,:)=xy1(indMn3,:); 
[~,indMn4]=min(d4); eck(4,:)=xy1(indMn4,:); 

% Berechne die Homographie 
abstv=6; absth=8; 
xy0Zeichn=[0 0; 0 abstv; absth abstv; absth 0];
v = homography_solve(eck, xy0Zeichn);
xyKlZeichn = homography_transform(zentr, v);

% Sortiere die Mittelpunkte und Bohrungen 
[~,inds]=sort(xyKlZeichn(:,1)+xyKlZeichn(:,2)*absth); 
zentr_s=zentr(inds,:); 
radii_s=radii(inds);

end

