% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

%% Testweise-Auswertung VT2

clear all; close all; clc;

%% Laden der Werte

strVT='Versuchstagebuch_Tag2.xlsx';
Tvt=readtable(strVT);

% Konfigurationen, Wiederholungen und Kalibrierungen auslesen
VersuchID_temp=split(Tvt.VersuchID,'_');
Tvt.id_konfig=str2double(VersuchID_temp(:,2));
Tvt.id_wdh=str2double(VersuchID_temp(:,3));
Tvt.id_kal=str2double(VersuchID_temp(:,5));

% Spaltennamen auslesen
nms=Tvt.Properties.VariableNames; 

% Informationen auslesen
nKonfig=length(unique(Tvt.id_konfig)); 
nKal=length(unique(Tvt.id_kal)); 
nWdh=length(unique(Tvt.id_wdh)); 

%% Auswertung aller Merkmale (Wiederholungen gemittelt)

% Spalten auswählen
sp_abw=contains(nms,'Abw_');
sp_werte=contains(nms,'Werte_');
sp_abw_x=contains(nms,'_X_') & sp_abw;
sp_abw_y=contains(nms,'_Y_') & sp_abw;
sp_abw_z=contains(nms,'_Z_') & sp_abw;
sp_abw_d=contains(nms,'_Durchmesser_Ebene') & sp_abw;
sp_abw_cx=contains(nms,'_Bogen') & sp_abw;
sp_abw_cv=contains(nms,'_Schale') & sp_abw;

% Anzahl der Merkmale
nMerk=sum(sp_abw_y); 

% Zeilen der normalen Kalibrierung auswählen
zl_kalNorm=Tvt.id_kal<6;

% Mittelwerte und Streuungen je optischer Konfiguration berechnen 
ABW_m_x=zeros(nKonfig,nMerk); ABW_m_y=zeros(nKonfig,nMerk);  
STREU_m_x=zeros(nKonfig,nMerk); STREU_m_y=zeros(nKonfig,nMerk);  
abw_x=zeros(nKonfig,1); abw_y=zeros(nKonfig,1);
abw_z=zeros(nKonfig,1); abw_d=zeros(nKonfig,1); 
streu_x=zeros(nKonfig,1); streu_y=zeros(nKonfig,1); 
streu_z=zeros(nKonfig,1); streu_d=zeros(nKonfig,1); 
abw_cx=zeros(nKonfig,1); abw_cv=zeros(nKonfig,1);
streu_cx=zeros(nKonfig,1); streu_cv=zeros(nKonfig,1);
leg=cell(nKonfig,1); 
for i=unique(Tvt.id_konfig)'

    % Konfigurationen bestimmen
    zl_konf_i=Tvt.id_konfig==i; 
    zl_i=zl_konf_i & zl_kalNorm; 
    
    % Mittelwert und Streuung aller Wiederholungen bestimmen und
    % anschließend für alle Merkmale den Mittelwert berechnen 
    ABW_m_x(i,:)=mean( Tvt{zl_i,sp_abw_x} );
    STREU_m_x(i,:)=2*std( Tvt{zl_i,sp_abw_x} );
    abw_x(i)=mean( ABW_m_x(i,:) );
    streu_x(i)=mean( STREU_m_x(i,:) );
    
    % Für y
    ABW_m_y(i,:)=mean( Tvt{zl_i,sp_abw_y} );
    STREU_m_y(i,:)=2*std( Tvt{zl_i,sp_abw_y} );
    abw_y(i)=mean( ABW_m_y(i,:) );
    streu_y(i)=mean( STREU_m_y(i,:) );
    
    % Für z
    abw_z(i)=mean( mean(Tvt{zl_i,sp_abw_z}) );
    streu_z(i)=mean( 2*std(Tvt{zl_i,sp_abw_z}) );
    
    % Für d
    abw_d(i)=mean( mean(Tvt{zl_i,sp_abw_d}) );
    streu_d(i)=mean( 2*std(Tvt{zl_i,sp_abw_d}) );
    
    % Für convex
    abw_cx(i)=mean( mean(Tvt{zl_i,sp_abw_cx}) );
    streu_cx(i)=mean( 2*std(Tvt{zl_i,sp_abw_cx}) );
    
    % Für concav
    abw_cv(i)=mean( mean(Tvt{zl_i,sp_abw_cv}) );
    streu_cv(i)=mean( 2*std(Tvt{zl_i,sp_abw_cv}) );
    
    % Legende
    leg{i}=['Configuration ',num2str(i)];

end

%% Balkendiagramm (1)
werte=[abw_x, abw_y, abw_z, abw_d]';
streu=[streu_x, streu_y, streu_z, streu_d]';
beschr.tit='Average systematic error including measurement uncertainty';
beschr.xName='';
beschr.yName='Systematic error in mm'; 
beschr.xTickNames={'x coord.','y coord.','z coord.','diameter'}; 
beschr.leg=leg;
f_error_1=fgAusw_plotBalken(werte,streu,beschr,...
    'cmat',[254,240,217; 253,204,138; 252,141,89; 215,48,31]/255,...
    'lpos',[0.6 0.63 0.2575 0.2475]);
f_error_1.Position = [485 621 400 200];

%% Balkendiagramm (2)
werte=[abw_cx, abw_cv]';
streu=[streu_cx, streu_cv]';
beschr.tit='Average systematic error including measurement uncertainty';
beschr.xName='';
beschr.yName='Systematic error in mm'; 
beschr.xTickNames={'Convex','Concave'}; 
beschr.leg=leg;
f_error_2=fgAusw_plotBalken(werte,streu,beschr,...
    'cmat',[254,240,217; 253,204,138; 252,141,89; 215,48,31]/255,...
    'lpos',[0.6 0.25 0.2575 0.2475]);
f_error_2.Position = [985 621 400 200];

%% Exportieren 

sstr_i='Diagramme/topo_error_1'
export_fig(f_error_1,sstr_i,'-jpg','-r300')

sstr_i='Diagramme/topo_error_2'
export_fig(f_error_2,sstr_i,'-jpg','-r300')