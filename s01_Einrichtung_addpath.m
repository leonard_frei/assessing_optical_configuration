% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

% Korrekte Ordner hinzufügen

% Funktione für dieses Projekt
addpath(genpath('FunktionenProjekt'));

% Allgemeine Funktionen für Verarbeitung
addpath(genpath('FunktionenGenerell'));

% Funktionen aus der Filebase
addpath(genpath('FunktionenFilebase'));

% Check, ob alle Daten da sind
if ~isfolder('Daten/KMGMessungen')
    disp('KMG Daten fehlen!')
end
if ~isfolder('Daten/20210114_Versuchstag2')
    disp('Kameradaten fehlen!')
end

% Folgende Funktionen sollten aus def Filebase geladen werden
disp('Folge Funktionen laden');
disp('supererr.m');
disp('superbar.m');
disp('natsortfiles.m');
disp('natsort.m');
disp('homography_transform.m');
disp('homography_solve.m');
disp('export_fig');