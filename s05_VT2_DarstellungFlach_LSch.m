% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

%% Testweise-Auswertung VT2

clear all; close all; clc;

%% Laden der Werte

strVT='Versuchstagebuch_Tag2_flach.xlsx';
strVT_kalM='Versuchstagebuch_Tag2_flach_kalMean.xlsx';
Tvt=readtable(strVT);
Tvt_kalM=readtable(strVT_kalM);

% Kal-Mean als eigene Kalibrierung (6) einf�gen
Tvt_kalM.VersuchID=strrep(Tvt_kalM.VersuchID,'Kal_1','Kal_6');
Tvt=[Tvt;Tvt_kalM];

% Konfigurationen, Wiederholungen und Kalibrierungen auslesen
VersuchID_temp=split(Tvt.VersuchID,'_');
Tvt.id_konfig=str2double(VersuchID_temp(:,2));
Tvt.id_wdh=str2double(VersuchID_temp(:,3));
Tvt.id_kal=str2double(VersuchID_temp(:,5));

% Spaltennamen auslesen
nms=Tvt.Properties.VariableNames; 

% Informationen auslesen
nKonfig=length(unique(Tvt.id_konfig)); 
nKal=length(unique(Tvt.id_kal)); 
nWdh=length(unique(Tvt.id_wdh)); 

%% Auswertung aller Merkmale (Wiederholungen gemittelt)

% Spalten ausw�hlen
sp_abw=contains(nms,'Abw_');
sp_werte=contains(nms,'Werte_');
sp_abw_x=contains(nms,'_x_') & sp_abw;
sp_abw_y=contains(nms,'_y_') & sp_abw;
sp_abw_d=contains(nms,'_d_') & sp_abw;
sp_werte_x=contains(nms,'_x_') & sp_werte;
sp_werte_y=contains(nms,'_y_') & sp_werte;
sp_werte_d=contains(nms,'_d_') & sp_werte;

% Anzahl der Merkmale
nMerk=sum(sp_abw_y); 

% Zeilen der normalen Kalibrierung ausw�hlen
zl_kalNorm=Tvt.id_kal<6;

% Mittelwerte und Streuungen je optischer Konfiguration berechnen 
ABW_m_x=zeros(nKonfig,nMerk); ABW_m_y=zeros(nKonfig,nMerk);  
STREU_m_x=zeros(nKonfig,nMerk); STREU_m_y=zeros(nKonfig,nMerk);  
WERTE_m_x=zeros(nKonfig,nMerk); WERTE_m_y=zeros(nKonfig,nMerk);  
abw_x=zeros(nKonfig,1); abw_y=zeros(nKonfig,1); abw_d=zeros(nKonfig,1); 
streu_x=zeros(nKonfig,1); streu_y=zeros(nKonfig,1); streu_d=zeros(nKonfig,1); 
leg=cell(nKonfig,1); 
for i=unique(Tvt.id_konfig)'

    % Konfigurationen bestimmen
    zl_konf_i=Tvt.id_konfig==i; 
    zl_i=zl_konf_i & zl_kalNorm; 
    
    % Mittelwert und Streuung aller Wiederholungen bestimmen und
    % anschlie�end f�r alle Merkmale den Mittelwert berechnen 
    ABW_m_x(i,:)=mean( Tvt{zl_i,sp_abw_x} );
    STREU_m_x(i,:)=2*std( Tvt{zl_i,sp_abw_x} );
    WERTE_m_x(i,:)=mean( Tvt{zl_i,sp_werte_x} );
    abw_x(i)=mean( ABW_m_x(i,:) );
    streu_x(i)=mean( STREU_m_x(i,:) );
    
    % F�r y
    ABW_m_y(i,:)=mean( Tvt{zl_i,sp_abw_y} );
    STREU_m_y(i,:)=2*std( Tvt{zl_i,sp_abw_y} );
    WERTE_m_y(i,:)=mean( Tvt{zl_i,sp_werte_y} );
    abw_y(i)=mean( ABW_m_y(i,:) );
    streu_y(i)=mean( STREU_m_y(i,:) );
    
    % F�r d
    abw_d(i)=mean( mean(Tvt{zl_i,sp_abw_d}) );
    streu_d(i)=mean( 2*std(Tvt{zl_i,sp_abw_d}) );
    
    % Legende
    leg{i}=['Configuration ',num2str(i)];

end

%% Balkendiagramm 
werte=[abw_x, abw_y, abw_d]';
streu=[streu_x, streu_y, streu_d]';
beschr.tit='Average systematic error including measurement uncertainty';
beschr.xName='';
beschr.yName='Systematic error in mm'; 
beschr.xTickNames={'x coord.','y coord.','diameter'}; 
beschr.leg=leg;
f_error=fgAusw_plotBalken(werte,streu,beschr,...
    'cmat',[254,240,217; 253,204,138; 252,141,89; 215,48,31]/255,...
    'lpos',[0.6275 0.67 0.2575 0.2475]);
f_error.Position = [985 621 400 200];

%% Pfeildiagramm

% Quiver darstellen
fg=figure('color',[1 1 1]); 
fg.Position=[340 280 900 700];
sgtitle({'Average systematic error of hole centers',...
    '(Arrow length in 1/100mm)'})
for i=1:nKonfig
    
    % Werte der aktuellen Konfiguration 
    x_i=WERTE_m_x(i,:);
    y_i=WERTE_m_y(i,:);
    xA_i=ABW_m_x(i,:)*100;
    yA_i=ABW_m_y(i,:)*100;
    
    % Darstellung als Pfeilplot
    subplot(2,2,i); 
    ax_i=quiver(x_i,y_i,xA_i,yA_i,0); axis equal
    xlabel('x Position'); ylabel('y Position'); 
    title(leg{i},'Interpreter','none')
    
end
f_quiver = gcf;
f_quiver.Position = [985 121 400 360];

%% Vergleich der Kalibrierung f�r Bildwiederholung 1

ABW_wdh_kal_y=zeros(nKal-1,nMerk,nKonfig); 
for i=1:nKonfig
    
    % F�r Konfiguration i alle Kalibrierungen f�r Bildset 1 suchen
    zl_konf_i=Tvt.id_konfig==i;
    zl_wdh_1=Tvt.id_wdh==1;
    zl_wdh_kal_i=zl_konf_i & zl_wdh_1 & zl_kalNorm;
    
    % Alle Abweichungen der y-Koordinate auslesen
    ABW_wdh_kal_y(:,:,i)=Tvt{zl_wdh_kal_i,sp_abw_y};
    
end

% Berechnung der maximalen Unterschiede der Mittelwerte der
% Bildwiederholungen f�r die verschiedenen Konfigurationen
ABW_wdh_kal_y_mw=median(ABW_wdh_kal_y,2);
abw_wdh_kal_y_mwAbst=squeeze(peak2peak(ABW_wdh_kal_y_mw));

%% Darstellung des Vergleichs der Kalibrierungen 

% Boxplots f�r die erste Konfiguration
nmsBoxPlot={'Rep. 1', 'Rep. 2', 'Rep. 3', 'Rep. 4', 'Rep. 5'};
f_wdh_kal_1=figure;
boxplot(ABW_wdh_kal_y(:,:,1)', nmsBoxPlot);
xlabel('Calibration Repetition');
ylabel('Systematic error in mm');
title('y coordinate / Image set 1 / Optical configuration 1');
axis([0 6 -0.32 0.7]);
set(gcf, 'color', 'white');
f_wdh_kal_1.Position=[243 568 320 200]

% Vergleich der Mittelwerte der Boxplots
f_wdh_kal=figure;
b = bar(abw_wdh_kal_y_mwAbst,'FaceColor','flat');
b.CData(1,:) = [254 240 217]/255;
b.CData(2,:) = [253 204 138]/255;
b.CData(3,:) = [252 141 89]/255;
b.CData(4,:) = [215 48 31]/255;
ylabel('Deviation in mm');
xlabel('Configuration');
title('y coordinate / Image set 1');
axis([0 5 0 0.45]);
set(gcf, 'color', 'white');
f_wdh_kal.Position=[571 568 320 200]

%% Vergleich der Bildwiederholung f�r Kalibrierung Mean

ABW_wdh_img_y=zeros(nKal-1,nMerk,nKonfig); 
for i=1:nKonfig
    
    % F�r Konfiguration i alle Kalibrierungen f�r Bildset 1 suchen
    zl_konf_i=Tvt.id_konfig==i;
    zl_wdh_img_i=zl_konf_i & ~zl_kalNorm;
    
    % Alle Abweichungen der y-Koordinate auslesen
    ABW_wdh_img_y(:,:,i)=Tvt{zl_wdh_img_i,sp_abw_y};
    
end

% Berechnung der maximalen Unterschiede der Mittelwerte der
% Bildwiederholungen f�r die verschiedenen Konfigurationen
ABW_wdh_img_y_mw=mean(ABW_wdh_img_y,2);
abw_wdh_img_y_mwAbst=squeeze(peak2peak(ABW_wdh_img_y_mw));

%% Darstellung des Vergleichs der Bildwiederholung

% Boxplots f�r die erste Konfiguration
nmsBoxPlot={'Rep. 1', 'Rep. 2', 'Rep. 3', 'Rep. 4', 'Rep. 5'};
f_wdh_img_1=figure;
boxplot(ABW_wdh_img_y(:,:,1)', nmsBoxPlot);
xlabel('Imaging Process Repetition');
ylabel('Deviation in mm');
title('y coordinate / Mean Calibration / Optical configuration 1');
axis([0 6 -0.32 0.7]);
set(gcf, 'color', 'white');
f_wdh_img_1.Position=[243 268 320 200]

% Vergleich der Mittelwerte der Boxplots
f_wdh_img=figure;
b = bar(abw_wdh_img_y_mwAbst,'FaceColor','flat');
b.CData(1,:) = [254 240 217]/255;
b.CData(2,:) = [253 204 138]/255;
b.CData(3,:) = [252 141 89]/255;
b.CData(4,:) = [215 48 31]/255;
ylabel('Deviation in mm');
xlabel('Configuration');
title('y coordinate / Mean Calibration');
axis([0 5 0 0.45]);
set(gcf, 'color', 'white');
f_wdh_img.Position=[571 268 320 200]

%% Exportieren 

sstr_i='Diagramme/flat_error'
export_fig(f_error,sstr_i,'-jpg','-r300')

sstr_i='Diagramme/flat_quiver'
export_fig(f_quiver,sstr_i,'-jpg','-r300')

sstr_i='Diagramme/flat_wdh_kal_1'
export_fig(f_wdh_kal_1,sstr_i,'-jpg','-r300')

sstr_i='Diagramme/flat_wdh_kal'
export_fig(f_wdh_kal,sstr_i,'-jpg','-r300')

sstr_i='Diagramme/flat_wdh_img_1'
export_fig(f_wdh_img_1,sstr_i,'-jpg','-r300')

sstr_i='Diagramme/flat_wdh_img'
export_fig(f_wdh_img,sstr_i,'-jpg','-r300')