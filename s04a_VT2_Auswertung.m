% Copyright (c) <2022>, <Leonard Schild>
% All rights reserved.
% 
% This source code is licensed under the MIT-style license found in the
% README file in the root directory of this source tree. 

%% Auswertung Versuchstag

%% Erstelle die Punktwolke

% Aufnahmen laden
load(aufnKalStr{1}); load(aufnKalStr{2});

% Entzerre alle Aufnahmen
AufnProjU=fgKam_undistImage(AufnProj,kamPara);

% Berechne die Punktewolke des Streifenmusters
xyz_Kam=fgStereo_StrLichtPW(AufnProjU,kamPara,nSt,...
    'thDiffB',thDiffB,'parDiffSt',parDiffSt,'visBinErg',false);
xyzC_Kam=xyz_Kam(xyz_Kam(:,3)<450 & xyz_Kam(:,3)>300,:);

%% Erstellung der Punktwolke

% Berechne die Punktewolke der drei Bohrungen auf der Grundplatte
[xyzB_Kam, rB_Kam]=fpStereoMu_Bohr(AufnProjU,kamPara,radiusGr,...
    'thK',thK,'vis',false);

% Erstelle Gesamtpunktwolke
PW_k=pointCloud([xyzB_Kam;xyz_Kam]);

%% Berechnung der Messwerte

% Registriere die Punktewolke (Nullpunkt: obere linke Bohrung (in der Linie
% der Welle), x-Achse: Verbindung der beiden oberen Bohrungen, z-Achse:
% senkrecht zur oberen Fläche)
[PW_w, kam2world]=fpStereoMu_PunktWolkeReg(PW_k,dZEbene,'vis',false);

% Berechnung der Messelemente
messElm=fgStereo_MessElm(messElm,PW_w,'visErg',false,...
    'Bohr',[PW_w.Location(4:15,:), rB_Kam(4:end)]);

% Berchnung der Prüfelemente
prfElm=fgStereo_PrfElm(prfElm,messElm,TRef); struct2table(prfElm);

%% Versuchstagebuch beschreiben

PrfElmW=[prfElm(:).prfElmWertP]; 
MessAbw=[prfElm(:).abwRefP]; 

nameP_VB=cellfun(@(x) strrep(x,' ','_'),nameP,'UniformOutput',false);
nameP_VB=cellfun(@(x) strrep(x,'+',''),nameP_VB,'UniformOutput',false);
nameAbw_VTB=cellfun(@(x) append('Abw_',x),nameP_VB,'UniformOutput',false);
nameWerte_VTB=cellfun(@(x) append('Werte_',x),nameP_VB,'UniformOutput',false);

fgAusw_vtagebuchEintrag(VersuchID,{'Aufnahmen','Kalibrierung'},aufnKalStr,'strVT',strVT);
fgAusw_vtagebuchEintrag(VersuchID,nameAbw_VTB,MessAbw,'strVT',strVT);
fgAusw_vtagebuchEintrag(VersuchID,nameWerte_VTB,PrfElmW,'strVT',strVT);